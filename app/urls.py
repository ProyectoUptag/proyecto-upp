"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from api import views
from api import reports

from django.contrib import admin

urlpatterns = [
    url(r'^$', csrf_exempt(TemplateView.as_view(template_name='index.html'))),
    url(r'^api/', include('api.urls')),

    url(r'^report/solicitud/(?P<pk>\d+)/$', reports.report),
    url(r'^solicitud', views.solicitud, name='solicitud'),
    #url(r'^pusher/(?P<pk>\d+)/$', views.pusher, name='pusher'),
    #url(r'^get_id_notificacion', views.get_id_notificacion, name='get_id_notificacion'),

    #url(r'^post_id_notificacion/(?P<pk>.*)/$', views.post_id_notificacion, name='post_id_notificacion'),

    url(r'^reset_password', views.reset_password, name='reset_password'),
    url(r'^#/password-forgot/(?P<hash_string>[0-9A-Za-z_\-]+)/$', views.get_confirm_password, name='get_password_password'),
    url(r'^confirm_password/(?P<action>[A-Za-z_\-]+)/(?P<hash_string>[0-9A-Za-z_\-]+)/$', views.confirm_password, name='confirm_password'),
]
#