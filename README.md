# proyecto-upp
	Proyecto - Unidad de Practicas Profesionales

# Requerimientos
	yarn >= 1.3.2 							[gestor de paquetes]
	node  >= 8.9.4 							[servidor]

# Descargar las dependencias
	sudo apt-get install python-dev 		[requerimientos python]
	sudo apt-get install libmysqlclient-dev
	pip install -r requirements.txt
	yarn install							[requerimientos react]

# Iniciar el servidor de desarrollo de react
	yarn start 								[podras ver el entorno de desarrollo de react corriendo en el navegador con la siguiente ruta `localhost:3000`]

# Iniciar el servidor de produccion de python
	1. abrir el archivo .dbConfig.conf 		[archivo oculto para agregar los datos de la conexion con la base de datos MySQL]	
	2. python manage.py makemigrations 		[busca los cambios en los modelos de app]
	3. python manage.py makemigrations api 	[busca los cambios en los modelos de la api]
	4. python manage.py migrate				[hace crea las tablas en la basa de datos]
	5 ./load_data_db.sh						[carga los estados de las solicitudes]
	6. yarn run build 						[compila la aplicacion]	
	7. python manage.py runserver  			[podras ver la aplicacion corriendo en el navegador con la siguiente ruta `localhost:8000`]

#Usuarios precargados
   ###Administrador
        usuario: admin
        contraseña: admin
        
   ###Aulumno
        usuario: alumno
        conraseña: alumno