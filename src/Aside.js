import React, { Component } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Progress } from 'reactstrap';
import classnames from 'classnames';

import { Switch, Icon } from 'antd';

class Aside extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  onChange = (checked) => {
    console.log(`switch to ${checked}`);
  }

  render() {
    return (
      <aside className="aside-menu">
        <Nav tabs>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.toggle('1'); }}>
              <Icon type="bell" />
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.toggle('2'); }}>

              <Icon type="mail" />
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.toggle('3'); }}>
              <Icon type="setting" style={{fontSize: '15px'}} />
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Icon type="logout" />
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <div className="callout m-0 py-2 text-muted text-center bg-faded text-uppercase">
              <small><b>Hoy</b></small>
            </div>
            <hr className="transparent mx-3 my-0"/>
            <div className="callout callout-warning m-0 py-3">
              <div className="avatar float-right">
                <img src={'static/img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
              </div>
              <div>Nueva solicitud de <strong>Luis</strong></div>
              <small className="text-muted mr-3"><i className="icon-calendar"></i>&nbsp; 1 - 3pm</small>
              <small className="text-muted"><i className="icon-location-pin"></i>&nbsp; Palo Alto, CA </small>
            </div>
            <hr className="mx-3 my-0"/>
            <div className="callout callout-info m-0 py-3">
              <div className="avatar float-right">
                <img src={'static/img/avatars/4.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
              </div>
              <div>Nueva solicitud de <strong>Maria</strong></div>
              <small className="text-muted mr-3"><i className="icon-calendar"></i>&nbsp; 4 - 5pm</small>
              <small className="text-muted"><i className="icon-social-skype"></i>&nbsp; On-line </small>
            </div>
            <hr className="transparent mx-3 my-0"/>
            <div className="callout m-0 py-2 text-muted text-center bg-faded text-uppercase">
              <small><b>Historial</b></small>
            </div>
            <hr className="transparent mx-3 my-0"/>
            <div className="callout callout-danger m-0 py-3">
              <div>Solicitudes de <strong>Acreditacion</strong></div>
              <small className="text-muted mr-3"><i className="icon-calendar"></i>&nbsp; 10 - 11pm</small>
              <small className="text-muted"><i className="icon-home"></i>&nbsp; creativeLabs HQ </small>
              <div className="avatars-stack mt-2">
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/2.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/3.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/4.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/5.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
              </div>
            </div>
            <hr className="mx-3 my-0"/>
            <div className="callout callout-success m-0 py-3">
              <div> Solicitud con <strong>Datos Faltantes</strong></div>
              <small className="text-muted mr-3"><i className="icon-calendar"></i>&nbsp; 1 - 3pm</small>
              <small className="text-muted"><i className="icon-location-pin"></i>&nbsp; Palo Alto, CA </small>
            </div>
            <hr className="mx-3 my-0"/>
            <div className="callout callout-primary m-0 py-3">
              <div><strong>Asignados</strong></div>
              <small className="text-muted mr-3"><i className="icon-calendar"></i>&nbsp; 4 - 6pm</small>
              <small className="text-muted"><i className="icon-home"></i>&nbsp; creativeLabs HQ </small>
              <div className="avatars-stack mt-2">
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/2.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/3.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/4.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/5.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
                <div className="avatar avatar-xs">
                  <img src={'static/img/avatars/8.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                </div>
              </div>
            </div>
            <hr className="mx-3 my-0"/>
          </TabPane>
          <TabPane tabId="2" className="p-3">
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img src={'static/img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Luis Rodriguez</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
            <hr/>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img src={'static/img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Luis Rodriguez</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
            <hr/>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img src={'static/img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Luis Rodriguez</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
            <hr/>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img src={'static/img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Luis Rodriguez</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
            <hr/>
            <div className="message">
              <div className="py-3 pb-5 mr-3 float-left">
                <div className="avatar">
                  <img src={'static/img/avatars/7.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com"/>
                  <span className="avatar-status badge-success"></span>
                </div>
              </div>
              <div>
                <small className="text-muted">Luis Rodriguez</small>
                <small className="text-muted float-right mt-1">1:52 PM</small>
              </div>
              <div className="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
              <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>
            </div>
          </TabPane>
          <TabPane tabId="3" className="p-3">
            <h6>Configuración</h6>

            <div className="aside-options">
              <div className="clearfix mt-4">
                <small><b>Option 1</b></small>
                <label className="switch switch-text switch-pill switch-success switch-sm float-right">
                  <Switch defaultChecked={true} checkedChildren={<Icon type="check" />} unCheckedChildren={<Icon type="cross" />} onChange={this.onChange()} />
                </label>
              </div>
              <div>
                <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>
              </div>
            </div>

            <div className="aside-options">
              <div className="clearfix mt-3">
                <small><b>Option 2</b></small>
                <label className="switch switch-text switch-pill switch-success switch-sm float-right">
                  <Switch defaultChecked={false} checkedChildren={<Icon type="check" />} unCheckedChildren={<Icon type="cross" />} onChange={this.onChange()} />
                </label>
              </div>
              <div>
                <small className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>
              </div>
            </div>

            <div className="aside-options">
              <div className="clearfix mt-3">
                <small><b>Option 3</b></small>
                <label className="switch switch-text switch-pill switch-success switch-sm float-right">
                  <Switch defaultChecked={false} checkedChildren={<Icon type="check" />} unCheckedChildren={<Icon type="cross" />} onChange={this.onChange()} />
                </label>
              </div>
            </div>

            <div className="aside-options">
              <div className="clearfix mt-3">
                <small><b>Option 4</b></small>
                <label className="switch switch-text switch-pill switch-success switch-sm float-right">
                  <Switch defaultChecked={false} checkedChildren={<Icon type="check" />} unCheckedChildren={<Icon type="cross" />} onChange={this.onChange()} />
                </label>
              </div>
            </div>

            <hr/>
            <h6>Historial del Sistema</h6>

            <div className="text-uppercase mb-1 mt-4"><small><b>Solicitudes Aprobadas</b></small></div>
            <Progress className="progress-xs" color="success" value="80" />
            <small className="text-muted">348 Procesadas</small>

            <div className="text-uppercase mb-1 mt-2"><small><b>Solicitudes con Datos Faltantes</b></small></div>
            <Progress className="progress-xs" color="warning" value="35" />
            <small className="text-muted">114 Faltantes</small>

            <div className="text-uppercase mb-1 mt-2"><small><b>Solicitudes Rechazadas</b></small></div>
            <Progress className="progress-xs" color="danger" value="5" />
            <small className="text-muted">5 Rechazadas</small>

            <div className="text-uppercase mb-1 mt-2"><small><b>Solicitudes Pendientes</b></small></div>
            <Progress className="progress-xs" color="info" value="15" />
            <small className="text-muted">15 Pendientes</small>



          </TabPane>
        </TabContent>
      </aside>
    )
  }
}

export default Aside;