//URLS
import { URLS } from './config';

module.exports = {

    getToken(username, pass, res) {

        fetch(URLS.SERVER_URL + '/api/obtain-auth-token/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: pass
            })
        })
        .then(response => {
            return response.json();

        })
        .then(data => {
            res({
                token: data.token
            });

        })
        .catch( e => console.error( 'Ha ocurrido un error' ));
    },

    login(username, pass, recDatos, log) {

        if (recDatos){
            localStorage.r = true;
        }

        this.getToken(username, pass, (res) => {
            if (res.token){
                localStorage.token = res.token;
                log(true);

            }else {
                log(false);
            }
        });
    },    
    
    logout() {
        delete localStorage.token;
        delete localStorage.user_data;
        delete localStorage._a;
        if (localStorage.r){
            delete localStorage.r;
        }
    },

    loggedIn() {
        return !!localStorage.token;
    }
};
