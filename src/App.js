// Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Dependencies Material UI
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ThemeDefault from './theme-default';
// import getMuiTheme from 'material-ui/styles/getMuiTheme'

class App extends Component {

  render() {

    return (
      <MuiThemeProvider muiTheme={ThemeDefault}>
        <div>
            {this.props.children}
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

// window.addEventListener("beforeunload", function (e) {

//     if (!localStorage.r){
//         delete localStorage.token;
//     }
// });

export default App;

