import React, { Component } from 'react';
//import PropTypes from 'prop-types';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';

//Components
import App from './App';
import Index from './views/Index';
import Admin from './views/Admin';
import User from './views/User';
import Login from './views/Login';
import SignUp from './views/SignUp';
import NotFoundPage from './views/NotFoundPage.js';
import PasswordForgot from './views/PasswordForgot';

//Auth
//import auth from './Auth';

const history = createBrowserHistory();

export default class AppRoutes extends Component{

	render(){
		return(
			<App>
				<HashRouter history={history}>
					<Switch>
						<Route exact path='/' name="Index" component={Index}/>
						<Route exact path='/iniciar' name="Login Page" component={Login}/>												
						<Route exact path='/registrar' name="SignUp Page" component={SignUp}/>
						<Route path='/admin' name="Admin" component={Admin}/>												
						<Route path='/password-forgot' name="Contraseña Olvidada" component={PasswordForgot}/>
						{/*<Route exact path='/admin/:navas' component={Admin}/>*/}
						<Route path='/user' name="User" component={User}/>
						<Route name="Page 404" component={NotFoundPage}/>
					</Switch>
				</HashRouter>
			</App>

		);
	}

}

// AppRoutes.contextTypes = {
// 	router: PropTypes.object.isRequired
// }