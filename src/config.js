import axios from 'axios';

export const URLS = {
	SERVER_URL: window.location.origin,
	API_URL: 'http://192.168.0.110/test_api.php'
};

export const axiosClient = function(url) {
    const token = localStorage.token;
    const params = {
        baseURL: URLS.SERVER_URL + url,
        headers: {
        	'Authorization': 'Token ' + token,
            'Content-Type': 'application/json'
        }
    };
    return axios.create(params);
}