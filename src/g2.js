import G2 from 'g2';

G2.track(false);

const colors = [
  '#F4B13F', '#2D6EC4', '#0ECC44', '#1890FF', '#13C2C2', '#2FC25B', '#fa8c16', '#a0d911',
];

const config = {
  ...G2.Theme,
  defaultColor: '#1089ff',
  colors: {
    default: colors,
    intervalStack: colors,
  },
  tooltip: {
    background: {
      radius: 4,
      fill: '#000',
      fillOpacity: 0.75,
    },
  },
};

G2.Global.setTheme(config);
