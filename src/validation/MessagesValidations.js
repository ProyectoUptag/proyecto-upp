export const isRequired = fieldName => `Este campo es requerido`; /*`${fieldName} is required`;*/

export const mustMatch = otherFieldName => {
    return (fieldName) => `Las contraseñas no coinciden`; /*`${fieldName} must match ${otherFieldName}`;*/
};

export const minLength = length => {
    return (fieldName) => `Este campo debe tener al menos ${length} caracteres`; /*`${fieldName} must be at least ${length} characters`;*/
};

export const emailMessage = fieldName => `Los datos ingresados son invalidos`; /*`${fieldName} is invalid`;*/

export const alphaNumMessage = fieldName => `El campo ${fieldName} debe ser alfanumerico`; /*`${fieldName} must be alphanumeric`;*/



