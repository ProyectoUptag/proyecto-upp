import * as ValidationMessage from './MessagesValidations.js';

export const required = (text) => {
    if (text) {
        return null;
    } else {
        return ValidationMessage.isRequired;
    }
};

export const mustMatch = (field, fieldName) => {
    return (text, state) => {
        return state[field] === text ? null : ValidationMessage.mustMatch(fieldName);
    };
};

export const minLength = (length) => {
    return (text) => {
        return text.length >= length ? null : ValidationMessage.minLength(length);
    };
};

export const emailRule = (text) => {
    let regex = /^([a-zA-Z0-9_.-])+@((([a-zA-Z0-9-]+)\.)+([a-zA-Z0-9]{2,4}))+$/;

    return regex.test(text) ? null : ValidationMessage.emailMessage;
};

export const alphaNumRule = (text) => {
    let regex = /^[a-z0-9]+$/i;

    return regex.test(text) ? null : ValidationMessage.alphaNumMessage;
};

