export const MakeRule = (field, name, ...validations) => {
    return (state) => {
        for (let validation  of validations) {
            let MessageFunc = validation(state[field], state);
            if (MessageFunc) {
                return {[field]: MessageFunc(name)};
            }
        }
        return null;
    };
};

export const RunRules = (state, runners) => {
    return runners.reduce((memo, runner) => {
        return Object.assign(memo, runner(state));
    }, {});
};