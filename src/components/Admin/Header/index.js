//Dependencies
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import groupBy from 'lodash/groupBy';
import moment from 'moment';
import 'moment/locale/es'

// Styles Less
import './Header.less';

// Ant Design
import {Layout, Menu, Icon, Avatar, Dropdown, message, Tag} from 'antd';

// Components
import HeaderSearch from './HeaderSearch';
import NoticeIcon from './NoticeIcon';

// Auth
import auth from '../../../Auth';

// URLS
import {URLS} from '../../../config';

const {Header} = Layout;

class HeaderUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            user: [],
            avatar: 'static/img/user.png',
            collapsed: false,
            popupOpen: false,
            notices: []
        }

        moment.locale('es');

        this.getNotifications = this.getNotifications.bind(this);

        this.socket = new WebSocket("ws://" + window.location.host + "/?token=" + localStorage.token)

    }

    componentWillUpdate(next_props) {
        this.socket.onmessage = (e) => {
            this.getNotifications(JSON.parse(e.data));
        }
    }


    componentWillMount() {

        fetch(URLS.SERVER_URL + '/api/users/i/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
            .then(response => response.json())
            .then(data => {
                this.setState({user: data})

            })
            .catch(e => console.error('Ha ocurrido un error'));
    }

    getNotifications(notices) {
        this.setState({notices: notices})
    }

    sidebarToggle = () => {

        if (document.body.className.indexOf('sidebar-minimized') === -1 && document.body.className.indexOf('sidebar-hidden') === -1) {
            document.body.classList.toggle('sidebar-minimized');
            this.setState({
                collapsed: false,
            });
        }
        else if (document.body.className.indexOf('sidebar-minimized') !== -1) {
            document.body.classList.toggle('sidebar-minimized');
            document.body.classList.toggle('sidebar-hidden');

            this.setState({
                collapsed: true,
            });
        }
        else {
            document.body.classList.toggle('sidebar-hidden');
            this.setState({
                collapsed: false,
            });
        }
    }

    mobileSidebarToggle = (e) => {
        e.preventDefault();
        document.body.classList.toggle('sidebar-mobile-show');

        this.setState({
            collapsed: !this.state.collapsed,
        });
    }


    handleNoticeClear = (type) => {
        message.success(`Se han marcado como leídas todas las notificaciones`);
        var notices = this.state.notices.filter(item => (item.tipo_id === 1 || item.tipo_id === 2) && item.read === false);
        var ids = notices.map(e => e.pk);
        
        this.socket.send(JSON.stringify(ids));
        this.setState({notices: []})
    }

    handleNoticeVisibleChange = (visible) => {
        this.setState({popupOpen: true})
    }

    handleNotifications = (notices) => {

        return (
            notices.filter(item => (item.tipo_id === 1 || item.tipo_id === 2) && (item.read === false)).length

        );

    }

    onCollapse = (collapsed) => {
        this.props.dispatch({
            payload: collapsed,
        });
    }

    onMenuClick = ({key}) => {
        if (key === 'logout') {
            auth.logout()
            this.context.router.history.push('/iniciar', null);
        }
    }

    getNoticeData() {

        const {notices} = this.state;
        if (notices.length === 0) {
            return {};
        }
        const newNotices = notices.map((notice) => {
            const newNotice = {...notice};
            if (newNotice.datetime) {
                newNotice.datetime = moment(notice.datetime).fromNow();
            }

            if (newNotice.id) {
                newNotice.key = newNotice.id;
            }
            if (newNotice.extra && newNotice.status) {
                const color = ({
                    todo: '',
                    processing: 'blue',
                    urgent: 'red',
                    doing: 'gold',
                })[newNotice.status];
                newNotice.extra = <Tag color={color} style={{marginRight: 0}}>{newNotice.extra}</Tag>;
            }
            return newNotice;
        });
        return groupBy(newNotices, 'tipo_id');
    }

    render() {

        const noticeData = this.getNoticeData();
        const {notices} = this.state;

        const menu = (
            <Menu className={'menu'} selectedKeys={[]} onClick={this.onMenuClick}>
                <Menu.Item key="logout"><Icon type="logout"/>Cerrar sesion</Menu.Item>
            </Menu>
        );

        return (

            <div>
                <Layout>
                    <Layout className={'app-header'}>
                        <Header className={'header'}>

                            <div className={'logo'}>
                                <img src="static/img/logo.jpg" alt="logo"/>
                                <h1>SIGEPP</h1>
                            </div>

                            <Icon
                                className={'trigger d-md-down-none'}
                                type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                onClick={this.sidebarToggle}
                            />

                            <Icon
                                className={'trigger d-lg-none'}
                                type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                                onClick={this.mobileSidebarToggle}
                            />

                            <div className={'right'}>
                                <HeaderSearch
                                    className={'action search'}
                                    placeholder="Buscar"
                                    dataSource={['elemento 1', 'elemento 2', 'elemento 3']}
                                    onSearch={(value) => {
                                        console.log('input', value);
                                    }}
                                    onPressEnter={(value) => {
                                        console.log('enter', value);
                                    }}
                                />

                                <NoticeIcon
                                    className={'action'}
                                    count={this.handleNotifications(notices)}
                                    onItemClick={(item, tabProps) => {

                                        var index = this.state.notices.findIndex(e => e.pk === item.pk)
                                        const notices = [...this.state.notices]
                                        notices[index].read = true;

                                        this.setState({notices: notices})

                                        this.socket.send(JSON.stringify([item.pk]));

                                    }}
                                    onClear={this.handleNoticeClear}
                                    onPopupVisibleChange={this.handleNoticeVisibleChange}
                                    popupAlign={{offset: [20, -16]}}
                                >
                                    <NoticeIcon.Tab
                                        list={noticeData['2']}
                                        title="Registros"
                                        emptyText="Has leído todos los registros"
                                        emptyImage="static/img/1.svg"
                                    />
                                    <NoticeIcon.Tab
                                        list={noticeData['1']}
                                        title="Solicitudes"
                                        emptyText="Has leído todas las solicitudes"
                                        emptyImage="static/img/2.svg"
                                    />

                                </NoticeIcon>

                                <Dropdown overlay={menu}>
			                	  	<span className={'action account'}>
			                	    	<Avatar size="small" className={'avatar'} src={'static/img/user.png'}/>
                                        {this.state.user.username}
			                	  	</span>
                                </Dropdown>
                            </div>
                        </Header>
                    </Layout>
                </Layout>
            </div>
        )
    }
}

export default HeaderUser;

HeaderUser.contextTypes = {
    router: PropTypes.object.isRequired
};

