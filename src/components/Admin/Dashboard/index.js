import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Ant Design
import { Avatar } from 'antd';
import './Dashboard.less';

class Dashboard extends Component {

	constructor() {
	    super();
	    this.state = { user: [] };
	}

	componentWillMount() {

	}

  	render() {

	    return (
	    	<div>
	            <div style={{ padding: 24, background: '#fff' }}>

	              	<div className={'pageHeaderContent'}>

                        <div className={"avatar"}>
                            <Avatar size="large" src={ 'static/img/user.png' } />
                        </div>

	              	    <div className={'content'}>
    	              	    <p className={'contentTitle'}>Bienvenido Administrador {this.state.user.username}</p>
    	              	    <p>Unidad de Practicas Profesionales | PNFI - PNFC - PNFA - PNFM - PNFQ</p>
	              	    </div>
	              	</div>
	              	
	            </div>
	        </div>
		
	    );
  	}
}

export default Dashboard;

Dashboard.contextTypes = {
    router: PropTypes.object.isRequired
};