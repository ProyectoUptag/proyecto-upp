import React, { PureComponent } from 'react';
import axios from 'axios';
import './Tutores.less';

// Ant Design
import { Table, Button, Input, message, Popconfirm, Divider } from 'antd';

// Material UI
import Snackbar from 'material-ui/Snackbar';

// URLS
import { URLS } from '../../../config';

export default class TableForm extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            data: props.value,
            open: false,
            message: ''

        };
    }

    componentWillMount(){
        message.config({
            top: 70,
            duration: 3,
        });
    }

    componentWillReceiveProps(nextProps) {
        if ('value' in nextProps) {
            this.setState({
                data: nextProps.value,
            });
        }
    }

    getRowByKey(key) {
        return this.state.data.filter(item => item.id === key)[0];
    }

    index = 1;
    cacheOriginData = {};


    toggleEditable(e, key) {
        e.preventDefault();
        const target = this.getRowByKey(key);
        if (target) {
            if (!target.editable) {
                this.cacheOriginData[key] = { ...target };
            }

            target.editable = !target.editable;
            this.setState({ data: [...this.state.data] });
        }
    }

    remove(id) {
        const newData = this.state.data.filter(item => item.id !== id);
        this.setState({ data: newData });
        this.props.onChange(newData);

        axios({
            method: 'DELETE',
            url: URLS.SERVER_URL + '/api/tutor/' + id + '/',
            headers: {
                'Authorization': 'Token ' + localStorage.token,
                'Content-Type': 'application/json'
            },
            data: null

        })
        .then((response) => {
            this.setState({
                open: true,
                message: 'El tutor ha sido eliminado'
            });

        })
    }

    newCompany = () => {
        const newData = [...this.state.data];
        newData.push({
            id: `${this.index}`,
            nombre: '',
            apellido: '',
            cedula: '',
            numero_contacto: '',
            editable: true,
            isNew: true,
        });
        this.index += 1;
        this.setState({ data: newData });
    }

    handleKeyPress(e, key) {
        if (e.key === 'Enter') {
            this.saveRow(e, key);
        }
    }

    handleFieldChange(e, fieldName, key) {
        const newData = [...this.state.data];
        const target = this.getRowByKey(key);

        if (target) {
            target[fieldName] = e.target.value;
            this.setState({ data: newData });
        }
    }

    saveRow(e, key) {
        e.persist();
        // save field when blur input
        setTimeout(() => {
            if (document.activeElement.tagName === 'INPUT' &&
                document.activeElement !== e.target) {
                return;
            }

            if (this.clickedCancel) {
                this.clickedCancel = false;
                return;
            }

            const target = this.getRowByKey(key);

            if (!target.nombre || !target.apellido || !target.cedula || !target.numero_contacto) {
                message.error('Debe llenar todos los campos para agregar un tutor');
                e.target.focus();
                return;
            }


            if(target.isNew === undefined){
                axios({
                    method: 'PUT',
                    url: URLS.SERVER_URL + '/api/tutor/' + target.id + '/',
                    headers: {
                        'Authorization': 'Token ' + localStorage.token,
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        id: target.id,
                        nombre: target.nombre,
                        apellido: target.apellido,
                        numero_contacto: target.numero_contacto,
                        cedula: target.cedula,
                    })

                })
                .then((response) => {
                    this.setState({
                        open: true,
                        message: 'Los datos del tutor han sido actualizados'
                    });

                })
                .catch((data) => {
                    this.setState({
                        open: true,
                        message: 'No se han podido guardar los datos del tutor'
                    });
                });

            }else{
                axios({
                    method: 'POST',
                    url: URLS.SERVER_URL + '/api/tutor/',
                    headers: {
                        'Authorization': 'Token ' + localStorage.token,
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        nombre: target.nombre,
                        apellido: target.apellido,
                        numero_contacto: target.numero_contacto,
                        cedula: target.cedula,
                    })

                })
                .then((response) => {
                    this.setState({
                        open: true,
                        message: 'Se agrego el tutor con exito'
                    });

                })
                .catch((data) => {
                    this.setState({
                        open: true,
                        message: 'No se han podido guardar los datos del tutor'
                    });
                });
            }

            delete target.isNew;
            this.toggleEditable(e, key);
            this.props.onChange(this.state.data);


        }, 10);
    }

    cancel(e, key) {
        e.preventDefault();
        this.clickedCancel = true;
        const target = this.getRowByKey(key);

        if (this.cacheOriginData[key]) {
            Object.assign(target, this.cacheOriginData[key]);
            target.editable = false;
            delete this.cacheOriginData[key];
        }

        this.setState({ data: [...this.state.data] });
    }

    handleRequestClose = () => {
        this.setState({
            open: false,
            message: ''
        });
    };
    
    render() {
        const columns = [{
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
            width: '20%',
            render: (text, record) => {
                if (record.editable) {
                    return (
                        <Input
                            value={text}
                            autoFocus
                            onChange={e => this.handleFieldChange(e, 'nombre', record.id)}
                            onKeyPress={e => this.handleKeyPress(e, record.id)}
                            placeholder="Nombre"
                        />
                    );
                }
                return text;
            },
        },
        {
            title: 'Apellido',
            dataIndex: 'apellido',
            key: 'apellido',
            width: '25%',
            render: (text, record) => {
                if (record.editable) {
                    return (
                        <Input
                            value={text}
                            onChange={e => this.handleFieldChange(e, 'apellido', record.id)}
                            onKeyPress={e => this.handleKeyPress(e, record.id)}
                            placeholder="Apellido"
                        />
                    );
                }
                return text;
            },
        },
        {
            title: 'Cedula',
            dataIndex: 'cedula',
            key: 'cedula',
            width: '20%',
            render: (text, record) => {
                if (record.editable) {
                    return (
                        <Input
                            value={text}
                            onChange={e => this.handleFieldChange(e, 'cedula', record.id)}
                            onKeyPress={e => this.handleKeyPress(e, record.id)}
                            placeholder="Cedula"
                        />
                    );
                }
                return text;
            },
        },
        {
            title: 'Numero de Contacto',
            dataIndex: 'numero_contacto',
            key: 'numero_contacto',
            width: '20%',
            render: (text, record) => {
                if (record.editable) {
                    return (
                        <Input
                            value={text}
                            onChange={e => this.handleFieldChange(e, 'numero_contacto', record.id)}
                            onKeyPress={e => this.handleKeyPress(e, record.id)}
                            placeholder="Numero de Contacto"
                        />
                    );
                }
                return text;
            },
        }, 
        {
            title: 'Operacion',
            key: 'action',
            render: (text, record) => {
                if (record.editable) {
                    if (record.isNew) {
                        return (
                            <span>
                                <a onClick={e => this.saveRow(e, record.id)}>Guardar</a>
                                <Divider type="vertical" />
                                <Popconfirm title="Desea eliminar este tutor" onConfirm={() => this.remove(record.id)}>
                                    <a>Eliminar</a>
                                </Popconfirm>
                            </span>
                        );
                    }
                    return (
                        <span>
                            <a onClick={e => this.saveRow(e, record.id)}>Guardar</a>
                            <Divider type="vertical" />
                            <a onClick={e => this.cancel(e, record.id)}>Cancelar</a>
                        </span>
                    );
                }

                return (
                    <span>
                        <a onClick={e => this.toggleEditable(e, record.id)}>Editar</a>
                        <Divider type="vertical" />
                        <Popconfirm title="Desea eliminar este tutor" onConfirm={() => this.remove(record.id)}>
                            <a>Eliminar</a>
                        </Popconfirm>
                    </span>
                );
            },
        }];

        return (
            <div>
                <Table
                    columns={columns}
                    dataSource={this.state.data}
                    pagination={false}
                    rowClassName={(record) => {
                        return record.editable ? 'editable' : '';
                    }}
                    locale={{ emptyText: 'No existe ningun tutor agregado' }}
                />
                <Button
                    style={{ width: '100%', marginTop: 16, marginBottom: 8 }}
                    type="dashed"
                    onClick={this.newCompany}
                    icon="plus"
                >
                    Agregar tutores
                </Button>

                <Snackbar
                    open={this.state.open}
                    message={this.state.message}
                    autoHideDuration={3000}
                    onRequestClose={this.handleRequestClose}
                />
            </div>
        );
    }
}
