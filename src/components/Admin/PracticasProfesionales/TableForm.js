import React, {PureComponent} from 'react';
import axios from 'axios';
import * as _ from 'lodash';

// Ant Design
import {Table, Button, Input, message, Divider, Icon, Badge} from 'antd';

// Material UI
import Snackbar from 'material-ui/Snackbar';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';

// URLS
import {URLS} from '../../../config';

const {Column} = Table;

const colorCustumized = 'rgba(244, 177, 63, 0.96)';

const styles = {
    inputs: {
        width: 100 + '%',
    },
};


export default class TableForm extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            data: props.value,
            dataSearch: props.value,
            open: false,
            message: '',
            filterDropdownVisible: false,
            searchText: '',
            filtered: false,
            previewVisible: false,
            previewImage: '',
            fileList: [{
                uid: -1,
                name: 'xxx.png',
                status: 'done',
                url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
            }],
            empresas: [],
            empresa: '',
            tutores: [],
            tutor: '',
            fechaInicial: '',
            fechaFinal: '',
            statesEdit: []

        };
    }

    componentWillMount() {
        message.config({
            top: 70,
            duration: 3,
        });
    }

    componentWillReceiveProps(nextProps) {
        if ('value' in nextProps) {
            this.setState({
                data: nextProps.value,
                dataSearch: nextProps.value,
            });
        }

        this.getEmpresas()
        this.getTutores()
    }

    getRowByKey(key) {
        return this.state.data.filter(item => item.id === key)[0];
    }

    index = 1;
    cacheOriginData = {};

    toggleEditable(e, key) {
        e.preventDefault();
        const target = this.state.dataSearch.filter(item => item.id === key)[0];

        const allStates = _.filter(this.state.statesEdit, state => state.key !== key);

        let currentState = {};
        let currentIndexState = _.findIndex(this.state.statesEdit, state => state.key === key);

        if (currentIndexState === -1) {
            currentState = {...{key: key}};
        } else {
            currentState = this.state.statesEdit[currentIndexState];
        }

        this.setState({
            statesEdit: allStates.concat(currentState)
        });


        if (target) {
            if (!target.editable) {
                this.cacheOriginData[key] = {...target};
            }

            target.editable = !target.editable;
            this.setState({data: [...this.state.data]});
            this.setState({dataSearch: [...this.state.dataSearch]});
        }
    }

    getEmpresas = (id) => {

        fetch(URLS.SERVER_URL + '/api/empresa/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
            .then(response => response.json())
            .then(data => {

                this.setState({
                    empresas: data,
                });

            })
            .catch(e => console.error('Ha ocurrido un error'));
    }

    getTutores = (id) => {

        fetch(URLS.SERVER_URL + '/api/tutor/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
            .then(response => response.json())
            .then(data => {

                this.setState({
                    tutores: data,
                });

            })
            .catch(e => console.error('Ha ocurrido un error'));
    }

    handleFieldEmpresa = (event, index, value) => {
        this.setState({empresa: value})
    }

    handleFieldTutor = (event, index, value) => {
        this.setState({tutor: value})
    }

    handleFieldState(e, fieldName, key) {

        const newData = [...this.state.data];
        const target = this.state.data.filter(item => item.id === key)[0];

        if (e.target.innerHTML === 'Pendiente') {

            target[fieldName] = 1;
            this.setState({data: newData});
        }

        if (e.target.innerHTML === 'Activo') {

            target[fieldName] = 4;
            this.setState({data: newData});
        }

        if (e.target.innerHTML === 'Finalizado') {

            target[fieldName] = 5;
            this.setState({data: newData});
        }
    }

    handleFieldChange(e, fieldName, key) {

        const newData = [...this.state.data];
        const target = this.state.data.filter(item => item.id === key)[0];

        target[fieldName] = e.target.value;
        this.setState({data: newData});

    }

    saveRow(e, key) {
        e.persist();

        const data = new FormData();
        const config = {
            headers: {'content-type': 'application/x-www-form-urlencoded'}
            //headers: { 'content-type': 'multipart/form-data' }
        }

        setTimeout(() => {
            if (document.activeElement.tagName === 'INPUT' &&
                document.activeElement !== e.target) {
                return;
            }

            if (this.clickedCancel) {
                this.clickedCancel = false;
                return;
            }

            const target = this.getRowByKey(key);

            data.append('estado', target.estado);
            data.append('empresa', this.state.empresa);
            data.append('tutor', this.state.tutor);
            data.append('fecha_inicio', target.fecha_inicio || '');
            data.append('fecha_final', target.fecha_final || '');
            data.append('nota', target.nota || '');


            axios.put(URLS.SERVER_URL + '/api/practicaprofesionalput/' + target.id + '/', data, config)
                .then((response) => {

                    this.setState({
                        open: true,
                        message: 'Practica profesional actualizada exitosamente'
                    });


                })
                .catch((err) => {
                    this.setState({
                        open: true,
                        message: 'No se ha podido actualizar la practica profesional'
                    });
                });

            this.toggleEditable(e, key);
            this.props.onChange(this.state.data);

        }, 10);
    }

    cancel(e, key) {
        e.preventDefault();
        this.clickedCancel = true;
        const target = this.state.dataSearch.filter(item => item.id === key)[0];

        if (this.cacheOriginData[key]) {
            Object.assign(target, this.cacheOriginData[key]);
            target.editable = false;
            delete this.cacheOriginData[key];
        }

        this.setState({data: [...this.state.data]});
        this.setState({dataSearch: [...this.state.dataSearch]});
    }

    handleRequestClose = () => {
        this.setState({
            open: false,
            message: ''
        });
    };

    onInputChange = (e) => {
        this.setState({searchText: e.target.value});
    }

    onSearch = () => {
        const {searchText, data} = this.state;
        const reg = new RegExp(searchText, 'gi');

        this.setState({
            filterDropdownVisible: false,
            filtered: !!searchText,
            dataSearch: data.map((record) => {
                const match = record.solicitud.persona.nombre_completo.match(reg);
                if (!match) {
                    return null;
                }
                return {
                    ...record,
                    solicitud: {
                        ...record.solicitud,
                        persona: {
                            ...record.solicitud.persona,
                            nombre_completo: (
                                <span> {record.solicitud.persona.nombre_completo.split(reg).map((text, i) => ( i > 0 ? [
                                    <span
                                        className="highlight">{match[0]}</span>, text] : text ))} </span>)
                        }
                    }
                }
            }).filter(record => !!record),
        });
    };

    handleStates = (key, fieldName) => {
        const allStates = _.filter(this.state.statesEdit, state => state.key !== key);
        let currentIndex = _.findIndex(this.state.statesEdit, state => state.key === key);
        let currentState = this.state.statesEdit[currentIndex];

        const newData = [...this.state.data];
        const target = this.state.data.filter(item => item.id === key)[0];

        return (event, date) => {
            currentState = {
                ...currentState,
                [fieldName]: (date? this.formatDate(date): null)
            };

            this.setState({
                statesEdit: allStates.concat(currentState),
                data: newData,
            });

            target[fieldName] = currentState[fieldName];
        };

    };

    handleFechaInicial = (key, fieldName) => {
        const allStates = _.filter(this.state.statesEdit, state => state.key !== key);
        let currentIndex = _.findIndex(this.state.statesEdit, state => state.key === key);
        let currentState = this.state.statesEdit[currentIndex];

        const newData = [...this.state.data];
        const target = this.state.data.filter(item => item.id === key)[0];

        return (event, date) => {
            currentState = {
                ...currentState,
                fecha_inicio: (date? this.formatDate(date): null)
            };

            this.setState({
                statesEdit: allStates.concat(currentState),
                data: newData,
            });

            target[fieldName] = currentState.fecha_inicio;
        };

    };

    handleFechaFinal = (event, date) => {
        this.setState({fechaFinal: this.formatDate(date)});

    };

    formatDate = (date) => {

        if (typeof date === "string") {
            return date;
        }

        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    }

    render() {
        return (
            <div>
                <Table
                    style={{marginBottom: 16}}
                    pagination={false}
                    rowClassName={(record) => {
                        return record.editable ? 'editable' : '';
                    }}
                    loading={this.state.loading}
                    dataSource={this.state.dataSearch}
                    locale={{emptyText: 'No existe ninguna practica profesional en curso'}}
                >
                    <Column
                        title="Practicante"
                        dataIndex="solicitud.persona.nombre_completo"
                        key="solicitud.persona.nombre_completo"
                        width={'12%'}
                        filterDropdown={
                            <div className="custom-filter-dropdown">
                                <Input
                                    ref={ele => this.searchInput = ele}
                                    placeholder="Buscar por nombre"
                                    value={this.state.searchText}
                                    onChange={this.onInputChange}
                                    onPressEnter={this.onSearch}
                                />
                                <Button type="primary" onClick={this.onSearch}>Buscar</Button>
                            </div>
                        }

                        filterIcon={<Icon type="search" style={{color: this.state.filtered ? '#108ee9' : '#aaa'}}/>}
                        filterDropdownVisible={this.state.filterDropdownVisible}
                        onFilterDropdownVisibleChange={(visible) => {
                            this.setState({
                                filterDropdownVisible: visible,
                            }, () => this.searchInput && this.searchInput.focus());
                        }}
                    />

                    <Column
                        title="Estado"
                        dataIndex="estado"
                        key="estado"
                        width={'12%'}
                        render={(text, record) => {

                            if (record.editable) {
                                return (
                                    <SelectField
                                        value={record.estado}
                                        floatingLabelText="Estado"
                                        onChange={e => this.handleFieldState(e, 'estado', record.id)}
                                        style={styles.inputs}
                                        selectedMenuItemStyle={{color: colorCustumized}}
                                    >
                                        <MenuItem value={1} primaryText="Pendiente"/>
                                        <MenuItem value={4} primaryText="Activo"/>
                                        <MenuItem value={5} primaryText="Finalizado"/>

                                    </SelectField>

                                );
                            }

                            if (text === 1) {
                                return (<Badge status="processing" text="Pendiente"/>);
                            }

                            if (text === 4) {
                                return (<Badge status="success" text="Activo"/>);
                            }

                            if (text === 5) {
                                return (<Badge status="success" text="Finalizado"/>);
                            }


                        }}
                    />

                    <Column
                        title={'Empresa'}
                        dataIndex={'empresa.nombre'}
                        key={'empresa.nombre'}
                        width={'12%'}
                        render={(text, record) => {
                            if (record.editable) {
                                return (
                                    <SelectField
                                        value={this.state.empresa}
                                        floatingLabelText="Empresa"
                                        onChange={this.handleFieldEmpresa}
                                        style={{width:'122px'}}
                                        selectedMenuItemStyle={{color: colorCustumized}}
                                    >
                                        {
                                            this.state.empresas.map((empresa) => {
                                                return (<MenuItem value={empresa.id} primaryText={empresa.nombre}/>)
                                            })
                                        }
                                    </SelectField>

                                );
                            }

                            if (!text) {
                                return 'Sin asignar'
                            } else {
                                return text
                            }

                        }}
                    />

                    <Column
                        title={'Tutor'}
                        dataIndex={'tutor.nombre_completo'}
                        key={'tutor.nombre_completo'}
                        width={'12%'}
                        render={(text, record) => {
                            if (record.editable) {
                                return (
                                    <SelectField
                                        value={this.state.tutor}
                                        floatingLabelText="Tutor"
                                        onChange={this.handleFieldTutor}
                                        style={{width:'122px'}}
                                        selectedMenuItemStyle={{color: colorCustumized}}
                                    >
                                        {
                                            this.state.tutores.map((tutor) => {
                                                return (<MenuItem value={tutor.id} primaryText={tutor.nombre + ' ' + tutor.apellido}/>)
                                            })
                                        }
                                    </SelectField>

                                );
                            }

                            if (!text) {
                                return 'Sin asignar'
                            } else {
                                return text
                            }

                        }}
                    />

                    <Column
                        title={'Fecha inicial'}
                        dataIndex={'fecha_inicio'}
                        key={'fecha_inicio'}
                        width={'13%'}
                        render={(text, record) => {

                            let currentIndex = _.findIndex(this.state.statesEdit, state => state.key === record.id);
                            let currentState = this.state.statesEdit[currentIndex];

                            if (record.editable) {
                                return (
                                    <DatePicker
                                        hintText="Fecha inicial"
                                        floatingLabelText="Fecha inicial"
                                        formatDate={this.formatDate}
                                        textFieldStyle={styles.inputs}
                                        mode="landscape"
                                        locale='es-ES'
                                        value={currentState.fecha_inicio ? new Date(currentState.fecha_inicio) : text}
                                        onChange={this.handleStates(record.id, 'fecha_inicio')}
                                    />

                                );
                            }

                            if (!text) {
                                return 'Sin asignar'
                            } else {
                                return text
                            }
                        }}
                    />

                    <Column
                        title={'Fecha final'}
                        dataIndex={'fecha_final'}
                        key={'fecha_final'}
                        width={'12%'}
                        render={(text, record) => {

                            let currentIndex = _.findIndex(this.state.statesEdit, state => state.key === record.id);
                            let currentState = this.state.statesEdit[currentIndex];
                           
                            if (record.editable) {
                                return (
                                    <DatePicker
                                        hintText="Fecha final"
                                        floatingLabelText="Fecha final"
                                        formatDate={this.formatDate}
                                        textFieldStyle={styles.inputs}
                                        mode="landscape"
                                        locale='es-ES'
                                        value={currentState.fecha_final ? new Date(currentState.fecha_final) : text}
                                        onChange={this.handleStates(record.id, 'fecha_final')}
                                    />

                                );
                            }

                            if (!text) {
                                return 'Sin asignar'
                            } else {
                                return text
                            }
                        }}
                    />

                    <Column
                        title={'Nota'}
                        dataIndex={'nota'}
                        key={'nota'}
                        width={'10%'}
                        render={(text, record) => {
                            if (record.editable) {
                                return (
                                    <TextField
                                        floatingLabelText="Nota"
                                        type="text"
                                        onChange={e => this.handleFieldChange(e, 'nota', record.id)}
                                        style={styles.inputs}
                                    />

                                );
                            }

                            if (!text) {
                                return 'Sin asignar'
                            } else {
                                return text
                            }

                        }}
                    />

                    <Column
                        title='Operacion'
                        key='action'
                        width={'70%'}
                        render={(text, record) => {
                            if (record.editable) {
                                if (record.isNew) {
                                    return (
                                        <span>
                                    <a href="javascript:void(0);" onClick={e => this.saveRow(e, record.id)}>Guardar</a>
                                </span>
                                    );
                                }
                                return (
                                    <span>
                                <a href="javascript:void(0);" onClick={e => this.saveRow(e, record.id)}>Guardar</a>
                            <Divider type="vertical"/>
                                <a href="javascript:void(0);" onClick={e => this.cancel(e, record.id)}>Cancelar</a>
                            </span>
                                );
                            }

                            return (
                                <span>
                            <a href="javascript:void(0);" onClick={e => this.toggleEditable(e, record.id)}>Asignar</a>
                        </span>
                            );
                        }}
                    />

                </Table>

                <Snackbar
                    open={this.state.open}
                    message={this.state.message}
                    autoHideDuration={3000}
                    onRequestClose={this.handleRequestClose}
                />
            </div>
        );
    }
}
