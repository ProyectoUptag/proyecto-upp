import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TableForm from './TableForm';
import './PracticasProfesionales.less';

// Ant Design
import {Card, Form} from 'antd';

// URLS
import {URLS} from '../../../config';

class PracticasProfesionales extends Component {
    constructor() {
        super();
        this.state = {
            fixedHeader: true,
            fixedFooter: true,
            stripedRows: false,
            showRowHover: true,
            selectable: false,
            multiSelectable: false,
            enableSelectAll: false,
            deselectOnClickaway: false,
            showCheckboxes: false,
            height: '300px',
            dataSearch: [],
            tableData: [],
            loading: false,
            filterDropdownVisible: false,
            searchText: '',
            filtered: false,
        }

    }

    componentWillMount() {

        const add_full_name = (obj) => {
            if (obj.tutor !== null){
                return {
                    ...obj,
                    tutor: {
                        ...obj.tutor,
                        nombre_completo: `${obj.tutor.nombre} ${obj.tutor.apellido}`
                    },
                    solicitud: {
                        ...obj.solicitud,
                        persona: {
                            ...obj.persona,
                            nombre_completo: `${obj.solicitud.persona.primer_nombre} ${obj.solicitud.persona.primer_apellido}`
                        }
                    },
                }
            }else{
                return {
                    ...obj,
                    solicitud: {
                        ...obj.solicitud,
                        persona: {
                            ...obj.persona,
                            nombre_completo: `${obj.solicitud.persona.primer_nombre} ${obj.solicitud.persona.primer_apellido}`
                        }
                    },
                }
            }
        };

        fetch(URLS.SERVER_URL + '/api/practicaprofesional/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
            .then(response => response.json())
            .then(data => {
                const new_data = data.map(add_full_name);

                this.setState({
                    tableData: new_data,
                    dataSearch: new_data,
                    loading: false
                });

            })
            .catch(e => console.error('Ha ocurrido un error'));
    }

    componentWillUnmount() {
        const route = this.context.router.history.location.pathname;

        if (route === '/admin/practicas-profesionales') {
            this.context.router.history.push('/admin/practicas-profesionales/practicas', null);
        }
    }

    render() {

        const { form } = this.props;
        const { getFieldDecorator } = form;
        const { tableData } = this.state;

        return (

            <div>
                <Card title="Practicas Profesionales" className={'card-antd'} bordered={false}>
                    <div className={'card-antd-content'}>
                        {getFieldDecorator('PracticasProfesionales', {
                            initialValue: tableData,
                        })(<TableForm />)}


                    </div>
                </Card>
            </div>
        );
    }
}

export default Form.create()(PracticasProfesionales);


PracticasProfesionales.contextTypes = {
    router: PropTypes.object.isRequired
};