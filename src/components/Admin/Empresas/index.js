import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TableForm from './TableForm';
import './Empresas.less';

// Ant Design
import { Card, Form } from 'antd';

// URLS
import { URLS } from '../../../config';

class Empresas extends Component {
    constructor(){
        super();
        this.state ={
            tableData: [],
            loading: true,
        }

    }

    componentWillMount() {

        fetch(URLS.SERVER_URL + '/api/empresa/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
        .then( response => response.json())
        .then( data => {

            this.setState({
                tableData: data,
                loading: false
            })

        }) 
        .catch( e => console.error( 'Ha ocurrido un error' ));
    }

    componentWillUnmount() {
        const route = this.context.router.history.location.pathname;

        if (route === '/admin/empresas'){
            this.context.router.history.push('/admin/empresas', null);
        }
    }

    render() {
        const { form } = this.props;
        const { getFieldDecorator } = form;
        const { tableData } = this.state;

        return (

            <div>
                <Form onSubmit={this.handleSubmit} layout="vertical" hideRequiredMark>
                    <Card title="Empresas" className={'card-antd'} bordered={false}>
                        <div className={'card-antd-content'}>

                            {getFieldDecorator('members', {
                                initialValue: tableData,
                            })(<TableForm loading={this.state.loading}/>)}

                        </div>
                    </Card>
                </Form> 
            </div>
        );
    }
}

export default Form.create()(Empresas);

Empresas.contextTypes = {
    router: PropTypes.object.isRequired
};