const routes = {
  '/admin': 'Inicio',
  '/admin/solicitudes': 'Solicitudes',
  '/admin/solicitudes/historial': 'Historial',
  '/admin/practicas-profesionales': 'Practicas Profesionales',
  '/admin/practicas-profesionales/practicas': 'Practicas en curso',
  '/admin/empresas': 'Empresas',
  '/admin/tutores': 'Tutores',
  '/admin/estadisticas': 'Estadisticas',
  '/admin/administrar': 'Administrar',
  '/admin/administrar/alumnos': 'Alumnos',
  '/admin/administrar/reportes': 'Reportes',

};
export default routes;
