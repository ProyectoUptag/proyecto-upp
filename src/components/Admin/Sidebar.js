import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

class Sidebar extends Component {

  handleClick(e) {
    e.preventDefault();
    e.target.parentElement.classList.toggle('open');
  }

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
  }

  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
  // }

  render() {
    return (
      <div className="sidebar">
        <nav className="sidebar-nav">
          <ul className="nav">
            <li className="nav-item">
              <NavLink to={'/admin'} className="nav-link" activeClassName="active"><i className="icon-speedometer"></i>Inicio</NavLink>
            </li>
            <li className="nav-title">
              MENUS
            </li>
            <li className={this.activeRoute("/admin/solicitudes")}>
              <a className="nav-link nav-dropdown-toggle" href="" onClick={this.handleClick.bind(this)}><i className="fa fa-mortar-board"></i> Solicitudes </a>
              <ul className="nav-dropdown-items">
                <li className="nav-item">
                  <NavLink to={'/admin/solicitudes/historial'} className="nav-link" activeClassName="active"><i className="fa fa-group"></i> Historial</NavLink>
                </li>
              </ul>
            </li>
            <li className={this.activeRoute("/admin/practicas-profesionales")}>
              <a className="nav-link nav-dropdown-toggle" href="" onClick={this.handleClick.bind(this)}><i className="fa fa-book"></i> Practicas <span style={{marginLeft: 30}}>Profesionales</span> </a>
              <ul className="nav-dropdown-items">
                <li className="nav-item">
                  <NavLink to={'/admin/practicas-profesionales/practicas'} className="nav-link" activeClassName="active"><i className="fa fa-pencil"></i> Practicas en curso </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <NavLink to={'/admin/empresas'} className="nav-link nav-link-item" activeClassName="active"><i className="fa fa-institution"></i> Empresas </NavLink>
            </li>            
            <li className="nav-item">
              <NavLink to={'/admin/tutores'} className="nav-link nav-link-item" activeClassName="active"><i className="fa fa-bookmark"></i> Tutores </NavLink>
            </li>
            {/*<li className="nav-item">*/}
              {/*<NavLink to={'/admin/estadisticas'} className="nav-link nav-link-item" activeClassName="active"><i className="fa fa-bar-chart"></i> Estadisticas</NavLink>*/}
            {/*</li>*/}
            {/*<li className="divider"></li>*/}

            {/*<li className="nav-title">*/}
              {/*Extras*/}
            {/*</li>*/}

            {/*<li className={this.activeRoute("/admin/administrar")}>*/}
              {/*<a className="nav-link nav-dropdown-toggle" href="" onClick={this.handleClick.bind(this)}><i className="fa fa-book"></i> Administrar</a>*/}
              {/*<ul className="nav-dropdown-items">*/}
                {/*<li className="nav-item">*/}
                  {/*<NavLink to={'/admin/administrar/alumnos'} className="nav-link" activeClassName="active"><i className="fa fa-database"></i> Consultar Alumnos </NavLink>*/}
                {/*</li>*/}
                {/*<li className="nav-item">*/}
                  {/*<NavLink to={'/admin/administrar/reportes'} className="nav-link" activeClassName="active"><i className="fa fa-sticky-note"></i> Reportes </NavLink>*/}
                {/*</li>*/}
              {/*</ul>*/}
            {/*</li>*/}
          </ul>
        </nav>
      </div>
    )
  }
}

export default Sidebar;
