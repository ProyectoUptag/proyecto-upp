import React, { PureComponent } from 'react';
import axios from 'axios';

// Ant Design
import { Table, Button, Input, message, Divider, Icon, Badge, Upload, Modal } from 'antd';

// Material UI
import Snackbar from 'material-ui/Snackbar';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

// URLS
import { URLS } from '../../../config';

const {Column} = Table;

const colorCustumized = 'rgba(244, 177, 63, 0.96)';

const styles = {
    inputs: {
        width: 100 + '%',
    },
};


export default class TableForm extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            data: props.value,
            dataSearch: props.value,
            open: false,
            message: '',
            filterDropdownVisible: false,
            searchText: '',
            filtered: false,
            previewVisible: false,
            previewImage: '',
            archivos: [],
        };
    }

    componentWillMount(){
        message.config({
            top: 70,
            duration: 3,
        });
    }

    componentWillReceiveProps(nextProps) {
        if ('value' in nextProps) {
            this.setState({
                data: nextProps.value,
                dataSearch: nextProps.value,
            });
        }
    }

    handleCancel = () => this.setState({ previewVisible: false })

    handlePreview = (file) => {
        this.setState({
            previewImage: file.url,
            previewVisible: true,
        });
    }

    handleChange = ({ fileList }) => this.setState({ fileList })

    getRowByKey(key) {
        return this.state.data.filter(item => item.id === key)[0];
    }

    index = 0;
    cacheOriginData = {};


    toggleEditable(e, key) {
        e.preventDefault();
        const target = this.state.dataSearch.filter(item => item.id === key)[0];

        if (target) {
            if (!target.editable) {
                this.cacheOriginData[key] = { ...target };
            }

            target.editable = !target.editable;
            this.setState({ data: [...this.state.data] });
            this.setState({ dataSearch: [...this.state.dataSearch] });
        }
    }

    handleFieldChange(e, fieldName, key) {

        const newData = [...this.state.data];
        const target = this.state.data.filter(item => item.id === key)[0];

        if(e.target.innerHTML === 'Pendiente'){

            target[fieldName] = 1;
            this.setState({ data: newData });
        }

        if(e.target.innerHTML === 'Aceptado'){

            target[fieldName] = 2;
            this.setState({ data: newData });
        }

        if(e.target.innerHTML === 'Rechazado'){

            target[fieldName] = 3;
            this.setState({ data: newData });
        }
    }

    saveRow(e, key) {
        e.persist();

        const data = new FormData();
        const config = {
            headers: { 'content-type': 'application/x-www-form-urlencoded' }
        //headers: { 'content-type': 'multipart/form-data' }
        }

        setTimeout(() => {
            if (document.activeElement.tagName === 'INPUT' &&
                document.activeElement !== e.target) {
                return;
            }

            if (this.clickedCancel) {
                this.clickedCancel = false;
                return;
            }

            const target = this.getRowByKey(key);

            data.append('estado',target.estado);

            axios.put(URLS.SERVER_URL + '/api/solicitud/' + target.id + '/', data, config)
                .then((response) => {

                    this.setState({
                        open: true,
                        message: 'El estado ha sido actualizado exitosamente'
                    });


                })
                .catch((err) => {
                    this.setState({
                        open: true,
                        message: 'No se ha podido actualizar el estado'
                    });
                });

            this.toggleEditable(e, key);
            this.props.onChange(this.state.data);


        }, 10);
    }

    cancel(e, key) {
        e.preventDefault();
        this.clickedCancel = true;
        const target = this.state.dataSearch.filter(item => item.id === key)[0];

        if (this.cacheOriginData[key]) {
            Object.assign(target, this.cacheOriginData[key]);
            target.editable = false;
            delete this.cacheOriginData[key];
        }

        this.setState({ data: [...this.state.data] });
        this.setState({ dataSearch: [...this.state.dataSearch] });
    }

    handleRequestClose = () => {
        this.setState({
            open: false,
            message: ''
        });
    };

    handleDownload = (id) => {

        return () => {
            window.location.href = '/report/solicitud/' + id;

        }
    }

    onInputChange = (e) => {
        this.setState({searchText: e.target.value});
    }

    onSearch = () => {
        const {searchText, data} = this.state;
        const reg = new RegExp(searchText, 'gi');

        this.setState({
            filterDropdownVisible: false,
            filtered: !!searchText,
            dataSearch: data.map((record) => {
                const match = record.persona.nombre_completo.match(reg);
                if (!match) {
                    return null;
                }
                return {
                    ...record,
                    persona: {
                        ...record.persona,
                        nombre_completo: (
                            <span> {record.persona.nombre_completo.split(reg).map((text, i) => ( i > 0 ? [<span
                                className="highlight">{match[0]}</span>, text] : text ))} </span>)
                    }
                }
            }).filter(record => !!record),
        });
    };

    render() {
        const { previewVisible, previewImage, data } = this.state;

        const newData = data.map((data) =>{
            const newData = {...data};

            return  newData.archivos.map(e => {
                let ext = (e.file.substring(e.file.lastIndexOf("."))).toLowerCase();

                if (e.id) {
                    e.uid = e.id;
                }

                if(ext === '.pdf'){
                    e.thumbUrl = 'static/img/icon_pdf.png';
                }
                return e
            })
        })

        return (

            <div>
                <Table
                    dataSource={this.state.dataSearch}
                    pagination={false}
                    rowClassName={(record) => {
                        console.log(newData)
                        return record.editable ? 'editable' : '';
                    }}
                    expandedRowRender={record =>(
                            <div className="clearfix">
                                <Upload
                                    listType="picture-card"
                                    fileList={newData[record.id-1]}
                                    onPreview={this.handlePreview}
                                    showUploadList={{'showRemoveIcon': false}}
                                >
                                </Upload>

                                <Modal width={600} visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                                    <object width={'100%'} height={500} data={previewImage}></object>
                                </Modal>
                            </div>
                        )

                    }
                    locale={{emptyText: 'No existe ninguna solicitud'}}
                >
                    <Column
                        title="Practicante"
                        dataIndex="persona.nombre_completo"
                        key="persona.nombre_completo"
                        width={'18%'}
                        filterDropdown={
                            <div className="custom-filter-dropdown">
                                <Input
                                    ref={ele => this.searchInput = ele}
                                    placeholder="Buscar por nombre"
                                    value={this.state.searchText}
                                    onChange={this.onInputChange}
                                    onPressEnter={this.onSearch}
                                />
                                <Button type="primary" onClick={this.onSearch}>Buscar</Button>
                            </div>
                        }

                        filterIcon={<Icon type="search" style={{color: this.state.filtered ? '#108ee9' : '#aaa'}}/>}
                        filterDropdownVisible={this.state.filterDropdownVisible}
                        onFilterDropdownVisibleChange={(visible) => {
                            this.setState({
                                filterDropdownVisible: visible,
                            }, () => this.searchInput && this.searchInput.focus());
                        }}
                    />


                    <Column
                        title="Estado"
                        dataIndex="estado"
                        key="estado"
                        width={'15%'}
                        render={(text, record) => {

                            if (record.editable) {
                                return (
                                    <SelectField
                                        value={record.estado}
                                        floatingLabelText="Estado"
                                        onChange={e => this.handleFieldChange(e, 'estado', record.id)}
                                        style={styles.inputs}
                                        selectedMenuItemStyle={{color: colorCustumized}}
                                    >
                                        <MenuItem value={1} primaryText="Pendiente" />
                                        <MenuItem value={2} primaryText="Aceptado" />
                                        <MenuItem value={3} primaryText="Rechazado" />

                                    </SelectField>

                                );
                            }

                            if(text === 1){
                                return (<Badge status="processing" text="Pendiente" />);
                            }

                            if(text === 2){
                                return (<Badge status="success" text="Aceptado" />);
                            }

                            if(text === 3){
                                return (<Badge status="error" text="Rechazado" />);
                            }
                        }}
                    />

                    <Column
                        title="PNF"
                        dataIndex="persona.datos_universitarios.pnf"
                        key="persona.datos_universitarios.pnf"
                        width={'15%'}
                    />

                    <Column
                        title="Fecha"
                        dataIndex="fecha"
                        key="fecha"
                        width={'18%'}
                    />

                    <Column
                        title="Reporte"
                        dataIndex="id"
                        key="download"
                        width={'15%'}
                        render={(id) => (
                            <Button type="primary" shape="circle" icon="download" size={'default'} onClick={this.handleDownload(id)}/>
                        )}
                    />

                    <Column
                        title='Operacion'
                        key='action'
                        width={'20%'}
                        render={(text, record) => {
                            if (record.editable) {
                                if (record.isNew) {
                                    return (
                                        <span>
                                            <a onClick={e => this.saveRow(e, record.id)}>Guardar</a>
                                        </span>
                                    );
                                }
                                return (
                                    <span>
                                        <a href="javascript:void(0);" onClick={e => this.saveRow(e, record.id)}>Guardar</a>
                                    <Divider type="vertical" />
                                        <a href="javascript:void(0);" onClick={e => this.cancel(e, record.id)}>Cancelar</a>
                                    </span>
                                );
                            }

                            return (
                                <span>
                                    <a href="javascript:void(0);" onClick={e => this.toggleEditable(e, record.id)}>Cambiar estado</a>
                                </span>
                            );
                        }}
                    />

                </Table>

                <Snackbar
                    open={this.state.open}
                    message={this.state.message}
                    autoHideDuration={3000}
                    onRequestClose={this.handleRequestClose}
                />
            </div>
        );
    }
}
