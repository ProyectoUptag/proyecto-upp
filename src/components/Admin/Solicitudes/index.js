import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TableForm from './TableForm';
import './Solicitudes.less';

// Ant Design
import {Card, Form} from 'antd';

// URLS
import {URLS} from '../../../config';

class Solicitudes extends Component {
    constructor() {
        super();
        this.state = {
            dataSearch: [],
            tableData: [],
            loading: false,
            searchText: '',
        }

    }

    componentWillMount() {

        const add_full_name = (obj) => {
            return {
                ...obj,
                persona: {
                    ...obj.persona,
                    nombre_completo: `${obj.persona.primer_nombre} ${obj.persona.primer_apellido}`
                }
            }
        };

        fetch(URLS.SERVER_URL + '/api/solicitud/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
        .then( response => response.json())
        .then( data => {

            const new_data = data.map(add_full_name);

            this.setState({
                tableData: new_data,
                dataSearch: new_data,
                loading: false
            })

        })
        .catch( e => console.error( 'Ha ocurrido un error' ));
    }

    componentWillUnmount() {
        const route = this.context.router.history.location.pathname;

        if (route === '/admin/solicitudes') {
            this.context.router.history.push('/admin/solicitudes/historial', null);
        }
    }

    onInputChange = (e) => {
        this.setState({searchText: e.target.value});
    }

    handleDownload = (id) => {

        return () => {
            window.location.href = '/report/solicitud/' + id;

        }
    }

    render() {

        const { form } = this.props;
        const { getFieldDecorator } = form;
        const { tableData } = this.state;

        return (

            <div>
                <Card title="Historial de Solicitudes a la Practica Profesional" className={'card-antd'} bordered={false}>
                    <div className={'card-antd-content'}>
                        {getFieldDecorator('solcitudes', {
                            initialValue: tableData,
                        })(<TableForm />)}

                    </div>
                </Card>
            </div>
        );
    }
}

export default Form.create()(Solicitudes);


Solicitudes.contextTypes = {
    router: PropTypes.object.isRequired
};