import React, { Component } from 'react';
import { Row, Col, Card, Radio, Avatar } from 'antd';
import './Estadisticas.less';
import Pie from "./Charts/Pie/index";
import numeral from 'numeral';

// URLS
import {URLS} from '../../../config';

export default class Analysis extends Component {
    state = {
        loading: true,
        salesType: 'genero',
        currentTabKey: '',
        rangePickerValue: [],
        genero: [],
        estado: [],
        ciudad: []
    }

    componentDidMount() {

        fetch(URLS.SERVER_URL + '/api/practicaprofesional/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
            .then( response => response.json())
            .then( data => {
                let masculino = data.filter(e => e.solicitud.persona.sexo === 1)
                let femenino = data.filter(e => e.solicitud.persona.sexo === 2)

                let estado_pendiente = data.filter(e => e.estado === 1)
                let estado_activo = data.filter(e => e.estado === 4)
                let estado_finalizado = data.filter(e => e.estado === 5)

                let coro = data.filter(e => e.solicitud.persona.datos_direccion.ciudad_origen === 'coro' || e.solicitud.persona.datos_direccion.ciudad_origen === 'Coro')
                let otro_ciudad = data.filter(e => e.solicitud.persona.datos_direccion.ciudad_origen !== 'coro' && e.solicitud.persona.datos_direccion.ciudad_origen !== 'Coro')

                this.setState({
                    genero: [
                        {
                            x: 'Femenino',
                            y: femenino.length,
                        },
                        {
                            x: 'Masculino',
                            y: masculino.length,
                        }
                    ],
                    estado: [
                        {
                            x: 'Pendiente',
                            y: estado_pendiente.length,
                        },
                        {
                            x: 'Activo',
                            y: estado_activo.length,
                        },
                        {
                            x: 'Finalizado',
                            y: estado_finalizado.length,
                        },
                    ],
                    ciudad: [
                        {
                            x: 'Coro',
                            y: coro.length,
                        },
                        {
                            x: 'Otro',
                            y: otro_ciudad.length
                        },
                    ],
                    loading: false
                })

            })
            .catch( e => console.error( 'Ha ocurrido un error' ));
    }

    componentWillUnmount() {

    }

    handleChangeSalesType = (e) => {
        this.setState({
            salesType: e.target.value,
        });
    }

    render() {
        const yuan = val => `${numeral(val).format('0,0')} estudiantes`;

        const formatNumber = {
            separador: ".", // separador para los miles
            sepDecimal: ',', // separador para los decimales
            formatear:function (num){
                num +='';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                }
                return this.simbol + splitLeft +splitRight;
            },
            new:function(num, simbol){
                this.simbol = simbol ||'';
                return this.formatear(num);
            }
        }
        const { salesType, loading } = this.state;
        const { genero, estado, ciudad } = this.state;
        const salesPieData = salesType === 'genero' ? genero : (salesType === 'estado' ? estado : ciudad);

        return (
            <div>
                <div style={{ padding: 24, background: '#fff' }}>

                    <div className={'pageHeaderContent'}>

                        <div className={"avatar"}>
                            <Avatar size="large" src={ 'static/img/user.png' } />
                        </div>

                        <div className={'content'}>
                            <p className={'contentTitle'}>Bienvenido Administrador </p>
                            <p>Unidad de Practicas Profesionales | PNFI - PNFC - PNFA - PNFM - PNFQ</p>
                        </div>
                    </div>

                </div>

                <Row gutter={24}>
                    <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                        <Card
                            loading={loading}
                            className={'salesCard'}
                            bordered={false}
                            title="Estadisticas de la Unidad de Practicas Profesionales"
                            extra={(
                                <div className={'salesCardExtra'}>
                                    <div className={'salesTypeRadio'}>
                                        <Radio.Group value={salesType} onChange={this.handleChangeSalesType}>
                                            <Radio.Button value="genero">Genero</Radio.Button>
                                            <Radio.Button value="estado">Estado</Radio.Button>
                                            <Radio.Button value="ciudad">Ciudad</Radio.Button>

                                        </Radio.Group>
                                    </div>
                                </div>
                            )}
                            style={{ marginTop: 24 }}
                        >
                            <h4 style={{ marginTop: 8, marginBottom: 32 }}>Estadisticas</h4>
                            <div style={{ marginBottom: 57 }}>
                                <Pie
                                    hasLegend
                                    subTitle="Estudiantes"
                                    total={formatNumber.new(salesPieData.reduce((pre, now) => now.y + pre, 0))}
                                    data={salesPieData}
                                    valueFormat={val => yuan(val)}
                                    height={240}
                                    lineWidth={4}
                                />
                            </div>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}
