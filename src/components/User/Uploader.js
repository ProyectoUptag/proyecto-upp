import React, { Component } from 'react';

// Ant Disign
import { Upload, Icon, Modal } from 'antd';
// import PDF from 'react-pdf-js';

// URLS
import { URLS } from '../../config';

class Uploader extends Component {

  state = {
    previewVisible: false,
    previewImage: '',
  };

  handleCancel = () => this.setState({ previewVisible: false })

  handlePreview = (file) => {
    this.setState({
      previewImage: file.url,
      previewVisible: true,
    });
  }

  render() {

    const { previewVisible, previewImage } = this.state;

    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Subir</div>
      </div>
    );

    return (
      <div>
        <Upload onPreview={this.handlePreview} listType="picture-card" action={URLS.SERVER_URL + '/api/archivo/'} fileList={this.props.fileList} onRemove={this.props.onRemove} beforeUpload={this.props.beforeUpload} onChange={this.props.onChange} multiple={this.props.multiple} style={{marginTop: '100'}}>
            
            {this.props.fileList.length >= 6 ? null : uploadButton}

        </Upload>

        <Modal title="Vista Previa" width={600} visible={previewVisible} footer={null} onCancel={this.handleCancel}>
          
          {/*<PDF file={previewImage} />*/}


          <object width={'100%'} height={500} data={previewImage}></object>
        </Modal>

      </div>
  
    )
  }
}

export default Uploader;