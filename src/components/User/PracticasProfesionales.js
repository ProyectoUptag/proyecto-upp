import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Ant Design
import { Card, Table, Badge } from 'antd';

// URLS
import { URLS } from '../../config';

const { Column } = Table;

class Practicas extends Component {
    constructor(){
        super();
        this.state ={
            fixedHeader: true,
            fixedFooter: true,
            stripedRows: false,
            showRowHover: true,
            selectable: false,
            multiSelectable: false,
            enableSelectAll: false,
            deselectOnClickaway: false,
            showCheckboxes: false,
            height: '300px',
            tableData: [],
            loading: true
        }

    }

    componentWillMount() {

        const validateObj = (obj) => {
            if (obj.tutor !== null){
                return {
                    ...obj,
                    tutor: {
                        ...obj.tutor,
                        nombre_completo: `${obj.tutor.nombre} ${obj.tutor.apellido}`
                    },
                    solicitud: {
                        ...obj.solicitud,
                        persona: {
                            ...obj.persona,
                            nombre_completo: `${obj.solicitud.persona.primer_nombre} ${obj.solicitud.persona.primer_apellido}`
                        }
                    },
                }
            }else{
                return {
                    ...obj,
                    solicitud: {
                        ...obj.solicitud,
                        persona: {
                            ...obj.persona,
                            nombre_completo: `${obj.solicitud.persona.primer_nombre} ${obj.solicitud.persona.primer_apellido}`
                        }
                    },
                }
            }
        };

        let user_data = JSON.parse(localStorage.user_data);

        fetch(URLS.SERVER_URL + '/api/practicaprofesional/?user_id=' + user_data.id, {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
        .then( response => response.json())
        .then( data => {
            const new_data = data.map(validateObj);

            this.setState({
                tableData: new_data,
                loading: false
            })

        }) 
        .catch( e => console.error( 'Ha ocurrido un error' ));
    }

    componentWillUnmount() {
        const route = this.context.router.history.location.pathname;

        if (route === '/user/practicas-profesionales'){
            this.context.router.history.push('/user/practicas-profesionales/cursando', null);
        }
    }

    handleDownload = (id) => {

        return () =>{
            window.location.href = '/report/solicitud/' + id;
            
        }
    }

    render() {

        return (

            <div>
                <div style={{ padding: 24, paddingLeft: 0, background: '#fff'}}>

                    <div className={'pageHeaderContent'}>
                        <div className={'content'}>
                            <p className={'contentTitle'}>Estados de la Practica Profesional:</p>

                            <div className={'text'}>
                                <p><Badge status="processing" text="Pendiente: Mientras se le asigna una empresa, un tutor y una fecha de inicio" /> </p>
                                <p><Badge status="success" text="Activo: mientras esta realizando su Practica Profesional" /> </p>
                                <p><Badge status="success" text="Finalizado: Cuando posee fecha de culminacion y nota final" /> </p>
                            </div>
                        </div>
                    </div>

                </div>

                <Card title="Practicas Profesionales" className={'card-antd'} bordered={false}>
                    <div className={'card-antd-content'}>

                        <Table
                            style={{ marginBottom: 16 }}
                            pagination={false}
                            loading={this.state.loading}
                            dataSource={this.state.tableData}
                            locale={{ emptyText: 'No existe ninguna practica profesional en curso' }}
                        >

                            <Column
                                title="Practicante"
                                dataIndex="solicitud.persona.nombre_completo"
                                key="solicitud.persona.nombre_completo"
                                width={'15%'}
                            />

                            <Column
                                title={'Estado'}
                                dataIndex={'estado'}
                                key={'estado'}
                                width={'13%'}
                                render={dataIndex => {
                                    if(dataIndex === 1) {
                                        return (<Badge status="processing" text="Pendiente"/>);
                                    }
                                    if (dataIndex === 4) {
                                        return (<Badge status="success" text="Activo"/>);
                                    }
                                    if (dataIndex === 3) {
                                        return (<Badge status="error" text="Finalizado"/>);
                                    }

                                }}

                            />


                            <Column
                                title={'Empresa asignada'}
                                dataIndex={'empresa.nombre'}
                                key={'empresa.nombre'}
                                render={(text, record) => {

                                    if (!text) {
                                        return 'Sin asignar'
                                    } else {
                                        return text
                                    }

                                }}
                            />

                            <Column
                                title={'Tutor asignado'}
                                dataIndex={'tutor.nombre_completo'}
                                key={'tutor.nombre_completo'}
                                render={(text, record) => {

                                    if (!text) {
                                        return 'Sin asignar'
                                    } else {
                                        return text
                                    }

                                }}
                            />

                            <Column
                                title={'Fecha de Inicio'}
                                dataIndex={'fecha_inicio'}
                                key={'fecha_inicio'}
                                render={(text, record) => {

                                    if (!text) {
                                        return 'Sin asignar'
                                    } else {
                                        return text
                                    }

                                }}
                            />

                            <Column
                                title={'Fecha de culminacion'}
                                dataIndex={'fecha_final'}
                                key={'fecha_final'}
                                render={(text, record) => {

                                    if (!text) {
                                        return 'Sin asignar'
                                    } else {
                                        return text
                                    }

                                }}
                            />

                            <Column
                                title={'Nota final'}
                                dataIndex={'nota'}
                                key={'nota'}
                                render={(text, record) => {

                                    if (!text) {
                                        return 'Sin asignar'
                                    } else {
                                        return text
                                    }

                                }}
                            />

                        </Table>
                        
                    </div>
                </Card>
            </div>
        );
    }
}

export default Practicas;

Practicas.contextTypes = {
    router: PropTypes.object.isRequired
};