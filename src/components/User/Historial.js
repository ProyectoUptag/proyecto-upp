import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Ant Design
import { Card, Table, Badge, Button } from 'antd';

// URLS
import { URLS } from '../../config';

class Historial extends Component {
    constructor(){
        super();
        this.state ={
            fixedHeader: true,
            fixedFooter: true,
            stripedRows: false,
            showRowHover: true,
            selectable: false,
            multiSelectable: false,
            enableSelectAll: false,
            deselectOnClickaway: false,
            showCheckboxes: false,
            height: '300px',
            tableData: [],
            loading: true
        }

    }

    componentWillMount() {

        const add_nombre_completo = (obj) => {
            return {
                ...obj,
                persona: {
                    ...obj.persona,
                    nombre_completo: `${obj.persona.primer_nombre} ${obj.persona.primer_apellido}`
                }
            }
        };

        let user_data = JSON.parse(localStorage.user_data)

        fetch(URLS.SERVER_URL + '/api/solicitud/?user_id=' + user_data.id, {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
        .then( response => response.json())
        .then( data => {
            const new_data = data.map(add_nombre_completo);

            let datos_update = {
                tableData: new_data,
                loading: false
            };

            this.setState(datos_update)
        }) 
        .catch( e => console.error( 'Ha ocurrido un error' ));
    }

    componentWillUnmount() {
        const route = this.context.router.history.location.pathname;

        if (route === '/user/solicitudes'){
            this.context.router.history.push('/user/solicitudes/historial', null);
        }
    }

    handleDownload = (id) => {

        return () =>{
            window.location.href = '/report/solicitud/' + id;
            
        }
    }

    render() {

        const progressColumns = [{
            title: 'Practicante',
            dataIndex: 'persona.nombre_completo',
            key: 'persona.nombre_completo',
        },{
            title: 'Estado',
            dataIndex: 'estado',
            key: 'estado',
            render: dataIndex => {
                if(dataIndex === 1){
                    return (<Badge status="processing" text="Pendiente" />);
                }
                if(dataIndex === 2){
                    return (<Badge status="success" text="Aceptado" />);
                }
                if(dataIndex === 3){
                    return (<Badge status="error" text="Rechazado" />);
                }
            },
        },{
            title: 'PNF',
            dataIndex: 'persona.datos_universitarios.pnf',
            key: 'persona.datos_universitarios.pnf',
        },{
            title: 'Fecha',
            dataIndex: 'fecha',
            key: 'fecha',
        }, {
            title: 'Reporte',
            dataIndex: 'id',
            key: 'download',
            render: id => (
                <Button type="primary" shape="circle" icon="download" size={'default'} onClick={this.handleDownload(id)} />
            ),
        }];

        return (

            <div>
                <div style={{ padding: 24, paddingLeft: 0, background: '#fff'}}>

                    <div className={'pageHeaderContent'}>
                        <div className={'content'}>
                            <p className={'contentTitle'}>Estados de la Solicitud:</p>

                            <div className={'text'}>
                                <p><Badge status="processing" text="Pendiente: Su Solicitud esta siendo revisada por el administrador, espere mientras cambia de estado." /> </p>
                                <p><Badge status="success" text="Aceptado: Su Solicitud ha sido Aceptada, y se ha creado su practica profesional, vaya al menu practicas profesionales - practica en curso." /> </p>
                                <p><Badge status="error" text="Rechazado: Su Solicitud ha sido Rechazada, no cumple con todos los criterios, si desea generar una nueva solicitud, puede hacerlo sin ningin inconveniente. " /> </p>
                            </div>
                        </div>
                    </div>

                </div>

                <Card title="Historial de Solicitudes" className={'card-antd'} bordered={false}>
                    <div className={'card-antd-content'}>

                        <Table
                            style={{ marginBottom: 16 }}
                            pagination={false}
                            loading={this.state.loading}
                            dataSource={this.state.tableData}
                            columns={progressColumns}
                            locale={{ emptyText: 'No existe ninguna solicitud' }}
                        />
                        
                    </div>
                </Card>
            </div>
        );
    }
}

export default Historial;

Historial.contextTypes = {
    router: PropTypes.object.isRequired
};