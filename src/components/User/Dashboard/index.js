import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';


// Ant Design
import { Avatar, Spin, Card, Steps, Badge, Popover } from 'antd';
import './Dashboard.less';

//Auth
import auth from '../../../Auth';

//URLS
import { URLS } from '../../../config';

const { Step } = Steps;

class Dashboard extends Component {

	constructor() {
	    super();
	    this.state = { user: [], avatar: '', stepDirection: 'horizontal', process: 0 , statusSolicitud: 'aceptado'};

	    this.logoutHandler = this.logoutHandler.bind(this);
	}

	componentWillMount() {

        let user_data = JSON.parse(localStorage.user_data);

        fetch(URLS.SERVER_URL + '/api/users/i/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
        .then( response => response.json())
        .then( data => {
            this.setState({user:data})

            fetch(URLS.SERVER_URL + '/api/users/' + data.id + '/persona/', {
                method: 'GET',
                headers: new Headers({
                    'Authorization': 'Token ' + localStorage.token
                })
            })
            .then( response => response.json())
            .then( dp_data => {

                var exclude = [];

                if(dp_data.datos_trabajo.trabaja === false){
                    exclude = exclude.concat(['datos_trabajo'])
                }if(dp_data.datos_antropometricos.enfermedad === false){
                    exclude = exclude.concat(['tipo_enfermedad'])
                }if(dp_data.datos_antropometricos.alergico === false){
                    exclude = exclude.concat(['tipo_alergia'])
                }

                const allTrue = (elem, index, vector) => {
                    return elem === true;
                }

                const checkObject = (vector, key) => {
                    if(typeof vector[key] === 'object'){
                        if(vector[key]){
                            return Object.keys(vector[key]).filter(key => exclude.indexOf(key) === -1).map(key2 => vector[key][key2] !== null).every(allTrue);
                        }

                        return false;
                    }

                    if (typeof vector[key] === 'string'){
                        return vector[key] !== null
                    }
                    if (typeof vector[key] === 'boolean'){
                        return vector[key] !== null
                    }
                    if (typeof vector[key] === 'number'){
                        return vector[key] !== null
                    }
                    return false;
                }

                var datosCompletados = Object.keys(dp_data).filter(key => exclude.indexOf(key) === -1).map(key => checkObject(dp_data, key)).every(allTrue);

                if(datosCompletados){
                    this.setState({process: 1});

                    fetch(URLS.SERVER_URL + '/api/solicitud/?user_id=' + user_data.id, {
                        method: 'GET',
                        headers: new Headers({
                            'Authorization': 'Token ' + localStorage.token
                        })
                    })
                    .then( response => response.json())
                    .then( history => {

                        if(history.length === 0){
                            this.setState({statusSolicitud: null});
                        }if(history[0].estado === 2){
                            this.setState({process: 2});
                        }else{
                            this.setState({statusSolicitud: history[0].estado});
                        }

                    }) 
                    .catch( e => console.error( 'Ha ocurrido un error' ));
                }else{
                    this.setState({process: 0});
                }

                fetch('http://daceos.uptag.edu.ve/api/estudiante.php?codigo='+ dp_data.datos_universitarios.codigo_est +'&oper=foto', {
                    method: 'GET',
                })
                .then( response => response.json())
                .then( data_avatar => {

                	if(data_avatar.success === false || data_avatar.num_rows === 0){
                        localStorage._a = 'static/img/user.png'
                		this.setState({avatar: 'static/img/user.png'})

                	}else{
                        localStorage._a = data_avatar.picture
                        this.setState({avatar:data_avatar.picture})
                	}
                }) 
                .catch( e => {
                	console.error( 'Ha ocurrido un error' )
                    localStorage._a = 'static/img/user.png'
                    this.setState({avatar: 'static/img/user.png'})
                })
            }) 
            .catch( e => console.error( 'Ha ocurrido un error' ));
        })
        .catch( e => console.error( 'Ha ocurrido un error' ));
	}

	logoutHandler() {
	    auth.logout()
	    this.context.router.history.push('/iniciar', null);
	}

    changeProcess = () =>{
        this.setState({process: 2});
    }

  	render() {

      const popoverContent0 = (
        <div style={{ width: 160 }}>
            Rellene los datos personales.
        </div>
      );

        const popoverContent1 = () => {
            const { statusSolicitud } = this.state;

            if(statusSolicitud === null){
                return (
                    <div style={{ width: 145 }}>
                        <p className={'textSecondary'} style={{ margin: 4 }}>Genere una solicitud. </p>
                    </div>
                );
            }

            else if (statusSolicitud === 1){
                return (
                    <div style={{ width: 160 }}>
                        Estado
                        <span className={'textSecondary'} style={{ float: 'right' }}>
                          <Badge status="processing" text={<span style={{ color: 'rgba(0, 0, 0, 0.45)' }}>Pendiente</span>} />
                        </span>
                        <p className={'textSecondary'} style={{ marginTop: 4 }}>Solictud en proceso... </p>
                    </div>
                );
            }
            // else if (statusSolicitud === 'aceptado'){
            //     return (
            //         <div style={{ width: 160 }}>
            //             Estado
            //             <span className={'textSecondary'} style={{ float: 'right' }}>
            //               <Badge status="success" text={<span style={{ color: 'rgba(0, 0, 0, 0.45)' }}>Aceptada</span>} />
            //             </span>
            //             <p className={'textSecondary'} style={{ marginTop: 4 }}>Su solictud ha sido aceptada! Presione continuar para confirmar el inicio de la Practica Profesional. </p>
            //         </div>
            //     );
            // }
            else if (statusSolicitud === 3){
                return (
                    <div style={{ width: 160 }}>
                        Estado
                        <span className={'textSecondary'} style={{ float: 'right' }}>
                            <Badge status="error" text={<span style={{ color: 'rgba(0, 0, 0, 0.45)' }}>Rechazada</span>} />
                        </span>
                        <p className={'textSecondary'} style={{ marginTop: 4 }}>Su solictud ha sido rechazada! Verifíque que haya subido todos los documentos requeridos y que sus datos personales son correctos. Consulte el historial de solicitudes para más información. </p>
                     </div>

                );
            }
        };

        const popoverContent2 = (
            <div style={{ width: 160 }}>
                Practica Profesional:
                <p className={'textSecondary'} style={{ marginTop: 4 }}>Ha iniciado su Practica Profesional, ahora puede ver su progreso dirigiendose en el menu a la opcion 'Practica Profesional'. </p>
            </div>
        );

        const popoverContent3 = (
            <div style={{ width: 160 }}>
                Practica Profesional:
                <p className={'textSecondary'} style={{ marginTop: 4 }}>Ha culminado la Practica Profesional con exito, ahora puede ir a retirar su planilla en la Unidad de Practica Profesional. </p>
            </div>
        );

      // const customDot = (dot, { status , index}) => (status === 'process' ?
      //   <Popover placement="topLeft" arrowPointAtCenter content={popoverContent}>
      //     {dot}
      //   </Popover>
      //   : dot
      // );      

        const customDot = (dot, { status , index}) => (

            status === 'process' ?

                this.state.process === 0 ?
                    <Popover placement="topLeft" arrowPointAtCenter content={popoverContent0}>
                      {dot}
                    </Popover>

                :

                    this.state.process === 1 ?
                        <Popover placement="topLeft" arrowPointAtCenter content={popoverContent1()}>
                          {dot}
                        </Popover>

                    :

                        this.state.process === 2 ?
                            <Popover placement="topLeft" arrowPointAtCenter content={popoverContent2}>
                              {dot}
                            </Popover>

                        :

                            this.state.process === 3 ?
                                <Popover placement="topLeft" arrowPointAtCenter content={popoverContent3}>
                                  {dot}
                                </Popover>

                            :
                                dot

            : 
                dot
        );

        const desc0 = (
            <div className={classNames('textSecondary', 'stepDescription')}>
                <div style={{textAlign: 'center'}}>
                    {
                        this.state.process === 0 ?
                            <Link to="/user/datos-personales/editar">
                                <a href="">Completar</a>
                            </Link>
                        :

                        <span/>
                    }        
                </div>
            </div>
        );      

        const desc1 = () => {
            if (this.state.statusSolicitud === null){
                return (
                    <div className={classNames('textSecondary', 'stepDescription')}>
                        <div style={{textAlign: 'center'}}>
                            <Link to="/user/solicitudes/nueva-solicitud">
                                <a href="">Generar</a>
                            </Link>      
                        </div>
                    </div>
                );
            }else if(this.state.statusSolicitud === 'rechazado'){
                return (
                    <div className={classNames('textSecondary', 'stepDescription')}>
                        <div style={{textAlign: 'center'}}>
                            <Link to="/user/solicitudes/historial">
                                <a href="">Historial de solicitudes</a>
                            </Link>
                        </div>
                    </div>
                );
            }
        };

      const desc2 = (
        <div className={'stepDescription'}>
          <div style={{textAlign: 'center'}}>

          </div>
        </div>
      );

      const desc3 = (
        <div className={'stepDescription'}>
          <div style={{textAlign: 'center'}}>

          </div>
        </div>
      );

      const { stepDirection } = this.state;

	    return (
	    	<div>
	            <div style={{ padding: 24, background: '#fff' }}>

	              	<div className={'pageHeaderContent'}>
	              	  <div className={"avatar"}>
	              	  	{ localStorage.hasOwnProperty('_a') ? (
	              	  	  	<Avatar size="large" src={ localStorage._a } />
	              	  	) : <Spin size="large" className={'spinnerAvatar'} />}

	              	  </div>
	              	  <div className={'content'}>
	              	    <p className={'contentTitle'}>Bienvenido, {this.state.user.username}, ¡Ahora ya puedes realizar tu solicitud a las Practicas Profesionales!</p>
	              	    <p>Unidad de Practicas Profesionales | PNFI - PNFC - PNFA - PNFM - PNFQ</p>
	              	  </div>
	              	</div>
	              	
	            </div>

              <Card title="Proceso de Practicas Profesionales" className={'card-antd'} bordered={false}>
                <div className={'card-antd-content'}>
                  <Steps direction={stepDirection} progressDot={customDot} current={this.state.process}>
                    <Step title="Datos Personales" description={desc0} />
                    <Step title="Envío de Solicitud" description={desc1()} />
                    <Step title="Iniciacion" description={desc2} />
                    <Step title="Culminacion" description={desc3} />
                  </Steps>
                </div>
              </Card>


	        </div>
		
	    );
  	}
}

export default Dashboard;

Dashboard.contextTypes = {
    router: PropTypes.object.isRequired
};