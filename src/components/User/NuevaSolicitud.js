//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'react-addons-update';
import axios from 'axios';

// Material UI
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import Snackbar from 'material-ui/Snackbar';

// Components
import FooterToolbar from './NuevaSolicitud/FooterToolbar';

// Ant Disign
import { message, Card, Col, Row } from 'antd';
import Uploader from './Uploader';

// Reactstrap
import { Collapse } from 'reactstrap';

// URLS
import { URLS } from '../../config';

// Validator
import { MakeRule, RunRules } from '../../validation/RuleRunner';
import { required } from '../../validation/Rules';

const styles = {
    inputs: {
        width: 100 + '%',
    },
    divInputs: {
        overflow: 'hidden'
    }, 
    slide: {
        padding: 10,
    },
    customWidth: {
        width: 150,
    },
    block: {
        maxWidth: 250,
    },
    radioButton: {
        display: 'inline-block',
    },
    radioButtonDisease:{
       marginTop: 30, 
       marginLeft: 20
    },
    checkbox: {
        marginBottom: 16,
    },
    inkBar: {
        background: '#7D7D7D', 
        height: '2'
    }
};


class NuevaSolicitud extends Component {

    constructor() {
        super();
        this.state = {
            open: false,
            slideIndex: 0,
            showView: false,
            showErrors: false,
            validationMessages: {},
            message: '',
            solicitudesPendientes: 0,
        // ------- DATOS DE SOLICITUD ---------
            fileList: [],
        // ------- DATOS DE UBICACION ---------
            ubicacionLocal: false,
            ubicacionRegional: false,
            ubicacionNacional: false,
        // ------- DATOS DE EMPRESA ---------
            prevContactCompany: false,
            collapseDataCompany: false,
            nombreEmpresa: "",
            nContacto1: "",
            nContacto2: "",
        };

        this.rulesValidations = [
            MakeRule("pnf", "PNF", required),
            MakeRule("promedio", "Promedio", required),
            MakeRule("fechaIngreso", "Fecha de Ingreso", required),
            MakeRule("codigoEstudiante", "Codigo", required),
        ];

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeFileList = this.handleChangeFileList.bind(this);
        this.handleFieldChanged = this.handleFieldChanged.bind(this);
        this.getError = this.getError.bind(this);
    }

    componentWillMount() {

        message.config({
            top: 70,
            duration: 3,
        });
    }

    componentWillUnmount() {
        const route = this.context.router.history.location.pathname;

        if (route === '/user/solicitudes'){
            this.context.router.history.push('/user/solicitudes/nueva-solicitud', null);
        }
    }

    getError = (field) => {
        if (this.state.showErrors) {
            return this.state.validationMessages[field] || "";
        }
    }

    handleFieldChanged = (field) => {

        return (e) => {
            let newState = update(this.state, {
                [field]: {$set: e.target.value}
            });

            newState.validationMessages = RunRules(newState, this.rulesValidations);

            this.setState(newState);
        };
    }

    handleChangeFileList = ({fileList}) => {

        for (var i = 0; i < fileList.length; i++) {
            var file = fileList[i];

        }

        let aa = fileList.map((f) => {
            let reader = new FileReader();
            
            reader.onload = (function (theFile) {
                return function (e) {
                    if(theFile.type === 'application/pdf'){
                        theFile.url = e.target.result;
                        theFile.thumbUrl = 'static/img/icon_pdf.png';
                    }

                    if(theFile.type === 'image/jpeg' || theFile.type === 'image/png'){
                        theFile.url = e.target.result;
                    }
                };
            })(f);

            reader.onloadend = function (e) {

            };

            reader.readAsDataURL(f.originFileObj);

            return f;

        });

        if (file.name.length > 50){
            this.setState({
                open: true,
                message: `El nombre del archivo "${file.name.substring(0,15)}..." es demasiado largo.`
            });
        }
        else if (file.size > 2000000){
            this.setState({
                open: true,
                message: `El archivo "${file.name.substring(0,15)}..." pesa mas de 2MB.`
            });
        }else{  
            this.setState({ fileList: aa }); 
        }
    }

    HandleBeforeUpload = (file) => {
        console.log(file)
        const isValid = file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'application/pdf';
        if (!isValid) {
            message.error('Solo puedes subir archivos con los siguientes formatos: JPG, PNG o PDF');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('¡La imagen o archivo debe ser más pequeña que 2 MB!');
        }
        return isValid && isLt2M;

    }

    handlePrevContactCompanyTrue = () => {
        this.setState({
            prevContactCompany: true,
            collapseDataCompany: true
        });
    };

    handlePrevContactCompanyFalse = () => {
        this.setState({
            prevContactCompany: false,
            collapseDataCompany: false
        });
    };

    handleCheckedUbicacionLocal = () => {
        this.setState({ ubicacionLocal: !this.state.ubicacionLocal });
    }

    handleCheckedUbicacionRegional = () => {
        this.setState({ ubicacionRegional: !this.state.ubicacionRegional });
    }

    handleCheckedUbicacionNacional = () => {
        this.setState({ ubicacionNacional: !this.state.ubicacionNacional });
    }


    handleSubmit = () => {

        const { fileList, ubicacionLocal, ubicacionRegional, ubicacionNacional, nombreEmpresa, nContacto1, nContacto2 } = this.state;
        const data = new FormData();
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }

        let user_data = JSON.parse(localStorage.user_data)

        fetch(URLS.SERVER_URL + '/api/users/' + user_data.id + '/persona/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
        .then( response => response.json())
        .then( datos_personales => {

            var exclude = [];

            const allTrue = (elem, index, vector) => {
                return elem === true;
            }

            const checkObject = (vector, key) => {
                if(typeof vector[key] === 'object'){
                    if(vector[key]){
                        return Object.keys(vector[key]).filter(key => exclude.indexOf(key) === -1).map(key2 => vector[key][key2] !== null).every(allTrue);
                    }

                    return false;
                }

                if (typeof vector[key] === 'string'){
                    return vector[key] !== null
                }
                if (typeof vector[key] === 'boolean'){
                    return vector[key] !== null
                }
                if (typeof vector[key] === 'number'){
                    return vector[key] !== null
                }
                return false;
            }

            if(datos_personales.datos_trabajo.trabaja === false){
                exclude = exclude.concat(['datos_trabajo'])
            }if(datos_personales.datos_antropometricos.enfermedad === false){
                exclude = exclude.concat(['tipo_enfermedad'])
            }if(datos_personales.datos_antropometricos.alergico === false){
                exclude = exclude.concat(['tipo_alergia'])
            }

            var datosCompletados = Object.keys(datos_personales).filter(key => exclude.indexOf(key) === -1).map(key => checkObject(datos_personales, key)).every(allTrue);

            if (datosCompletados){
                if (fileList.length === 0){
                    this.setState({
                        open: true,
                        message: `Suba los documentos solicitados para generar una solicitud.`
                    });
                }
                else if(ubicacionLocal === false && ubicacionNacional === false && ubicacionRegional === false){
                    this.setState({
                        open: true,
                        message: `Elija por lo menos una ubicacion antes de generar la solicitud.`
                    });
                }else{

                    for (var i = 0; i < fileList.length; i++) {
                        let file = fileList[i];
                        data.append('archivos', file.originFileObj);
                    }

                    data.append('user_id', user_data.id);
                    data.append('estado_id', 1);
                    data.append('ubicacion_local', Number(ubicacionLocal));
                    data.append('ubicacion_regional', Number(ubicacionRegional));
                    data.append('ubicacion_nacional', Number(ubicacionNacional));
                    data.append('nombre_empresa', nombreEmpresa);
                    data.append('numero_contacto1', nContacto1);
                    data.append('numero_contacto2', nContacto2);


                    fetch(URLS.SERVER_URL + '/api/solicitud/?user_id=' + user_data.id, {
                        method: 'GET',
                        headers: new Headers({
                            'Authorization': 'Token ' + localStorage.token
                        })
                    })
                    .then( response => response.json())
                    .then( datos => {

                        var solicitudesP = 0;

                        for (var i = 0; i < datos.length; i++) {

                            if(datos[i].estado === 1){
                                solicitudesP += 1;
                            }
                        }

                        if(solicitudesP >= 1){
                            this.setState({
                                open: true,
                                message: `Ya tienes una solicitud en proceso.`
                            });
                        }else{
                            message.loading('Generando solicitud...', 0);

                            axios.post(URLS.SERVER_URL + '/solicitud/', data, config)
                            .then((response) => {

                                message.destroy()
                                message.success('Solicitud generada con exito.');

                                this.setState({
                                    fileList: [],
                                    ubicacionLocal: false,
                                    ubicacionRegional: false,
                                    ubicacionNacional: false,
                                    prevContactCompany: false,
                                    collapseDataCompany: false,
                                    nombreEmpresa: "",
                                    nContacto1: "",
                                    nContacto2: "",
                                });
                                

                            })
                            .catch((err) => {
                                message.destroy()
                                message.error('No hemos podido generar su solicitud.');
                            });              
                        }
                    }) 
                    .catch( e => console.error( 'Ha ocurrido un error' ));
                }

            }else{
                this.setState({
                    open: true,
                    message: `Rellene los datos personales antes de generar una solicitud.`
                });
            }

        }) 
        .catch( e => console.error( 'Ha ocurrido un error' ));

        
    }

    HandleOnRemove = (file) => {
        this.setState(({ fileList }) => {
            const index = fileList.indexOf(file);
            const newFileList = fileList.slice();
            newFileList.splice(index, 1);

            return {
                fileList: newFileList,
            };
        });
    }

    handleRequestClose = () => {
        this.setState({
            open: false,
            message: ''
        });
    };


    render() {
        return (
            <div>
                <div style={{ padding: 24, paddingLeft: 0, background: '#fff'}}>

                      <div className={'pageHeaderContent'}>
                        <div className={'content'}>
                            <p className={'contentTitle'}>Para generar la solicitud, necesitas subir los siguientes documentos:</p>
                                
                            <div className={'text'}>
                                <p>1. Ficha curricular <span style={{color: 'red'}}>*</span></p>
                                <p>2. Historial académico <span style={{color: 'red'}}>*</span></p>
                                <p>3. Constancia de trabajo (opcional)</p>
                                <br/>
                                <p> El jefe del departamento le solicitara cualquier otro documento que solicite la empresa al momento de aceptar su solicitud. </p>
                            </div>
                        </div>
                      </div>
                      
                </div>

                <Card title="Solicitud" className={'card-antd'} bordered={false}>
                    <div className={'card-antd-content'}>
                        <Row gutter={16}>
                          <Col lg={24} md={24} sm={24}>
                              <p>Suba los documentos solicitados aqui:</p>

                          </Col>
                        </Row>

                        <Row gutter={16}>         

                            <Col lg={24} md={24} sm={24}>
                                <form enctype="multipart/form-data" onSubmit={this.handleSubmit}>
                                    <Uploader fileList={this.state.fileList} onChange={this.handleChangeFileList} onRemove={this.HandleOnRemove} beforeUpload={this.HandleBeforeUpload} multiple={true}/>
                                        
                                </form>
                            </Col>                                
                        </Row>
                    </div>      
                </Card>

                <Card title="Ubicacion" className={'card-antd'} bordered={false}>
                    <div className={'card-antd-content'}>
                        <Row gutter={16}>
                          <Col lg={24} md={24} sm={24}>
                              <p>Elija la ubicacion en la que prefiere realizar la Practica Profesional:</p>

                          </Col>
                        </Row>

                        <Row gutter={16}>
                            <Col lg={8} md={8} sm={8}>
                                <Checkbox
                                    label="Ubicacion Local"
                                    style={styles.checkbox}
                                    checked={this.state.ubicacionLocal}
                                    onCheck={this.handleCheckedUbicacionLocal}
                                />
                            </Col>

                            <Col lg={8} md={8} sm={8}>
                                <Checkbox
                                    label="Ubicacion Regional"
                                    style={styles.checkbox}
                                    checked={this.state.ubicacionRegional}
                                    onCheck={this.handleCheckedUbicacionRegional}
                                />
                            </Col>                                

                            <Col lg={8} md={8} sm={8}>
                                <Checkbox
                                    label="Ubicacion Nacional"
                                    style={styles.checkbox}
                                    checked={this.state.ubicacionNacional}
                                    onCheck={this.handleCheckedUbicacionNacional}
                                />
                            </Col>
                        </Row>
                    </div>      
                </Card>

                <Card title="Sugerir Empresa" className={'card-antd-footer'} bordered={false}>
                    <div className={'card-antd-content'}>
                        <Row gutter={16}>
                          <Col lg={24} md={24} sm={24}>
                              <p>Posee algun contacto previo con alguna empresa?</p>

                          </Col>
                        </Row>

                        <Row gutter={16}>
                          <Col lg={8} md={8} sm={8}>
                              <RadioButtonGroup style={{display: 'flex', flexDirection: 'row'}} name="shipSpeed" defaultSelected={false} valueSelected={this.state.prevContactCompany}>
                                  <RadioButton value={true} label="Si" style={styles.radioButton} onClick={this.handlePrevContactCompanyTrue}/>
                                  <RadioButton value={false} label="No" style={styles.radioButton} onClick={this.handlePrevContactCompanyFalse}/>
                              </RadioButtonGroup>
                          </Col> 
                        </Row>

                        <Collapse isOpen={this.state.collapseDataCompany}>
                            <Row gutter={16}>            
                                <Col lg={8} md={8} sm={8}>
                                    <TextField
                                        floatingLabelText="Nombre de Empresa"
                                        type="text"
                                        multiLine={true}
                                        style={styles.inputs}
                                        onChange={this.handleFieldChanged('nombreEmpresa')}
                                        value={this.state.nombreEmpresa}
                                    />
                                </Col>

                                <Col lg={8} md={8} sm={8}>
                                    <TextField
                                        floatingLabelText="N° de Contacto 1"
                                        type="text"
                                        multiLine={true}
                                        style={styles.inputs}
                                        onChange={this.handleFieldChanged('nContacto1')}
                                        value={this.state.nContacto1}
                                    />
                                </Col>                                

                                <Col lg={8} md={8} sm={8}>
                                    <TextField
                                        floatingLabelText="N° de Contacto 2"
                                        type="text"
                                        style={styles.inputs}
                                        onChange={this.handleFieldChanged('nContacto2')}
                                        value={this.state.nContacto2}
                                    />
                                </Col>
                            </Row>
                        </Collapse>
                    </div>      
                </Card>

                <FooterToolbar>
                    <div className={'buttonFooter'}>

                        <RaisedButton
                            primary={true}
                            label="Generar solicitud"
                            labelStyle={{fontSize: 12}}
                            onClick={this.handleSubmit}
                            icon={<i className="fa fa-dot-circle-o" style={{color: '#fff'}}></i>}
                        />                   
                    </div>
                </FooterToolbar>

                <Snackbar
                    open={this.state.open}
                    message={this.state.message}
                    autoHideDuration={6000}
                    onRequestClose={this.handleRequestClose}
                />
            </div>         
        )
    }
}

export default NuevaSolicitud;

NuevaSolicitud.contextTypes = {
    router: PropTypes.object.isRequired
};