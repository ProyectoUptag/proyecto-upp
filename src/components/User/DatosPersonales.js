//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import update from 'react-addons-update';
import axios from 'axios';
import './DatosPersonales.less';

// Ant Design
import { Card, Col, Row, Form, Popover, Icon } from 'antd';

// Material UI
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import Snackbar from 'material-ui/Snackbar';

// Reactstrap
import { Collapse } from 'reactstrap';

// Components
import FooterToolbar from './DatosPersonales/FooterToolbar';

// URLS
import { URLS } from '../../config';

const colorCustumized = 'rgba(244, 177, 63, 0.96)';

const fieldLabels = {
    // Datos Personales
    primerNombre: 'Primer Nombre',
    segundoNombre: 'Segundo Nombre',
    primerApellido: 'Primer Apellido',
    segundoApellido: 'Segundo Apellido',
    cedula: 'Cedula de identidad',
    telefonoMovil: 'Telefono Movil',
    telefonoFijo: 'Telefono Fijo',
    estadoCivil: 'Estado Civil',
    nacionalidad: 'Nacionalidad',
    fechaDeNacimiento: 'Fecha de Nacimiento',
    sexo: 'Sexo',
    codigoPostal: 'Codigo Postal',
    ciudadOrigen: 'Ciudad de Origen',
    ciudadActual: 'Ciudad Actual',
    estado: 'Estado',
    direccionPrimaria: 'Direccion 1',
    direccionSecundaria: 'Direccion 2',
    // Datos Universitarios
    pnf: 'PNF',
    promedio: 'Promedio',
    fechaIngreso: 'Fecha de Ingreso',
    codigoEstudiante: 'Codigo de Estudiante',
    turno: 'Turno',
    // Datos de Trabajo
    empresa: 'Nombre de la Empresa',
    actividad: 'Actividad',
    direccionEmpresa: 'Direccion de la Empresa',
    telefonoEmpresa: 'Telefono de la Empresa',
    // Datos Antropometricos
    peso: 'Peso',
    estatura: 'Estatura',
    talla: 'Talla',
    numeroCalzado: 'Numero de Calzado',
    grupoSanguineo: 'Grupo Sanguineo',
    tipoEnfermedad: 'Tipo de Emfermedad',
    tipoAlergia: 'Tipo de Alergia',
};

const styles = {
    inputs: {
        width: 100 + '%',
    },
    inputsLarge:{
        width: 100 + '%',
    },
    divInputs: {
        overflow: 'hidden'
    }, 
    headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
    },
    slide: {
        padding: 10,
    },
    customWidth: {
        width: 150,
    },
    block: {
        maxWidth: 250,
    },
    radioButton: {
        display: 'inline-block',
    },
    radioButtonDisease:{
       marginTop: 30, 
       marginLeft: 20
    },
    inkBar: {
        background: '#7D7D7D', 
        height: '2'
    }
};


class DatosPersonales extends Component {

    constructor() {
        super();
        this.state = {
            open: false,
            slideIndex: 0,
            message: '',
            user: [],
            person_data: [],
            loadingData: true,
            data: [],
        // ------- DATOS PERSONALES ---------
            estadoCivil: 0,
            nacionalidad: 0,
            fechaDeNacimiento: null,
            sexo: '',
        // ------- DATOS UNIVERSITARIOS ---------
            fechaIngreso: null,
            turno: null,
        // ------- DATOS DE TRABAJO ---------
            trabaja: false,
            collapseJob: false,
        // ------- DATOS ANTROPOMETRICOS ---------  
            enfermedad: false,
            collapseDisease: false,
            alergico: false,
            collapseAllergy: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.getError = this.getError.bind(this);
    }

    componentWillMount() {
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";

        let user_data = JSON.parse(localStorage.user_data)

        fetch(URLS.SERVER_URL + '/api/users/' + user_data.id + '/persona/', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + localStorage.token
            })
        })
        .then( response => response.json())
        .then( data => {

            this.setState({
                person_data: data,
                estadoCivil: data.estado_civil,
                nacionalidad: data.nacionalidad,
                fechaDeNacimiento: data.fecha_nac,
                sexo: data.sexo,
                fechaIngreso: data.datos_universitarios.fecha_entrada,
                turno: data.datos_universitarios.turno,
                trabaja: data.datos_trabajo.trabaja,
                collapseJob: data.datos_trabajo.trabaja,
                enfermedad: data.datos_antropometricos.enfermedad,
                collapseDisease: data.datos_antropometricos.enfermedad,
                alergico: data.datos_antropometricos.alergico,
                collapseAllergy: data.datos_antropometricos.alergico,
                loadingData: false
            });

            this.props.form.setFieldsValue({
                primerApellido: data.primer_apellido || '',
                segundoApellido: data.segundo_apellido || '',
                primerNombre: data.primer_nombre || '',
                segundoNombre: data.segundo_nombre || '',
                cedula: data.cedula || '',
                telefonoMovil: data.telefono_movil || '',
                telefonoFijo: data.telefono_fijo || '',
                nacionalidad: data.nacionalidad || '',
                estadoCivil: data.estado_civil || '',
                fechaDeNacimiento: data.fecha_nac || '',
                sexo: data.sexo,
                // ------- DATOS PERSONALES ---------
                ciudadOrigen: data.datos_direccion.ciudad_origen || '',
                ciudadActual: data.datos_direccion.ciudad_actual || '',
                estado: data.datos_direccion.estado || '',
                direccionPrimaria: data.datos_direccion.direccion_primaria || '',
                direccionSecundaria: data.datos_direccion.direccion_secundaria || '',
                codigoPostal: data.datos_direccion.codigo_postal || '',
                // ------- DATOS DE TRABAJO ---------
                empresa: data.datos_trabajo.empresa || '',
                actividad: data.datos_trabajo.actividad || '',
                direccionEmpresa: data.datos_trabajo.direccion_empresa || '',
                telefonoEmpresa: data.datos_trabajo.telefono_empresa || '',
                // ------- DATOS UNIVERSITARIOS ---------
                pnf: data.datos_universitarios.pnf || '',
                promedio: data.datos_universitarios.promedio || '',
                fechaIngreso: data.datos_universitarios.fecha_entrada || '',
                codigoEstudiante: data.datos_universitarios.codigo_est || '',
                turno: data.datos_universitarios.turno || '',

                // ------- DATOS ANTROPOMETRICOS ---------
                peso: data.datos_antropometricos.peso || '',
                estatura: data.datos_antropometricos.estatura || '',
                talla: data.datos_antropometricos.talla || '',
                numeroCalzado: data.datos_antropometricos.numero_calzado || '',
                grupoSanguineo: data.datos_antropometricos.grupo_sanguineo || '',
                tipoEnfermedad: data.datos_antropometricos.tipo_enfermedad || '',
                tipoAlergia: data.datos_antropometricos.tipo_alergia || '',

            })
        }) 
        .catch( e => console.error( 'Ha ocurrido un error' ));
    }

    componentWillUnmount() {
        const route = this.context.router.history.location.pathname;

        if (route === '/user/datos-personales'){
            this.context.router.history.push('/user/datos-personales/editar', null);
        }
    }

    formatDate = (date) => {

        if(typeof date === "string"){
            return date;
        }
        
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    }

    handleDate = (event, date) => {
        let newState = update(this.state, {
            "fechaIngreso": {$set: date}
        });

        this.setState(newState);

        this.props.form.setFieldsValue({fechaIngreso: this.formatDate(date)});
    };

    handleBirthDate = (event, date) => {
        this.props.form.setFieldsValue({fechaDeNacimiento: this.formatDate(date)});
        this.setState({fechaDeNacimiento: date});

    };

    handleChangeCivilState = (event, index, value) => {
        this.props.form.setFieldsValue({estadoCivil: value});
        this.setState({estadoCivil: value});
    }

    handleChangeNationality = (event, index, value) => {
        this.props.form.setFieldsValue({nacionalidad: value});
        this.setState({nacionalidad: value});
    }

    handleChangeSex = (event, index, value) => {
        this.props.form.setFieldsValue({sexo: value});
        this.setState({sexo: value})
    }

    handleChangeTurn = (event, index, value) => {
        this.props.form.setFieldsValue({turno: value});
        this.setState({turno: value})
    }

    handleSubmit(e) {
        e.preventDefault()

        this.props.form.validateFieldsAndScroll((error, values) => {
            if (!error) {
                axios({
                    method: 'PUT',
                    url: URLS.SERVER_URL + '/api/personas/' + this.state.person_data.id + '/',
                    headers: {
                        'Authorization': 'Token ' + localStorage.token,
                        'Content-Type': 'application/json'
                    },
                    data: JSON.stringify({
                        id: this.state.person_data.id,
                        user: this.state.person_data.user,
                        primer_nombre: values.primerNombre,
                        segundo_nombre: values.segundoNombre,
                        primer_apellido: values.primerApellido,
                        segundo_apellido: values.segundoApellido,
                        cedula: values.cedula,
                        telefono_movil: values.telefonoMovil,
                        telefono_fijo: values.telefonoFijo,
                        nacionalidad: values.nacionalidad,
                        estado_civil: values.estadoCivil,
                        fecha_nac: this.formatDate(values.fechaDeNacimiento),
                        sexo: values.sexo,
                        datos_direccion:{
                            id: this.state.person_data.datos_direccion.id,
                            ciudad_origen: values.ciudadOrigen,
                            ciudad_actual: values.ciudadActual,
                            estado: values.estado,
                            direccion_primaria: values.direccionPrimaria,
                            direccion_secundaria: values.direccionSecundaria,
                            codigo_postal: values.codigoPostal,
                        },
                        datos_universitarios:{
                            id: this.state.person_data.datos_universitarios.id,
                            pnf: values.pnf,
                            promedio: Number(values.promedio),
                            fecha_entrada: this.formatDate(values.fechaIngreso),
                            codigo_est: Number(values.codigoEstudiante),
                            turno: values.turno,

                        },
                        datos_antropometricos:{
                            id: this.state.person_data.datos_antropometricos.id,
                            peso: values.peso,
                            estatura: values.estatura,
                            talla: values.talla,
                            numero_calzado: values.numeroCalzado,
                            grupo_sanguineo: values.grupoSanguineo,
                            enfermedad: this.state.enfermedad,
                            tipo_enfermedad: values.tipoEnfermedad,
                            alergico: this.state.alergico,
                            tipo_alergia: values.tipoAlergia,
                        },
                        datos_trabajo:{
                            id: this.state.person_data.datos_trabajo.id,
                            trabaja: this.state.trabaja,
                            empresa: values.empresa,
                            actividad: values.actividad,
                            direccion_empresa: values.direccionEmpresa,
                            telefono_empresa: values.telefonoEmpresa,
                        },
                    })

                })
                .then((response) => {
                    this.setState({
                        open: true,
                        message: 'Los datos han sido actualizados'
                    });

                })
                .catch((data) => {

                    this.setState({
                        open: true,
                        message: 'No se han podido guardar los datos'
                    });
                });
            }else{
                this.setState({
                    open: true,
                    message: 'Faltan campos por completar'
                });
            }
        })
    }

    handleRequestClose = () => {
        this.setState({
            open: false,
            message: ''
        });
    };

    handleJobTrue = () => {
        this.setState({
            trabaja: true,
            collapseJob: true
        });
    };

    handleJobFalse = () => {
        this.setState({
            trabaja: false,
            collapseJob: false
        });
    };

    handleDiseaseTrue = () => {
        this.setState({
            enfermedad: true,
            collapseDisease: true

        });
    };

    handleDiseaseFalse = () => {
        this.setState({
            enfermedad: false,
            collapseDisease: false

        });
    };

    handleAllergyTrue = () => {
        this.setState({
            alergico: true,
            collapseAllergy: true
        });
    };

    handleAllergyFalse = () => {
        this.setState({
            alergico: false,
            collapseAllergy: false
        });
    };


    getError = (field) => {
        const form = this.props.form;

        if(form.getFieldError(field)){
            return true;
          
        }else{
            return false
        }
    }

      render() {

        const { form } = this.props;
        const { getFieldDecorator, getFieldsError } = form;

        const errors = getFieldsError();

        const getErrorInfo = () => {
            const errorCount = Object.keys(errors).filter(key => errors[key]).length;

            if (!errors || errorCount === 0) {
                return null;
            }

            const scrollToField = (fieldKey) => {
                const labelNode = document.querySelector(`#${fieldKey}`);
                if (labelNode) {
                    labelNode.scrollIntoView(true);
                }
            }

            const errorList = Object.keys(errors).map((key) => {
                if (!errors[key]) {
                    return null;
                }
                return (
                    <li key={key} className={'errorListItem'} onClick={() => scrollToField(key)}>
                        <Icon type="cross-circle-o" className={'errorIcon'} />
                        <div className={'errorMessage'}>{errors[key][0]}</div>
                        <div className={'errorField'}>{fieldLabels[key]}</div>
                    </li>
                );
            });

            return (
                <span className={'errorIcon'}>
                    <Popover
                        title="Información de verificación de formulario"
                        content={errorList}
                        overlayClassName={'errorPopover'}
                        trigger="click"
                        getPopupContainer={trigger => trigger.parentNode}
                    >
                        <Icon type="exclamation-circle" />
                    </Popover>
                    {errorCount}
                </span>
            );
        };

        return (
            <div>
                <div style={{ padding: 24, paddingLeft: 0, background: '#fff'}}>

                      <div className={'pageHeaderContent'}>
                        <div className={'content'}>
                          <p className={'contentTitle'}>Completa los datos faltantes para poder generar tu solicitud!</p>
                          <p>Algunos de estos datos son requeridos por las empresas para poder tener un perfil sobre el practicante.</p>
                        </div>
                      </div>
                      
                </div>

                <Form onSubmit={this.handleSubmit} layout="vertical" hideRequiredMark>

                    <div id={'primerNombre'}></div>
                    <div id={'segundoNombre'}></div>
                    <div id={'primerApellido'}></div>

                    <Card title="Datos Personales" type="flex" justify="center" className={'card-antd'} bordered={false} loading={this.state.loadingData}>

                        <div className={'card-antd-inputs'}>

                            <div id={'segundoApellido'}></div>
                            <div id={'cedula'}></div>
                            <div id={'telefonoMovil'}></div>

                            <Row gutter={16}>
                                
                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('primerNombre', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Primer Nombre"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("primerNombre")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>

                                <Col lg={8} md={8} sm={8} id={'segundoNombre'}>
                                    <Form.Item>
                                        {getFieldDecorator('segundoNombre', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Segundo Nombre"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("segundoNombre")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>

                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('primerApellido', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Primer Apellido"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("primerApellido")}
                                            />
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>

                            <div id={'telefonoFijo'}></div>
                            <div id={'estadoCivil'}></div>
                            <div id={'nacionalidad'}></div>

                            <Row gutter={16}>
                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('segundoApellido', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(


                                            <TextField
                                                floatingLabelText="Segundo Apellido"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("segundoApellido")}
                                            />
                                      )}
                                  </Form.Item>
                                </Col>

                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('cedula', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Cedula"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("cedula")}
                                            />
                                      )}
                                    </Form.Item>
                                </Col>

                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('telefonoMovil', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Telefono Movil"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("telefonoMovil")}
                                            />
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>

                            <div id={'fechaDeNacimiento'}></div>
                            <div id={'sexo'}></div>
                            <div id={'codigoPostal'}></div>

                            <Row gutter={16}>
                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('telefonoFijo', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(


                                            <TextField
                                                floatingLabelText="Telefono Fijo"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("telefonoFijo")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>

                                <Col lg={8} md={8} sm={8}>
                                    <SelectField
                                        floatingLabelText="Estado Civil"
                                        onChange={this.handleChangeCivilState}
                                        style={styles.inputs}
                                        value={this.state.estadoCivil ? this.state.estadoCivil : ''}
                                        selectedMenuItemStyle={{color: colorCustumized}}
                                        errorText={this.getError("estadoCivil")}
                                    >
                                        <MenuItem value={1} primaryText="Soltero" />
                                        <MenuItem value={2} primaryText="Casado" />
                                        <MenuItem value={3} primaryText="Viudo" />
                                        <MenuItem value={4} primaryText="Divorciado" />
                                    </SelectField>

                                    <Form.Item>
                                        {getFieldDecorator('estadoCivil', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <div style={{display: 'none'}}>
                                                <TextField />
                                            </div>

                                        )}
                                    </Form.Item>
                                </Col>


                                <Col lg={8} md={8} sm={8}>
                                    <SelectField
                                        floatingLabelText="Nacionalidad"
                                        onChange={this.handleChangeNationality}
                                        style={styles.inputs}
                                        value={this.state.nacionalidad ? this.state.nacionalidad : ''}
                                        selectedMenuItemStyle={{color: colorCustumized}}
                                        errorText={this.getError("nacionalidad")}
                                    >
                                        <MenuItem value={1} primaryText="Venezolano" />
                                        <MenuItem value={2} primaryText="Extranjero" />
                                    </SelectField>

                                    <Form.Item>
                                        {getFieldDecorator('nacionalidad', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <div style={{display: 'none'}}>
                                                <TextField />
                                            </div>

                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>

                            <div id={'ciudadOrigen'}></div>
                            <div id={'ciudadActual'}></div>
                            <div id={'estado'}></div>

                            <Row gutter={16}>
                                <Col lg={8} md={8} sm={8}>
                                    <DatePicker 
                                        hintText="Fecha de Nacimiento" 
                                        floatingLabelText="Fecha de Nacimiento"
                                        DateTimeFormat={ Intl.DateTimeFormat }
                                        formatDate={this.formatDate}
                                        textFieldStyle={styles.inputs}
                                        mode="landscape" 
                                        locale='es-ES' 
                                        value={this.state.fechaDeNacimiento ? new Date(this.state.fechaDeNacimiento) : ''}   
                                        errorText={this.getError("fechaDeNacimiento")}
                                        onChange={this.handleBirthDate}   
                                    />

                                    <Form.Item>
                                        {getFieldDecorator('fechaDeNacimiento', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <div style={{display: 'none'}}>
                                                <DatePicker />
                                            </div>

                                        )}
                                    </Form.Item>
                                </Col>


                                <Col lg={8} md={8} sm={8}>
                                    <SelectField
                                        floatingLabelText="Sexo"
                                        onChange={this.handleChangeSex}
                                        style={styles.inputs}
                                        value={this.state.sexo}
                                        selectedMenuItemStyle={{color: colorCustumized}}
                                        errorText={this.getError("sexo")}
                                    >
                                        <MenuItem value={1} primaryText="Masculino" />
                                        <MenuItem value={2} primaryText="Femenino" />
                                    </SelectField>

                                    <Form.Item>
                                        {getFieldDecorator('sexo', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <div style={{display: 'none'}}>
                                                <TextField />
                                            </div>

                                        )}
                                    </Form.Item>
                                </Col>

                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('codigoPostal', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Codigo Postal"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("codigoPostal")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>

                            <div id={'direccionPrimaria'}></div>
                            <div id={'direccionSecundaria'}></div>

                            <Row gutter={16}>
                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('ciudadOrigen', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Ciudad de Origen"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("ciudadOrigen")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>


                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('ciudadActual', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Ciudad Actual"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("ciudadActual")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>

                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('estado', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Estado"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("estado")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col lg={12} md={12} sm={12}>
                                    <Form.Item>
                                        {getFieldDecorator('direccionPrimaria', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(
                                            
                                            <TextField
                                                floatingLabelText="Direccion 1"
                                                type="text"
                                                multiLine={true}
                                                fullWidth={true}
                                                errorText={this.getError("direccionPrimaria")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>


                                <Col lg={12} md={12} sm={12}>
                                    <Form.Item>
                                        {getFieldDecorator('direccionSecundaria', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Direccion 2"
                                                type="text"
                                                multiLine={true}
                                                fullWidth={true}
                                                errorText={this.getError("direccionSecundaria")}
                                            />
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </div>   
                    </Card>             


                    <div id={'pnf'}></div>
                    <div id={'fechaIngreso'}></div>
                    <div id={'promedio'}></div>
                    <div id={'codigoEstudiante'}></div>
                    <div id={'turno'}></div>


                    <Card title="Datos Universitarios" className={'card-antd'} bordered={false} loading={this.state.loadingData}>
                        <div className={'card-antd-inputs'}>
                            <Row gutter={16}>
                                <Col lg={5} md={5} sm={5}>
                                    <Form.Item>
                                        {getFieldDecorator('pnf', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="PNF"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("pnf")}
                                            />
                                        )}
                                    </Form.Item>

                                </Col>

                                <Col lg={5} md={5} sm={5}>
                                    <DatePicker
                                        hintText="Fecha de Ingreso"
                                        floatingLabelText="Fecha de Ingreso"
                                        DateTimeFormat={ Intl.DateTimeFormat }
                                        formatDate={this.formatDate}
                                        textFieldStyle={styles.inputs}
                                        mode="landscape"
                                        locale='es-ES'
                                        value={this.state.fechaIngreso ? new Date(this.state.fechaIngreso) : ''}
                                        errorText={this.getError("fechaIngreso")}
                                        onChange={this.handleDate}
                                    />

                                    <Form.Item>
                                        {getFieldDecorator('fechaIngreso', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <div style={{display: 'none'}}>
                                                <DatePicker />
                                            </div>

                                        )}
                                    </Form.Item>
                                </Col>

                                <Col lg={4} md={4} sm={4}>
                                    <Form.Item>
                                        {getFieldDecorator('promedio', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Promedio"
                                                type="number"
                                                style={styles.inputs}
                                                errorText={this.getError("promedio")}
                                            />


                                        )}
                                    </Form.Item>
                                </Col>




                                <Col lg={5} md={5} sm={5}>
                                    <Form.Item>
                                        {getFieldDecorator('codigoEstudiante', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Codigo"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("codigoEstudiante")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>

                                <Col lg={5} md={5} sm={5}>
                                    <SelectField
                                        floatingLabelText="Turno"
                                        onChange={this.handleChangeTurn}
                                        style={styles.inputs}
                                        value={this.state.turno}
                                        selectedMenuItemStyle={{color: colorCustumized}}
                                        errorText={this.getError("turno")}
                                    >
                                        <MenuItem value={1} primaryText="Mañana" />
                                        <MenuItem value={2} primaryText="Tarde" />
                                        <MenuItem value={3} primaryText="Noche" />

                                    </SelectField>

                                    <Form.Item>
                                        {getFieldDecorator('turno', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <div style={{display: 'none'}}>
                                                <TextField />
                                            </div>

                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </div>
                    </Card>

                    <div id={'actividad'}></div>
                    <div id={'empresa'}></div>
                    <div id={'telefonoEmpresa'}></div>
                    <div id={'direccionEmpresa'}></div>

                    <Card title="Datos de Trabajo" className={'card-antd'} bordered={false} loading={this.state.loadingData}>
                        <div className={'card-antd-content'}>
                            <Row gutter={16}>
                                <Col lg={8} md={8} sm={8}>
                                    <p>Trabaja?</p>

                                    <RadioButtonGroup style={{display: 'flex', flexDirection: 'row'}} name="shipSpeed" defaultSelected={false} valueSelected={this.state.trabaja}>
                                        <RadioButton value={true} label="Si" style={styles.radioButton} onClick={this.handleJobTrue}/>
                                        <RadioButton value={false} label="No" style={styles.radioButton} onClick={this.handleJobFalse}/>
                                    </RadioButtonGroup>
                                </Col> 
                            </Row>

                            <Collapse isOpen={this.state.collapseJob}>
                                <Row gutter={16}>            
                                    <Col lg={8} md={8} sm={8}>
                                        <Form.Item>

                                            {
                                                this.state.collapseJob ? 
                                                (

                                                    getFieldDecorator('actividad', {
                                                        rules: [{ required: true, message: 'Este campo es requerido' }],
                                                    })(

                                                        <TextField
                                                            floatingLabelText="Actividad"
                                                            type="text"
                                                            multiLine={true}
                                                            style={styles.inputs}
                                                            errorText={this.getError("actividad")}
                                                        />

                                                    )
                                                ):

                                                (

                                                    <TextField
                                                        floatingLabelText="Actividad"
                                                        type="text"
                                                        multiLine={true}
                                                        style={styles.inputs}
                                                        errorText={this.getError("actividad")}
                                                    />

                                                )
                                            }
                                            
                                        </Form.Item>
                                        
                                    </Col>

                                    <Col lg={8} md={8} sm={8}>
                                        <Form.Item>
                                            {
                                                this.state.collapseJob ? 
                                                (

                                                    getFieldDecorator('empresa', {
                                                        rules: [{ required: true, message: 'Este campo es requerido' }],
                                                    })(


                                                        <TextField
                                                            floatingLabelText="Nombre de la Empresa"
                                                            type="text"
                                                            multiLine={true}
                                                            style={styles.inputs}
                                                            errorText={this.getError("empresa")}
                                                        />

                                                    )
                                                ):

                                                (

                                                    <TextField
                                                        floatingLabelText="Nombre de la Empresa"
                                                        type="text"
                                                        multiLine={true}
                                                        style={styles.inputs}
                                                        errorText={this.getError("empresa")}
                                                    />

                                                )
                                            }
                                        </Form.Item>
                                    </Col>                                

                                    <Col lg={8} md={8} sm={8}>
                                        <Form.Item>
                                            {
                                                this.state.collapseJob ? 
                                                (

                                                    getFieldDecorator('telefonoEmpresa', {
                                                        rules: [{ required: true, message: 'Este campo es requerido' }],
                                                    })(


                                                        <TextField
                                                            floatingLabelText="Telefono de la Empresa"
                                                            type="text"
                                                            style={styles.inputs}
                                                            errorText={this.getError("telefonoEmpresa")}
                                                        />

                                                    )
                                                ):

                                                (

                                                    <TextField
                                                        floatingLabelText="Telefono de la Empresa"
                                                        type="text"
                                                        style={styles.inputs}
                                                        errorText={this.getError("telefonoEmpresa")}
                                                    />

                                                )
                                            }
                                        </Form.Item>
                                    </Col>
                                </Row>

                                <Row gutter={16}>
                                    <Col lg={24} md={8} sm={8}>
                                        <Form.Item>
                                            {
                                                this.state.collapseJob ? 
                                                (

                                                    getFieldDecorator('direccionEmpresa', {
                                                        rules: [{ required: true, message: 'Este campo es requerido' }],
                                                    })(

                                                        <TextField
                                                            floatingLabelText="Direccion de la Empresa"
                                                            type="text"
                                                            fullWidth={true}
                                                            errorText={this.getError("direccionEmpresa")}
                                                        />

                                                    )
                                                ):

                                                (

                                                    <TextField
                                                        floatingLabelText="Direccion de la Empresa"
                                                        type="text"
                                                        fullWidth={true}
                                                        errorText={this.getError("direccionEmpresa")}
                                                    />

                                                )
                                            }
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Collapse>
                        </div>      
                    </Card>

                    <div id={'peso'}></div>
                    <div id={'estatura'}></div>
                    <div id={'talla'}></div>
                    <div id={'numeroCalzado'}></div>
                    <div id={'grupoSanguineo'}></div>

                    <Card title="Datos Antropometricos" className={'card-antd'} bordered={false} loading={this.state.loadingData}>
                        <div className={'card-antd-inputs'}>
                            <Row gutter={16}>
                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('peso', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Peso"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("peso")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>

                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('estatura', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(
                                            <TextField
                                                floatingLabelText="Estatura"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("estatura")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col> 

                                <Col lg={8} md={8} sm={8}>
                                    <Form.Item>
                                        {getFieldDecorator('talla', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Talla de franela"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("talla")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col>

                                <Col lg={12} md={12} sm={12}> 
                                    <Form.Item>
                                        {getFieldDecorator('numeroCalzado', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Numero Calzado"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("numeroCalzado")}
                                            />

                                        )}
                                    </Form.Item>

                                    
                                </Col> 

                                <Col lg={12} md={12} sm={12}>
                                    <Form.Item>
                                        {getFieldDecorator('grupoSanguineo', {
                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                        })(

                                            <TextField
                                                floatingLabelText="Grupo Sanguineo"
                                                type="text"
                                                style={styles.inputs}
                                                errorText={this.getError("grupoSanguineo")}
                                            />

                                        )}
                                    </Form.Item>
                                </Col> 
                            </Row>
                        </div>  
                    </Card>

                    <div id={'tipoEnfermedad'}></div>
                    <div id={'tipoAlergia'}></div>


                    <Card className={'card-antd-footer'} bordered={false} loading={this.state.loadingData}>
                        <div className={'card-antd-content'}>

                            <Row gutter={16}>
                                <Col lg={{span:8}} md={{span:8}} sm={{span:8}}>
                                    <p>Posee alguna enfermedad?</p>
                                    <RadioButtonGroup style={{display: 'flex', flexDirection: 'row'}} name="shipSpeed" defaultSelected={false} valueSelected={this.state.enfermedad}>
                                        <RadioButton value={true} label="Si" style={styles.radioButton} onClick={this.handleDiseaseTrue}/>
                                        <RadioButton value={false} label="No" style={styles.radioButton} onClick={this.handleDiseaseFalse}/>
                                    </RadioButtonGroup>
                                </Col>


                                <Col lg={{span:8, offset: 4}} md={{span:8, offset: 4}} sm={{span:8, offset: 4}}>           
                                    <p>Posee alguna alergia?</p>
                                    <RadioButtonGroup style={{display: 'flex', flexDirection: 'row'}} name="shipSpeed" defaultSelected={false} valueSelected={this.state.alergico}>
                                        <RadioButton value={true} label="Si" style={styles.radioButton} onClick={this.handleAllergyTrue}/>
                                        <RadioButton value={false} label="No" style={styles.radioButton} onClick={this.handleAllergyFalse}/>
                                    </RadioButtonGroup>
                                </Col> 
                            </Row>

                            <Row gutter={16}>
                                <Row gutter={16}>
                                    <Col lg={12} md={12} sm={12}>
                                        <Collapse isOpen={this.state.collapseDisease}>
                                            <Form.Item>
                                                {
                                                    this.state.collapseDisease ? 
                                                    (

                                                        getFieldDecorator('tipoEnfermedad', {
                                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                                        })(

                                                            <TextField
                                                                floatingLabelText="Tipo de Enfermedad"
                                                                type="text"
                                                                fullWidth={true}
                                                                multiLine={true}
                                                                errorText={this.getError("tipoEnfermedad")}
                                                            />   

                                                        )
                                                    ):

                                                    (

                                                        <TextField
                                                            floatingLabelText="Tipo de Enfermedad"
                                                            type="text"
                                                            fullWidth={true}
                                                            multiLine={true}
                                                            errorText={this.getError("tipoEnfermedad")}
                                                            value={''}
                                                        /> 

                                                    )
                                                }
                                            </Form.Item>
                                        </Collapse>    
                                    </Col>

                                    <Col lg={12} md={12} sm={12}>
                                        <Collapse isOpen={this.state.collapseAllergy}>
                                            <Form.Item>
                                                {
                                                    this.state.collapseAllergy ? 
                                                    (

                                                        getFieldDecorator('tipoAlergia', {
                                                            rules: [{ required: true, message: 'Este campo es requerido' }],
                                                        })(

                                                            <TextField
                                                                floatingLabelText="Tipo de Alergia"
                                                                type="text"
                                                                fullWidth={true}
                                                                multiLine={true}
                                                                errorText={this.getError("tipoAlergia")}
                                                            />      

                                                        )
                                                    ):

                                                    (

                                                        <TextField
                                                            floatingLabelText="Tipo de Alergia"
                                                            type="text"
                                                            fullWidth={true}
                                                            multiLine={true}
                                                            errorText={this.getError("tipoAlergia")}
                                                            value={''}
                                                        /> 

                                                    )
                                                }
                                            </Form.Item>
                                        </Collapse>    
                                    </Col>   
                                </Row>
                            </Row>
                        </div>  
                    </Card>

                    <Snackbar
                        open={this.state.open}
                        message={this.state.message}
                        autoHideDuration={3000}
                        onRequestClose={this.handleRequestClose}
                    />

                    <FooterToolbar>
                        <div className={'buttonFooter'}>  
                            {getErrorInfo()}

                            <RaisedButton
                                className={'button'}
                                primary={true}
                                label="Guardar"
                                labelStyle={{fontSize: 12}}
                                type="submit"
                                onClick={this.handleSubmit}
                                icon={<i className="fa fa-dot-circle-o" style={{color: '#fff'}}></i>}
                            />

                            <RaisedButton
                                label={'Deshacer'}
                                labelStyle={{fontSize: 12}}
                                labelColor={'#263238'}
                                type="reset"
                                icon={<i className="fa fa-ban" style={{color: colorCustumized}}></i>}
                            />                    
                        </div>
                    </FooterToolbar>
                </Form> 
            </div>

            
        );
      }
}

export default Form.create()(DatosPersonales);

DatosPersonales.contextTypes = {
    router: PropTypes.object.isRequired
};