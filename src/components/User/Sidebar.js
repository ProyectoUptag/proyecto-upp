import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

class Sidebar extends Component {

  handleClick(e) {
    e.preventDefault();
    e.target.parentElement.classList.toggle('open');
  }

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
  }

  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
  // }

  render() {
    return (
      <div className="sidebar">
        <nav className="sidebar-nav">
          <ul className="nav">
            <li className="nav-item">
              <NavLink to={'/user'} className="nav-link" activeClassName="active"><i className="icon-speedometer"></i>Inicio</NavLink>
            </li>
            <li className="nav-title">
              MENUS
            </li>
            <li className={this.activeRoute("/user/solicitudes")}>
              <a className="nav-link nav-dropdown-toggle" href="" onClick={this.handleClick.bind(this)}><i className="fa fa-mortar-board"></i> Solicitudes </a>
              <ul className="nav-dropdown-items">
                <li className="nav-item">
                  <NavLink to={'/user/solicitudes/nueva-solicitud'} className="nav-link" activeClassName="active"><i className="fa fa-thumbs-up"></i> Nueva solicitud</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to={'/user/solicitudes/historial'} className="nav-link" activeClassName="active"><i className="fa fa-group"></i> Historial</NavLink>
                </li>
              </ul>
            </li>
            <li className={this.activeRoute("/user/practicas-profesionales")}>
              <a className="nav-link nav-dropdown-toggle" href="" onClick={this.handleClick.bind(this)}><i className="fa fa-book"></i> Practicas <span style={{marginLeft: 30}}>Profesionales</span> </a>
              <ul className="nav-dropdown-items">
                <li className="nav-item">
                  <NavLink to={'/user/practicas-profesionales/cursando'} className="nav-link" activeClassName="active"><i className="fa fa-pencil"></i> Practica en curso </NavLink>
                </li>
              </ul>
            </li>
            <li className={this.activeRoute("/user/datos-personales")}>
              <a className="nav-link nav-dropdown-toggle" href="" onClick={this.handleClick.bind(this)}><i className="fa fa-book"></i> Datos personales</a>
              <ul className="nav-dropdown-items">
                <li className="nav-item">
                  <NavLink to={'/user/datos-personales/editar'} className="nav-link" activeClassName="active"><i className="fa fa-pencil"></i> Editar </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}

export default Sidebar;
