const routes = {
  '/user': 'Inicio',
  '/user/datos-personales': 'Datos Personales',
  '/user/datos-personales/editar': 'Editar',
  '/user/solicitudes': 'Solicitudes',
  '/user/solicitudes/nueva-solicitud': 'Nueva Solicitud',
  '/user/solicitudes/historial': 'Historial',
  '/user/practicas-profesionales': 'Practica Profesional',
  '/user/practicas-profesionales/cursando': 'Cursando',
};
export default routes;
