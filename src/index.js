//Dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import './g2';

// Rutes
import AppRoutes from './AppRoutes';

//Styles
import './index.less';

injectTapEventPlugin();

ReactDOM.render(

	<Router>
		<AppRoutes/>
	</Router>,
	document.getElementById('app')
);