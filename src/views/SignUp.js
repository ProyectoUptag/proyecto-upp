//Dependencies
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import DjangoCSRFToken from 'django-react-csrftoken'
import axios from 'axios'

// Ant Design
import { Card, Col, Row } from 'antd';

// Material UI
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';
import ExpandTransition from 'material-ui/internal/ExpandTransition';
import CircularProgress from 'material-ui/CircularProgress';
import { Step, Stepper, StepLabel } from 'material-ui/Stepper';

//Auth
import auth from '../Auth';

//Validator
import update from 'react-addons-update';
import {MakeRule, RunRules} from '../validation/RuleRunner';
import {required, mustMatch, minLength, emailRule} from '../validation/Rules';
import {objectIsEmpty} from '../utils/Utils';


//URLS
import {URLS} from '../config';

const colorDisabled = 'rgba(244, 177, 63, 0.89)';

const styles = {   
    buttons: {
    	textAlign: 'center',
        marginTop: 40,
        marginBottom: 5
    }
};


export default class SignUp extends Component {

    constructor() {
        super();

        this.state = {
            showView: false,
            showErrors: false,
            validationMessages: {},
            open: false,
            message: '',
            loading: false,
            finished: false,
            stepIndex: 0,
            username: '',
            email: '',
            password: '',
            passwordConfirm: '',
            cedula: '',
            boton: 'siguiente',
            loadingButton: false
        };

        this.rulesValidations = [
            MakeRule("username", "Usuario", required, minLength(4)),
            MakeRule("email", "Email", required, emailRule),
            MakeRule("password", "Contrasena", required, minLength(6)),
            MakeRule("passwordConfirm", "Contrasena2", required, mustMatch("password", "passwordConfirm"))
        ];

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFieldChanged = this.handleFieldChanged.bind(this);
        this.getError = this.getError.bind(this);
    }

    componentWillMount() {

        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";

        this.setState({
            showErrors: false,
            validationMessages: RunRules(this.state, this.rulesValidations)
        });

        if (!auth.loggedIn()) {
            this.setState({showView: true})
        } else {
            fetch(URLS.SERVER_URL + '/api/users/i', {
                method: 'GET',
                headers: new Headers({
                    'Authorization': 'Token ' + localStorage.token
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data.detail) {
                        console.error(data.detail)
                        this.context.router.history.push('/iniciar', null);
                    } else {
                        if (data.is_staff || data.is_superuser) {
                            this.context.router.history.push('/admin', null);
                        } else {
                            this.context.router.history.push('/user', null);
                        }
                    }
                })
                .catch(e => console.error('Ha ocurrido un error'));

        }
    }

    getError(field) {
        if (this.state.showErrors) {
            return this.state.validationMessages[field] || "";
        }
    }

    handleFieldChanged(field) {

        return (e) => {
            let newState = update(this.state, {
                [field]: {$set: e.target.value}
            });

            newState.validationMessages = RunRules(newState, this.rulesValidations);

            this.setState(newState);
        };
    }

    dummyAsync = (cb) => {
        this.setState({loading: true}, () => {
            this.asyncTimer = setTimeout(cb, 500);
        });
    };

    handleNext = () => {
        const {stepIndex} = this.state;
        if (!this.state.loading) {
            this.dummyAsync(() => this.setState({
                loading: false,
                stepIndex: stepIndex + 1,
                finished: stepIndex >= 2,
            }));
        }

        if (stepIndex === 0) {
            this.setState({
                showErrors: false,
                validationMessages: RunRules(this.state, this.rulesValidations)
            });
        }
    };

    handlePrev = () => {
        const {stepIndex} = this.state;
        if (!this.state.loading) {
            this.dummyAsync(() => this.setState({
                loading: false,
                stepIndex: stepIndex - 1,
            }));
        }
    };

    getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return (

                    <div style={{textAlign: 'center'}}>
                        <div>
                            <h1>Verificación</h1>
                            <p className="text-muted">Ingrese su cedula para validar sus datos</p>
                        </div>

                        <TextField
                            type="text"
                            floatingLabelText="Cedula"
                            onChange={this.changeInputCedula}
                        />
                    </div>

                );
            case 1:
                return (
                    <div>
                        <div style={{textAlign: 'center'}}>
                            <h1>Registro</h1>
                            <p className="text-muted">Registrate en la Unidad de Practicas Profesionales</p>
                        </div>

                        <form onSubmit={this.handleSubmit}>
                            <DjangoCSRFToken/>

                            <Row gutter={8} type="flex" justify="center" style={{textAlign: 'center'}}>
								<Col lg={{span: 10}} md={24} sm={24}>
							        <TextField
							          	floatingLabelText="Usuario"
							          	type="text"
							          	errorText={this.getError("username")}
							          	onChange={this.handleFieldChanged("username")}
							      	/>
								</Col>
								<Col lg={{span: 10}} md={24} sm={24}>
								    <TextField
								      	floatingLabelText="Correo electrónico"
								      	type="email"
								      	errorText={this.getError("email")}
								      	onChange={this.handleFieldChanged("email")}
								    />
								</Col>
							</Row>

                            <Row gutter={8} type="flex" justify="center" style={{textAlign: 'center'}}>
                                <Col lg={{span: 10}} md={24} sm={24}>

                                    <TextField
                                        floatingLabelText="Contraseña"
                                        type="password"
                                        errorText={this.getError("password")}
                                        onChange={this.handleFieldChanged("password")}
                                    />
                                </Col>
                                <Col lg={{span: 10}} md={24} sm={24}>
                                    <TextField
                                        floatingLabelText="Confirmar contraseña"
                                        type="password"
                                        errorText={this.getError("passwordConfirm")}
                                        onChange={this.handleFieldChanged("passwordConfirm")}
                                    />
                                </Col>
                            </Row> 

                        </form>
                    </div>
                );
            case 2:
                return (
                    <p>
                        Usted se ha registrado exitosamente.
                    </p>
                );
            default:
                return 'You\'re a long way from home sonny jim!';
        }
    }

    handleSubmit = () => {
        this.setState({showErrors: true});

        if (objectIsEmpty(this.state.validationMessages)) {

            let _username = this.state.username;
            let _email = this.state.email;
            let _password = this.state.password;

            axios({
                url: URLS.SERVER_URL + '/api/users/',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    username: _username,
                    email: _email,
                    password: _password,
                }),
                transformResponse: [function (data) {
                    // Do whatever you want to transform the data
                    
                    return data;
                }],
            })
            .then(data => {
                this.handleNext()
            })
            .catch(function(err){
                let response = JSON.parse(err.response.data)

                if (response.username){
                    this.setState({
                        open: true,
                        message: response.username[0]
                    });
                }
                else if (response.email){
                    this.setState({
                        open: true,
                        message: response.email[0]
                    });
                }
                else if (response.password){
                    this.setState({
                        open: true,
                        message: response.password[0]
                    });
                }else{
                    console.error('Ha ocurrido un error al realizar la consulta')
                }
            }.bind(this));
        }
    };


    changeInputCedula = (e) => {
        this.setState({
            cedula: e.target.value
        });
    }

    changeLoadingButton = (e) => {
        if (this.state.loadingButton) {
            return <CircularProgress size={26} thickness={3}/>
        } else {
            return null
        }
    }

    handleTouchTap = (e) => {
        this.setState({
            boton: '',
            loadingButton: true
        });


        this.timer = setTimeout(() => {
            this.handleNext()
            this.setState({
                boton: 'siguiente',
                loadingButton: false
            });
        }, 4000);

        // let _cedula = this.state.cedula;

        // this.setState({
        //     boton: '',
        //     loadingButton: true
        // });

        // axios.get(URLS.API_URL, {
        //     params: {
        //         cedula: _cedula
        //     }
        // })
        // .then((response) => {
        //     console.log(response.data[0].documento_identidad)
        //     if (response.data[0].documento_identidad !== ""){
        //         this.handleNext()

        //         this.setState({
        //             boton: 'siguiente',
        //             loadingButton: false
        //         });
        //     }
        //     else{
        //         this.setState({
        //             open: true,
        //             message: 'No se encuentra registrado en la base de datos del UPTAG',
        //             boton: 'siguiente',
        //             loadingButton: false
        //         });
        //     }

        // })
        // .catch((data) => {
        //     this.setState({
        //         open: true,
        //         message: 'Ha ocurrido un error al realizar la consulta',
        //         boton: 'siguiente',
        //         loadingButton: false
        //     });

        //     this.timer = setTimeout(() => {
        //         this.setState({
        //             message: `Verifique su conexion a internet`,
        //         });
        //     }, 4000);

        // });        
    };

    handleRequestClose = () => {
        this.setState({
            open: false,
            message: ''
        });
    };

    renderContent() {
        const {finished, stepIndex} = this.state;
        const contentStyle = {margin: '0 16px', overflow: 'hidden'};

        if (finished) {
            return (
                <div style={contentStyle}>
                    <p>
                        Click aquí para <Link to="/iniciar">iniciar sesion</Link>
                    </p>
                </div>
            );
        }

        return (
            <div>
                <div>{this.getStepContent(stepIndex)}</div>
                <div style={styles.buttons}>
                    <Link to={stepIndex === 0 ? '/iniciar': this.handlePrev}>
                        <RaisedButton
                            label={stepIndex === 0 ? 'Volver': 'Atras'}
                            labelColor={'#263238'}
                            onTouchTap={this.handlePrev}
                            style={{marginRight: 12}}
                            disabled={stepIndex === 2 ? true : false}
                        />
                    </Link>
                    <RaisedButton
                        primary={true}
                        label={stepIndex === 2 ? 'Finalizar' : this.state.boton}
                        disabled={this.state.loadingButton}
                        disabledBackgroundColor={colorDisabled}
                        onTouchTap={stepIndex === 1 ? this.handleSubmit : this.handleTouchTap}
                        icon={this.changeLoadingButton()}
                    />
                </div>
            </div>
        );
    }


    render() {
        const {loading, stepIndex} = this.state;

        if (this.state.showView) {
            return (
                <div className={styles.fondo}>
                    <div className="app flex-row align-items-center">
                        <div className="container animated fadeIn">
                            <div className="row justify-content-center">
                                <div className="col-md-10">
                                    <Card bordered={false}>
                                        <div className="card-block">
                                            <div style={{width: '100%', maxWidth: 700, margin: 'auto'}}>
                                                <Stepper className={'stepper'} activeStep={stepIndex}>
                                                    <Step>
                                                        <StepLabel>Verificacion</StepLabel>
                                                    </Step>

                                                    <Step>
                                                        <StepLabel>Registro</StepLabel>
                                                    </Step>

                                                    <Step>
                                                        <StepLabel>Finalizado</StepLabel>
                                                    </Step>
                                                </Stepper>

                                                <ExpandTransition loading={loading} open={true}>
                                                    {this.renderContent()}
                                                </ExpandTransition>
                                            </div>


                                            <Snackbar
                                                open={this.state.open}
                                                message={this.state.message}
                                                autoHideDuration={6000}
                                                onRequestClose={this.handleRequestClose}
                                            />
                                        </div>
                                    </Card>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
}

SignUp.contextTypes = {
    router: PropTypes.object.isRequired
};