//Dependencies
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import axios from 'axios';

// Ant Design
import { Card } from 'antd';

// Material UI
import RaisedButton from 'material-ui/RaisedButton';
import {grey500, white} from 'material-ui/styles/colors';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import Person from 'material-ui/svg-icons/social/person';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';

// Materialize
import {Input} from 'react-materialize';

//Auth
import auth from '../Auth';

//URLS
import { URLS } from '../config';

const colorCustumized = 'rgba(244, 177, 63, 0.96)'

const styles = {
    buttonsDiv: {
        textAlign: 'center',
        padding: 20
    },
    buttonsSocials: {
        textAlign: 'center',
        marginTop: 30
    },
    checkRemember: {
        style: {
            marginTop: 15,
            marginBottom: 15
        },
        labelStyle: {
            color: grey500
        },
        iconStyle: {
            color: grey500,
            borderColor: grey500,
            fill: grey500
        }
    },
    btn: {
        color: white,
        padding: 8,
        borderRadius: 2,
        margin: 2,
        fontSize: 13
    },
    btnFacebook: {
        background: '#4f81e9'
    },
    btnGoogle: {
        background: '#e14441'
    },
    btnSpan: {
        marginLeft: 5
    },
};

export default class Login extends Component {

    constructor() {
        super();

        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            showView: false,
            data: [],
            user: '',
            passwd: '',
            errorUser: '',
            errorPasswd: '',
            recDatos: false,
            open: false,
            message: 'No existe ningun registro con esos datos'
        }
    }

    componentWillMount() {
    	axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
    	axios.defaults.xsrfCookieName = "csrftoken";

        if (!auth.loggedIn()) {
            this.setState({showView: true})
        }else{
        	axios({
        	    url: URLS.SERVER_URL + '/api/users/i/',
        	    method: 'GET',
        	    headers: {
        	        'Authorization': 'Token ' + localStorage.token
        	    }
        	})
        	.then(data => {
        	    if (data.detail){
        	        console.error(data.detail)
        	        this.setState({showView: true})
        	        this.context.router.history.push('/iniciar', null);
        	    }else{
        	        if (data.is_staff || data.is_superuser){
        	            this.context.router.history.push('/admin', null);
        	        }else{
        	            this.context.router.history.push('/user', null);
        	        }
        	    }
        	})
        	.catch( e => console.error( 'Ha ocurrido un error' ));


            
            
        }
    }

    handleTouchTap = () => {
        this.setState({
            open: true,
        });

        this.timer = setTimeout(() => {
            this.setState({
                message: `Regístrese para iniciar sesion`,
            });
        }, 4000);
    };

    handleRequestClose = () => {
        this.setState({
            open: false,
            message: 'No existe ningun registro con esos datos'
        });
    };

    handleSubmit(e) {
        e.preventDefault()

        let user = this.state.user;
        let passwd = this.state.passwd;
        let recDatos = this.state.recDatos;

        if (user === "" && passwd === "") {
            this.setState({
                errorUser: "Este campo es requerido!",
                errorPasswd: "Este campo es requerido!",
            });
        }

        else if (user === "") {
            this.setState({
                errorUser: "Este campo es requerido!",
            });
        }
        else if (passwd === "") {
            this.setState({
                errorPasswd: "Este campo es requerido!",
            });
        } else {
            this.setState({
                errorUser: "",
                errorPasswd: "",
            });

            auth.login(user, passwd, recDatos, (log) => {
                if (log){
                    fetch(URLS.SERVER_URL + '/api/users/i/', {
                        method: 'GET',
                        headers: new Headers({
                            'Authorization': 'Token ' + localStorage.token
                        })
                    })
                    .then( response => response.json())
                    .then( data => {
                        this.setState({data:data})
                        if (!this.state.data.is_staff || !this.state.data.is_superuser){
                            this.context.router.history.push('/user', null);
                        }else{
                            this.context.router.history.push('/admin', null);
                        }
                    })
                    .catch( e => console.error( 'Ha ocurrido un error' ));


                }
                else {
                    this.handleTouchTap()
                }

            })
        }
    }

    handleCheckbox = () => {
        this.setState({
            recDatos: !this.state.recDatos
        });
    }

    changeInputUser = (e) => {
        this.setState({
            user: e.target.value,
            errorUser: "",
            errorPasswd: ""
        });
    }

    changeInputPasswd = (e) => {
        this.setState({
            passwd: e.target.value,
            errorUser: "",
            errorPasswd: ""
        });
    }

    render() {
        if(this.state.showView){
            return (
                <div className={styles.fondo}>
                    <div className="app flex-row align-items-center">
                        <div className="container animated fadeIn">
                            <div className="row justify-content-center">
                                <div className="col-md-10">
                                    <div className="card-group mb-0">
                                        <Card bordered={false} className="card p-8" style={{paddingTop: 24}}>
                                            <div className="card-block">

                                                <h1>Iniciar sesion</h1>
                                                <p className="text-muted">Inicia sesion con tu cuenta</p>

                                                <form onSubmit={this.handleSubmit}>
                                                    <div className="row">
                                                        <div className="col-12">

                                                            <TextField
                                                                style={{marginTop: -10}}
                                                                floatingLabelText="Usuario"
                                                                fullWidth={true}
                                                                type="text"
                                                                value={this.state.user}
                                                                errorText={this.state.errorUser}
                                                                onChange={this.changeInputUser}
                                                            />
                                                            <TextField
                                                                floatingLabelText="Contraseña"
                                                                fullWidth={true}
                                                                type="password"
                                                                value={this.state.passwd}
                                                                errorText={this.state.errorPasswd}
                                                                onChange={this.changeInputPasswd}
                                                            />

                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <Input type='checkbox' label='Recordar contraseña' onClick={this.handleCheckbox}/>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-6">

                                                            <RaisedButton
                                                                primary={true}
                                                                label="Iniciar"
                                                                labelStyle={{fontSize: 12}}
                                                                icon={<Person/>}
                                                                type='submit'
                                                            />

                                                        </div>

                                                        <div className="col-6">
                                                            <Link to="/password-forgot">                                                            
                                                                <button type="button" className="btn btn-link px-0">Olvidó su contraseña?</button>                                                        
                                                            </Link>
                                                            
                                                        </div>

                                                    </div>

                                                </form>

                                                <Snackbar
                                                    open={this.state.open}
                                                    message={this.state.message}
                                                    autoHideDuration={6000}
                                                    onRequestClose={this.handleRequestClose}
                                                />
                                            </div>
                                        </Card>

                                        <div className="card-inverse card-primary py-5 d-md-down-none"
                                             style={{width: 44 + '%'}}>
                                            <div style={{paddingTop: 65}} className="card-block text-center">
                                                <h2>Registrarse</h2>
                                                <p>Si eres estudiante de la UPTAG, al registrarte podras solicitar y realizar tus Practicas Profesionales.</p>
                                                <div style={styles.buttonsDiv}>
                                                    <Link to="/registrar">
                                                        <RaisedButton
                                                            default={true}
                                                            label="Registrate!"
                                                            labelColor={colorCustumized}
                                                            labelStyle={{fontSize: 12}}
                                                            className={'active'}
                                                            style={styles.flatutton}
                                                            icon={<PersonAdd />}
                                                        />
                                                    </Link>
                                                </div>

                                                <a rel="noreferrer noopener" target="_blank"  href="http://creativecommons.org/licenses/by-sa/4.0/"><img src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" alt="Licencia Creative Commons" /></a><br />Esta obra está bajo una Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }else{
            return null;
        }
    }
}

Login.contextTypes = {
    router: PropTypes.object.isRequired
};
