//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';

//Components
import Header from '../components/User/Header';
import Sidebar from '../components/User/Sidebar';
import Breadcrumb from '../components/User/Breadcrumb';
import DatosPersonales from '../components/User/DatosPersonales';
import NuevaSolicitud from '../components/User/NuevaSolicitud';
import Historial from '../components/User/Historial';
import PracticasProfesionales from '../components/User/PracticasProfesionales';
import Dashboard from '../components/User/Dashboard';

//Auth
import auth from '../Auth';

//URLS
import { URLS } from '../config';

class User extends Component{

    constructor() {
        super();
        this.state = {user: [], showView: false};

        this.logoutHandler = this.logoutHandler.bind(this);
    }

    componentWillMount() {
        if (!auth.loggedIn()) {
            this.context.router.history.push('/iniciar', null);
        }else{
            fetch(URLS.SERVER_URL + '/api/users/i/', {
                method: 'GET',
                headers: new Headers({
                    'Authorization': 'Token ' + localStorage.token
                })
            })
            .then( response => response.json())
            .then( data => {
                if (data.detail){
                    console.error(data.detail)
                    this.context.router.history.push('/iniciar', null);
                }else{
                    if (data.is_staff || data.is_superuser){
                        this.context.router.history.push('/admin', null);
                    }else{
                        localStorage.user_data = JSON.stringify(data)

                        this.setState({user:data, showView: true})

                        fetch(URLS.SERVER_URL + '/api/users/' + data.id + '/persona/', {
                            method: 'GET',
                            headers: new Headers({
                                'Authorization': 'Token ' + localStorage.token
                            })
                        })
                        .then( response => response.json())
                        .then( dp_data => {

                            fetch('http://daceos.uptag.edu.ve/api/estudiante.php?codigo='+ dp_data.datos_universitarios.codigo_est +'&oper=foto', {
                                method: 'GET',
                            })
                            .then( response => response.json())
                            .then( data_avatar => {

                                if(data_avatar.success === false){
                                    localStorage._a = 'static/img/user.png'
                                }else{
                                    localStorage._a = data_avatar.picture
                                }
                            }) 
                            .catch( e => {
                                console.error( 'Ha ocurrido un error' )
                                localStorage._a = 'static/img/user.png'
                            })
                        }) 
                        .catch( e => console.error( 'Ha ocurrido un error' ));
                    }
                }
            })
            .catch( e => console.error( 'Ha ocurrido un error' ));
            
        }
    }
            
    logoutHandler() {
        auth.logout()
        this.context.router.history.push('/iniciar', null);
    }

    render(){
        if(this.state.showView){
            return(
                <div className="app">
                    <Header/>
                    <div className="app-body">
                        <Sidebar {...this.props}/>
                        <main className="main">
                            <Breadcrumb />
                            <div>  
                                <Switch>
                                    <Route path='/user/practicas-profesionales/cursando' name="Practicas Profesionales" component={PracticasProfesionales}/>
                                    <Route path='/user/datos-personales/editar' name="Datos Personales" component={DatosPersonales}/>
                                    <Route path='/user/solicitudes/nueva-solicitud' name="Nueva Solicitud" component={NuevaSolicitud}/>
                                    <Route path='/user/solicitudes/historial' name="Historial" component={Historial}/>
                                    <Route path='/user' name="Inicio" component={Dashboard}/>
                                    <Redirect from="/" to="/user"/>
                                </Switch> 
                            </div>
                        </main>
                    </div>
                </div>
            );
        }else{
            return null;
        }
    }
}

export default User;

User.contextTypes = {
    router: PropTypes.object.isRequired
};



