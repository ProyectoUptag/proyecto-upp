//Dependencies
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Material UI
import RaisedButton from 'material-ui/RaisedButton';

export default class NotFoundPage extends Component {

	render() {
	  	return (
		    <div className="app flex-row align-items-center">
		      	<div className="container">
		        	<div className="row justify-content-center">
				        <div className="col-md-6">
				            <div className="clearfix">
				              	<h1 className="float-left display-3 mr-4">404</h1>
				              	<h4 className="pt-3">Lo sentimos! Esta direccion no existe.</h4>
				              	<p className="text-muted">Haz click en el boton para volver.</p>
				        		<Link to="/iniciar">                                                            		        		
				           			<RaisedButton
				           			    primary={true}
				           			    label="Volver"
				           			    labelStyle={{fontSize: 12}}
				           			/>
				        		</Link>
				            </div>
		          		</div>

		        	</div>
		      	</div>
		    </div>
	  	);
  	}
};
