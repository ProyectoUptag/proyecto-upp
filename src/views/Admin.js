//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';

//Components
import Header from '../components/Admin/Header';
import Sidebar from '../components/Admin/Sidebar';
import Breadcrumb from '../components/Admin/Breadcrumb';
//import Dashboard from '../components/Admin/Dashboard';
import Solicitudes from '../components/Admin/Solicitudes';
import PracticasProfesionales from '../components/Admin/PracticasProfesionales/index';
import Empresas from '../components/Admin/Empresas';
import Tutores from '../components/Admin/Tutores';
import Estadisticas from '../components/Admin/Estadisticas';


//Auth
import auth from '../Auth';

//URLS
import { URLS } from '../config';

class Admin extends Component{

	constructor() {
	  	super();
	  	this.state = {user: [], showView: false};

	  	this.logoutHandler = this.logoutHandler.bind(this);
	}

	componentWillMount() {
		if (!auth.loggedIn()) {
			this.context.router.history.push('/iniciar', null);
		}else{
			fetch(URLS.SERVER_URL + '/api/users/i', {
			    method: 'GET',
			    headers: new Headers({
			        'Authorization': 'Token ' + localStorage.token
			    })
			})
			.then( response => response.json())
			.then( data => {
				if (data.detail){
					console.error(data.detail)
					this.context.router.history.push('/iniciar', null);
				}else{
					if (!data.is_staff || !data.is_superuser){
					    this.context.router.history.push('/user', null);
					}else{
						this.setState({user:data, showView: true})
					}
				}
			})
			.catch( e => console.error( 'Ha ocurrido un error' ));

		}

	}

	logoutHandler() {
		auth.logout()
		this.context.router.history.push('/iniciar', null);
	}

	render(){

		if(this.state.showView){
			return(
				<div className="app">
					<Header/>
					<div className="app-body">
						<Sidebar {...this.props}/>
						<main className="main">
						  	<Breadcrumb />
					  		<div>
                                <Switch>
									<Route path='/admin/estadisticas' name="Estadisticas" component={Estadisticas}/>
									<Route path='/admin/tutores' name="Tutores" component={Tutores}/>
                                    <Route path='/admin/empresas' name="Empresas" component={Empresas}/>
                                    <Route path='/admin/practicas-profesionales/practicas' name="Practicas Profesionales" component={PracticasProfesionales}/>
                                    <Route path='/admin/solicitudes/historial' name="Solicitudes" component={Solicitudes}/>
                                    <Route path='/admin' name="Inicio" component={Estadisticas}/>
                                    <Redirect from="/" to="/admin"/>
                                </Switch>
                            </div>
						</main>
					</div>
				</div>
			);
		}else{
			return null;
		}
	}
}

export default Admin;

Admin.contextTypes = {
	router: PropTypes.object.isRequired
};
