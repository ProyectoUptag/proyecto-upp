//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class Index extends Component{

	constructor() {
	    super();
	    this.state = {showView: false};
	}

	componentWillMount() {
        this.context.router.history.push('/iniciar', null);

	    // if (!auth.loggedIn()) {
	    // 	this.setState({showView: true})
	    // }else{
	    //     fetch(URLS.SERVER_URL + '/api/users/i', {
	    //         method: 'GET',
	    //         headers: new Headers({
	    //             'Authorization': 'Token ' + localStorage.token
	    //         })
	    //     })
	    //     .then( response => response.json())
	    //     .then( data => {
	    //         if (data.detail){
	    //             console.error(data.detail)
	    //             this.context.router.history.push('/iniciar', null);
	    //         }else{
	    //             if (data.is_staff || data.is_superuser){
	    //                 this.context.router.history.push('/admin', null);
	    //             }else{
	    //                 this.context.router.history.push('/user', null);
	    //             }
	    //         }
	    //     })
	    //     .catch( e => console.error( 'Ha ocurrido un error' ));
	    //
	    // }
	}

	render(){
		if(this.state.showView){
			return(
				<div>
					<h1>Pagina Index</h1>
					
					<Link to={'/iniciar'}>
						<button>Iniciar Sesion</button>
					</Link>

					<Link to={'/registrar'}>
						<button>Registrarse</button>
					</Link>				
				</div>
			);
		}else{
		    return null;
		}
	}
}

Index.contextTypes = {
	router: PropTypes.object.isRequired
};