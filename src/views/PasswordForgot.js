//Dependencies
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import DjangoCSRFToken from 'django-react-csrftoken'
import axios from 'axios'

// Ant Design
import { Card, Col, Row, message } from 'antd';

// Material UI
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';
import ExpandTransition from 'material-ui/internal/ExpandTransition';
import CircularProgress from 'material-ui/CircularProgress';
import { Step, Stepper, StepLabel } from 'material-ui/Stepper';

//Auth
import auth from '../Auth';

//Validator
import update from 'react-addons-update';
import {MakeRule, RunRules} from '../validation/RuleRunner';
import {required, mustMatch, minLength, emailRule} from '../validation/Rules';
import {objectIsEmpty} from '../utils/Utils';


//URLS
import {URLS} from '../config';

const colorDisabled = 'rgba(244, 177, 63, 0.89)';

const styles = {
    buttons: {
    	textAlign: 'center',
        marginTop: 40,
        marginBottom: 5
    }
};


export default class SignUp extends Component {

    constructor() {
        super();

        this.state = {
            showView: false,
            showErrors: false,
            validationMessages: {},
            open: false,
            message: '',
            loading: false,
            finished: false,
            stepIndex: 0,
            password: '',
            passwordConfirm: '',
            email: '',
            boton: 'enviar',
            loadingButton: false,
            disabled: false,
            disabledSecondButton: false
        };

        this.rulesValidationEmail = [
        	MakeRule("email", "Email", required, emailRule)
        ];

        this.rulesValidations = [
            MakeRule("password", "Contrasena", required, minLength(6)),
            MakeRule("passwordConfirm", "Contrasena2", required, mustMatch("password", "passwordConfirm"))
        ];

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFieldChanged = this.handleFieldChanged.bind(this);
        this.getError = this.getError.bind(this);
    }

    componentWillMount() {


        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";

        if (window.location.hash.split("/")[2] !== undefined){
            fetch(URLS.SERVER_URL + '/confirm_password/verificar/' + window.location.hash.split("/")[2] + '/', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                return response.json();

            })
            .then(data => {
                if(data.existe){
                    this.handleNext()

                    this.setState({
                        disabled: true
                    });
                }else{
                    this.context.router.history.push('/password-forgot', null);
                }


            })
            .catch( e => console.error( 'Ha ocurrido un error' ));

        }

        message.config({
            top: 70,
            duration: 3,
        });

        this.setState({
            showErrors: false,
            validationMessages: RunRules(this.state, this.rulesValidationEmail),
            validationMessages: RunRules(this.state, this.rulesValidations)
        });

        if (!auth.loggedIn()) {
            this.setState({showView: true})
        } else {
            fetch(URLS.SERVER_URL + '/api/users/i', {
                method: 'GET',
                headers: new Headers({
                    'Authorization': 'Token ' + localStorage.token
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data.detail) {
                        console.error(data.detail)
                        this.context.router.history.push('/iniciar', null);
                    } else {
                        if (data.is_staff || data.is_superuser) {
                            this.context.router.history.push('/admin', null);
                        } else {
                            this.context.router.history.push('/user', null);
                        }
                    }
                })
                .catch(e => console.error('Ha ocurrido un error'));

        }
    }

    getError(field) {

        if (this.state.showErrors) {
            return this.state.validationMessages[field] || "";
        }
    }

    handleFieldChanged(field) {

        return (e) => {
            let newState = update(this.state, {
                [field]: {$set: e.target.value}
            });

            if(field === 'email'){
	            newState.validationMessages = RunRules(newState, this.rulesValidationEmail);

            }else{
	            newState.validationMessages = RunRules(newState, this.rulesValidations);

            }


            this.setState(newState);
        };
    }

    dummyAsync = (cb) => {
        this.setState({loading: true}, () => {
            this.asyncTimer = setTimeout(cb, 500);
        });
    };

    handleNext = () => {
        const {stepIndex} = this.state;
        if (!this.state.loading) {
            this.dummyAsync(() => this.setState({
                loading: false,
                stepIndex: stepIndex + 1,
                finished: stepIndex >= 2,
            }));
        }

        if (stepIndex === 0) {
            this.setState({
                showErrors: false,
                validationMessages: RunRules(this.state, this.rulesValidationEmail)
            });
        }
    };

    handlePrev = () => {
    	message.destroy()
        const {stepIndex} = this.state;
        if (!this.state.loading) {
            this.dummyAsync(() => this.setState({
                loading: false,
                stepIndex: stepIndex - 1,
            }));
        }
    };

    getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return (

                    <div style={{textAlign: 'center'}}>
                        <div>
                            <h1>Olvidó su contraseña?</h1>
                            <p className="text-muted">Ingrese su correo electrónico para buscar tu cuenta</p>
                        </div>

                        <form onSubmit={this.handleSendEmail}>
                            <DjangoCSRFToken/>
                            <TextField
                                floatingLabelText="Correo electrónico"
                                type="email"
                                errorText={this.getError("email")}
                                onChange={this.handleFieldChanged("email")}
                                value={this.state.email}
                            />
                        </form>
                    </div>

                );
            case 1:
                return (
                    <div>
                        <div style={{textAlign: 'center'}}>
                            <h1>Nueva contraseña</h1>
                            <p className="text-muted">Complete los campos para reestablecer su contraseña</p>
                        </div>

                        <form onSubmit={this.handleSubmit}>
                            <DjangoCSRFToken/>

                            <Row gutter={8} type="flex" justify="center" style={{textAlign: 'center'}}>
                                <Col lg={{span: 10}} md={24} sm={24}>

                                    <TextField
                                        floatingLabelText="Nueva contraseña"
                                        type="password"
                                        errorText={this.getError("password")}
                                        onChange={this.handleFieldChanged("password")}
                                    />
                                </Col>
                                <Col lg={{span: 10}} md={24} sm={24}>
                                    <TextField
                                        floatingLabelText="Confirmar contraseña"
                                        type="password"
                                        errorText={this.getError("passwordConfirm")}
                                        onChange={this.handleFieldChanged("passwordConfirm")}
                                    />
                                </Col>
                            </Row>

                        </form>
                    </div>
                );
            case 2:
                return (
                    <p>
                        Ha cambiado su contraseña exitosamente.
                    </p>
                );
        }
    }

    handleSubmit = () => {
        let _password = this.state.password;

        const data = new FormData();
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }

        this.setState({
            showErrors: true,
            validationMessages: RunRules(this.state, this.rulesValidations),
            disabled: true,
        });

        if (objectIsEmpty(this.state.validationMessages)) {

            data.append('password', _password);

            axios.post(URLS.SERVER_URL + '/confirm_password/guardar/' + window.location.hash.split("/")[2] + '/', data, config)
                .then((response) => {

                    if(response.data.guardado){
                        this.handleNext()
                    }else{
                        message.error('Ha ocurrido un error al cambiar su contraseña');
                    }


                })
        }
    };


    changeLoadingButton = (e) => {
        if (this.state.loadingButton) {
            return <CircularProgress size={26} thickness={3}/>
        } else {
            return null
        }
    }

    handleSendEmail = (e) => {
        let _email = this.state.email;

        const data = new FormData();
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }

        this.setState({
            showErrors: true,
            validationMessages: RunRules(this.state, this.rulesValidationEmail)
        });


        if (objectIsEmpty(this.state.validationMessages)) {

	        data.append('email', _email);

	        message.loading('Le estamos enviando un correo para reestablecer su contraseña', 0);

	        this.setState({
	            boton: '',
	            loadingButton: true
	        });

	        axios.post(URLS.SERVER_URL + '/reset_password/', data, config)
	        .then((response) => {
	            if(response.data.error){
	                message.destroy()
	                message.error(response.data.error);

	                this.setState({
	                    boton: 'enviar',
	                    loadingButton: false,
	                });
	            }
	            if (response.data.enviado){
	                message.destroy()
	                message.success('Le hemos enviado a su correo un enlace para restablacer su contraseña, revise su bandeja de entrada.',0);

	                this.setState({
	                	email: '',
	                    boton: 'enviado',
	                    loadingButton: false,
                        disabledSecondButton: true
	                });
	            }

	        })

	    }
    };

    handleRequestClose = () => {
        this.setState({
            open: false,
            message: ''
        });
    };

    handleTouchTap = () =>{
        const { stepIndex } = this.state;

        if(stepIndex === 0){
            this.handleSendEmail()
        }
        if(stepIndex === 1){
            this.handleSubmit()
        }
        if(stepIndex === 2){
            this.handleNext()
        }

    }

    renderContent() {
        const { finished, stepIndex, disabled } = this.state;
        const contentStyle = {margin: '0 16px', overflow: 'hidden'};

        if (finished) {
            return (
                <div style={contentStyle}>
                    <p>
                        Click aquí para <Link to="/iniciar">iniciar sesion</Link>
                    </p>
                </div>
            );
        }

        return (
            <div>
                <div>{this.getStepContent(stepIndex)}</div>
                <div style={styles.buttons}>
                    <Link to={stepIndex === 0 ? '/iniciar': this.handlePrev}>
                        <RaisedButton
                            label={stepIndex === 0 ? 'Volver': 'Atras'}
                            labelColor={'#263238'}
                            onTouchTap={this.handlePrev}
                            style={{marginRight: 12}}
                            disabled={disabled}
                        />
                    </Link>
                    <RaisedButton
                        primary={true}
                        label={stepIndex === 2 ? 'Finalizar' : this.state.boton}
                        disabled={this.state.loadingButton}
                        disabled={this.state.disabledSecondButton}
                        disabledBackgroundColor={colorDisabled}
                        onTouchTap={this.handleTouchTap}
                        icon={this.changeLoadingButton()}
                        type="submit"
                    />
                </div>
            </div>
        );
    }


    render() {
        const {loading, stepIndex} = this.state;

        if (this.state.showView) {
            return (
                <div className={styles.fondo}>
                    <div className="app flex-row align-items-center">
                        <div className="container animated fadeIn">
                            <div className="row justify-content-center">
                                <div className="col-md-10">
                                    <Card bordered={false}>
                                        <div className="card-block">
                                            <div style={{width: '100%', maxWidth: 700, margin: 'auto'}}>
                                                <Stepper className={'stepper'} activeStep={stepIndex}>
                                                    <Step>
                                                        <StepLabel>Verificacion</StepLabel>
                                                    </Step>

                                                    <Step>
                                                        <StepLabel>Reestablecer contraseña</StepLabel>
                                                    </Step>

                                                    <Step>
                                                        <StepLabel>Finalizado</StepLabel>
                                                    </Step>
                                                </Stepper>

                                                <ExpandTransition loading={loading} open={true}>
                                                    {this.renderContent()}
                                                </ExpandTransition>
                                            </div>


                                            <Snackbar
                                                open={this.state.open}
                                                message={this.state.message}
                                                autoHideDuration={6000}
                                                onRequestClose={this.handleRequestClose}
                                            />
                                        </div>
                                    </Card>
                                </div>
                            </div>
                        </div>
                    </div>

                    <Snackbar
                        open={this.state.open}
                        message={this.state.message}
                        autoHideDuration={6000}
                        onRequestClose={this.handleRequestClose}
                    />
                </div>
            );
        } else {
            return null;
        }
    }
}

SignUp.contextTypes = {
    router: PropTypes.object.isRequired
};
