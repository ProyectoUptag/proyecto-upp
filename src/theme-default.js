import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { white } from 'material-ui/styles/colors';

const colorCustumized = 'rgba(244, 177, 63, 0.96)';

const themeDefault = getMuiTheme({
  palette: {
  	primary1Color: colorCustumized,
  	primary2Color: colorCustumized,
  	accent1Color: '#BFBFBF',
  	alternateTextColor: white,
  },
});


export default themeDefault;