from django.contrib.auth.models import User
from django.http import HttpResponse
from channels.handler import AsgiHandler
from channels import Group
from django.core import serializers
import cgi
import json
from channels.sessions import channel_session
from rest_framework.authtoken.models import Token
from .models import Notificacion


# Connected to websocket.connect
@channel_session
def ws_add(message):
    # user= User.objects.get(pk=pk)

    # Accept the connection
    message.reply_channel.send({"accept": True})

    # Parse the query string
    params = cgi.parse_qs(message.content['query_string'])

    if "token" in params:

        token = Token.objects.filter(key=params["token"][0])

        if len(token) >= 1:
            # Set the username in the session
            message.channel_session["user_id"] = token[0].user.id
            # Add the user to the room_name group
            Group("notificacion-%d" % message.channel_session["user_id"]).add(message.reply_channel)

            filtro_notificacion = Notificacion.objects.filter(user_destino_id=message.channel_session["user_id"]).order_by('-datetime')

            data = []

            if len(filtro_notificacion) >= 1:
                for n in filtro_notificacion:
                    data.append(
                        {"pk": n.pk,
                         "title": n.data,
                         "datetime": str(n.datetime),
                         "tipo_id": n.tipo.pk,
                         "read": n.read,
                         "avatar": "static/notifications/star.png",
                         "show": True,
                         })



                Group("notificacion-%d" % n.user_destino.pk).send({
                    "text": json.dumps(data)

                })

        else:
            message.reply_channel.send({"close": True})


    else:
        # Close the connection.
        message.reply_channel.send({"close": True})


# Connected to websocket.receive
@channel_session
def ws_message(message):
    data = json.loads(message.content['text'])

    Notificacion.objects.filter(pk__in=data).update(read=True)


# Connected to websocket.disconnect
@channel_session
def ws_disconnect(message):
    Group("chat").discard(message.reply_channel)
