from rest_framework import permissions

class IsAutenticatedOrDenyMethodGet(permissions.BasePermission):

	def has_permission(self, request, view):
	
		if request.method == 'POST':
			return True

		if request.method == 'GET': 
			return request.user.is_authenticated()



