# -*- coding: utf-8 -*-
from django.shortcuts import render, HttpResponse
from .models import Persona, Solicitud
from .utils import get_edad
import os
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm
from reportlab.platypus import Paragraph, Frame
from reportlab.platypus import Image
from datetime import date




def report(request,pk=None):
	todays = date.today()
	solicitud=Solicitud.objects.get(pk=pk)
	

	response=HttpResponse(content_type='application/pdf')
	response['Content-Disposition']='attachment; filename=reporte.solicitud.{}.{}'.format((todays),'.pdf')
	buffer= BytesIO()
	c=canvas.Canvas(buffer, pagesize=A4)

	#Header per
	#Inicializamos platypus story.
	story1 = []
	imagen_logo = Image("public/img/imagen_logo", width=250, height=60)
	story1.append(imagen_logo)
	frame = Frame(80, 95, 185, 740)
	frame.addFromList(story1, c)

	#Añadimos algunos flowables
	
	c.setLineWidth(.3)
	
	c.setFont('Helvetica',12)
	c.drawString(200, 735,'Solicitud a las Practicas Profesionales')


	#Body
	story = []
	frame = Frame(47, 30, 500, 700, showBoundary=1)
	frame.addFromList(story, c)

	story2 = []
	frame = Frame(452, 630,84, 90, showBoundary=1)
	frame.addFromList(story2, c)

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,710,'Apellidos: ')
	c.setFont('Helvetica',12)
	c.drawString(112,710,' '+ solicitud.persona.primer_apellido.capitalize()+' '+ solicitud.persona.segundo_apellido.capitalize())

	
	c.setFont('Helvetica-Bold', 12)
	c.drawString(272,710,'Nombres: ')
	c.setFont('Helvetica',12)
	c.drawString(329,710,' '+ solicitud.persona.primer_nombre.capitalize()+' '+ solicitud.persona.segundo_nombre.capitalize())
	
	

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,687,'C.I.: ')
	c.setFont('Helvetica',12)
	c.drawString(77,687,' '+ solicitud.persona.cedula)
	
	c.setFont('Helvetica-Bold',12)
	c.drawString(149,687,'PNF: ')
	c.setFont('Helvetica',12)
	c.drawString(178,687,' '+ solicitud.persona.datos_universitarios.pnf.capitalize())
	

	c.setFont('Helvetica-Bold',12)
	c.drawString(272,687,'Indice Academico: ')
	c.setFont('Helvetica',12)
	c.drawString(380,687,' '+ str(solicitud.persona.datos_universitarios.promedio))
	
	c.setFont('Helvetica-Bold',12)
	c.drawString(54,664,'Fecha de Ingreso: ')
	c.setFont('Helvetica',12)
	c.drawString(158,664,' '+ str(solicitud.persona.datos_universitarios.fecha_entrada))

	c.setFont('Helvetica-Bold', 12)
	c.drawString(272, 664, 'Turno: ')

	if solicitud.persona.datos_universitarios.turno =='1':
		solicitud.persona.datos_universitarios.turno='Mañana'
	elif solicitud.persona.datos_universitarios.turno == '2':
		solicitud.persona.datos_universitarios.turno='Tarde'
	elif solicitud.persona.datos_universitarios.turno =='3':
		solicitud.persona.datos_universitarios.turno='Noche'
	c.setFont('Helvetica', 12)
	c.drawString(310, 664, ' ' + str(solicitud.persona.datos_universitarios.turno))

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,641,'Codigo Est: ')
	c.setFont('Helvetica',12)
	c.drawString(123,641,' ' + str(solicitud.persona.datos_universitarios.codigo_est))
	
	c.setFont('Helvetica-Bold',12)
	c.drawString(204,641,'Edad: ')
	c.setFont('Helvetica',12)
	c.drawString(238,641,' '+ str(get_edad(solicitud.persona.fecha_nac)))
	
	c.setFont('Helvetica-Bold',12)
	c.drawString(302,641,'Edo Civil: ')

	if solicitud.persona.estado_civil =='1':
		solicitud.persona.estado_civil='Soltero'
	elif solicitud.persona.estado_civil == '2':
		solicitud.persona.estado_civil='Casado'
	elif solicitud.persona.estado_civil =='3':
		solicitud.persona.estado_civil='Viudo'
	elif solicitud.persona.estado_civil =='4':
		solicitud.persona.estado_civil='Divorciado'
	c.setFont('Helvetica',12)
	c.drawString(358,641,' '+ solicitud.persona.estado_civil)

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,618,'Nacionalidad: ')
	if solicitud.persona.nacionalidad == '1':
		solicitud.persona.nacionalidad='Venezolano'
	elif solicitud.persona.nacionalidad=='2':
		solicitud.persona.nacionalidad='Extranjero'
	c.setFont('Helvetica',12)
	c.drawString(133,618,' '+ solicitud.persona.nacionalidad)

	c.setFont('Helvetica-Bold',12)
	c.drawString(248,618,'Ciudad de Origen: ')
	c.setFont('Helvetica',12)
	c.drawString(352,618,' '+ solicitud.persona.datos_direccion.ciudad_origen.capitalize())

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,595,'Direccion 1: ')
	c.setFont('Helvetica',12)
	c.drawString(123,595,' '+ solicitud.persona.datos_direccion.direccion_primaria.capitalize())

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,572,'Direccion 2: ')
	c.setFont('Helvetica',12)
	c.drawString(123,572,' '+ solicitud.persona.datos_direccion.direccion_secundaria.capitalize())


	c.setFont('Helvetica-Bold',12)
	c.drawString(54,549,'Ciudad: ')
	c.setFont('Helvetica',12)
	c.drawString(100,549,' '+ solicitud.persona.datos_direccion.ciudad_actual.capitalize())


	c.setFont('Helvetica-Bold',12)
	c.drawString(178,549,'Estado: ')
	c.setFont('Helvetica',12)
	c.drawString(223,549,' '+ solicitud.persona.datos_direccion.estado.capitalize())


	c.setFont('Helvetica-Bold',12)
	c.drawString(315,549,'Tlf(s): ')
	if solicitud.persona.telefono_fijo == '' or solicitud.persona.telefono_movil == '':
		vacio=''
	else:
		vacio=' - '
	c.setFont('Helvetica',12)
	c.drawString(349,549,' '+ solicitud.persona.telefono_fijo +vacio+ solicitud.persona.telefono_movil)

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,526,'E-mail: ')
	c.setFont('Helvetica',12)
	c.drawString(95,526,' '+ solicitud.persona.user.email)

	if solicitud.persona.datos_antropometricos.peso== None:
		solicitud.persona.datos_antropometricos.peso= ""
	c.setFont('Helvetica-Bold',12)
	c.drawString(54,503,'Peso: ')
	c.setFont('Helvetica',12)
	c.drawString(87,503,' '+ str(solicitud.persona.datos_antropometricos.peso))

	if solicitud.persona.datos_antropometricos.estatura== None:
		solicitud.persona.datos_antropometricos.estatura= ""
	c.setFont('Helvetica-Bold',12)
	c.drawString(160,503,'Estatura: ')
	c.setFont('Helvetica',12)
	c.drawString(212,503,' '+ str(solicitud.persona.datos_antropometricos.estatura))

	if solicitud.persona.datos_antropometricos.talla== None:
		solicitud.persona.datos_antropometricos.talla= ""
	c.setFont('Helvetica-Bold',12)
	c.drawString(281,503,'Talla: ')
	c.setFont('Helvetica',12)
	c.drawString(312,503,' '+ str(solicitud.persona.datos_antropometricos.talla))

	if solicitud.persona.datos_antropometricos.numero_calzado== None:
		solicitud.persona.datos_antropometricos.numero_calzado= ""
	c.setFont('Helvetica-Bold',12)
	c.drawString(381,503,'N° de Calzado: ')
	c.setFont('Helvetica',12)
	c.drawString(464,503,' '+ str(solicitud.persona.datos_antropometricos.numero_calzado))

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,480,'Sexo: ')
	if solicitud.persona.sexo == '1':
		solicitud.persona.sexo= 'Masculino'
	elif solicitud.persona.sexo == '2':
		solicitud.persona.sexo= 'Femenino'
	c.setFont('Helvetica',12)
	c.drawString(88,480,' '+ solicitud.persona.sexo)

	if solicitud.persona.datos_antropometricos.grupo_sanguineo== None:
		solicitud.persona.datos_antropometricos.grupo_sanguineo= ""
	c.setFont('Helvetica-Bold',12)
	c.drawString(190,480,'Grupo Sanguineo RH: ')
	c.setFont('Helvetica',12)
	c.drawString(315,480,' '+ solicitud.persona.datos_antropometricos.grupo_sanguineo.capitalize())

	c.setFont('Helvetica-Bold',12)
	c.drawString(400,480,'Alergico: ')
	if solicitud.persona.datos_antropometricos.alergico == True:
		solicitud.persona.datos_antropometricos.alergico= 'Si'
	else:
		solicitud.persona.datos_antropometricos.alergico='No'
		solicitud.persona.datos_antropometricos.tipo_alergia=' '
	c.setFont('Helvetica',12)
	c.drawString(460,480,' '+ solicitud.persona.datos_antropometricos.alergico)

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,457,'Especifique: ')
	c.setFont('Helvetica',12)
	c.drawString(127,457,' '+ solicitud.persona.datos_antropometricos.tipo_alergia.capitalize())

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,434,'Padece de alguna enfermedad: ')
	if solicitud.persona.datos_antropometricos.enfermedad == True:
		solicitud.persona.datos_antropometricos.enfermedad= 'Si'
	else:
		solicitud.persona.datos_antropometricos.enfermedad='No'
		solicitud.persona.datos_antropometricos.tipo_enfermedad=' '

	c.setFont('Helvetica',12)
	c.drawString(232,434,' '+ solicitud.persona.datos_antropometricos.enfermedad)

	c.setFont('Helvetica-Bold',12)
	c.drawString(280,434,'Especifique: ')
	c.setFont('Helvetica',12)
	c.drawString(352,434,' '+ solicitud.persona.datos_antropometricos.tipo_enfermedad.capitalize())


	c.setFont('Helvetica-Bold',12)
	c.drawString(54,411,'Trabaja: ')
	if solicitud.persona.datos_trabajo.trabaja == True:
		solicitud.persona.datos_trabajo.trabaja= 'Si'
	else:
		solicitud.persona.datos_trabajo.trabaja='No'
		solicitud.persona.datos_trabajo.empresa=' '
		solicitud.persona.datos_trabajo.actividad=' '
		solicitud.persona.datos_trabajo.direccion_empresa=' '
		solicitud.persona.datos_trabajo.telefono_empresa=' '

	c.setFont('Helvetica',12)
	c.drawString(102,411,' '+ solicitud.persona.datos_trabajo.trabaja)

	c.setFont('Helvetica-Bold',12)
	c.drawString(147,411,'Nombre de la Empresa: ')
	c.setFont('Helvetica',12)
	c.drawString(282,411,' '+ solicitud.persona.datos_trabajo.empresa.capitalize())

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,388,'Actividad que Realiza: ')
	c.setFont('Helvetica',12)
	c.drawString(181,388,' '+ solicitud.persona.datos_trabajo.actividad.capitalize())

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,365,'Direccion: ')
	c.setFont('Helvetica',12)
	c.drawString(113,365,' '+ solicitud.persona.datos_trabajo.direccion_empresa.capitalize())

	c.setFont('Helvetica-Bold',12)
	c.drawString(390,365,'Telefono: ')
	c.setFont('Helvetica',12)
	c.drawString(444,365,' '+ solicitud.persona.datos_trabajo.telefono_empresa.capitalize())

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,330,'Esta dispuesto a realizar sus practicas profesionales a nivel: ')
	
	if solicitud.ubicacion_local== True:
		solicitud.ubicacion_local='✓'
	else:
		solicitud.ubicacion_local=' '
	c.setFont('Helvetica-Bold',12)
	c.drawString(54,307,'Local: ')
	c.setFont('Helvetica',12)
	c.drawString(94,307,solicitud.ubicacion_local)

	if solicitud.ubicacion_regional== True:
		solicitud.ubicacion_regional='✓'
	else:
		solicitud.ubicacion_regional=' '
	c.setFont('Helvetica-Bold',12)
	c.drawString(110,307,'Regional: ')
	c.setFont('Helvetica',12)
	c.drawString(169,307,solicitud.ubicacion_regional)

	if solicitud.ubicacion_nacional== True:
		solicitud.ubicacion_nacional='✓'
	else:
		solicitud.ubicacion_nacional=' '
	c.setFont('Helvetica-Bold',12)
	c.drawString(187,307,'Nacional: ')
	c.setFont('Helvetica',12)
	c.drawString(245,307,solicitud.ubicacion_nacional)

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,257,'Posee algun contacto previo con una empresa, ingrese: ')

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,234,'Nombre de la Empresa: ')
	c.setFont('Helvetica',12)
	c.drawString(188,234,' '+ solicitud.nombre_empresa.capitalize())

	c.setFont('Helvetica-Bold',12)
	c.drawString(54,211,'Numeros de contacto: ')
	if solicitud.numero_contacto1 == '' or solicitud.numero_contacto2 == '':
		vacio=''
	else:
		vacio=' - '

	c.setFont('Helvetica',12)
	c.drawString(180,211,' '+ solicitud.numero_contacto1+vacio+solicitud.numero_contacto2)










	


	c.save()

	pdf= buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response	

	
