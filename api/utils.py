from datetime import date
import datetime

def get_edad(fecha_nacimiento):
	todays = date.today()
	edad= str((todays- fecha_nacimiento)/365)[:2]	
	return edad
	#


def agregar_dias_fecha(addDays=0):
	timeNow = datetime.datetime.now()
	if (addDays!=0):
		anotherTime = timeNow + datetime.timedelta(days=addDays)
	else:
		anotherTime = timeNow

	return anotherTime