from django.db.models.signals import post_save, pre_save
from django.contrib.auth.models import User
from .models import Solicitud, PracticaProfesional
from django.dispatch import receiver
from .models import *
from rest_framework.authtoken.models import Token
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from app.settings import PUSHER_APP_ID, PUSHER_KEY, PUSHER_SECRET
import json
from .constans import ACEPTADO, ACTIVO, PENDIENTE, RECHAZADO, SOLICITUD, REGISTRO, ESTUDIANTE

from channels import Group
import datetime


@receiver(post_save, sender=User)
def init_new_user(sender, instance, signal, created, **kwargs):
    """
    Create an authentication token for new users
    """
    if created:
        print('Token created')
        Token.objects.create(user=instance)

        filtro_user = User.objects.get(pk=instance.pk)
        Notificacion.objects.get_or_create(
            user_destino=filtro_user,
            data="Bienvenido a SIGEPP UPTAG ahora usted podra realizar su solicitud a las practicas profesionales",
            tipo=TipoNotificacion.objects.get(pk=ESTUDIANTE),
            datetime=datetime.datetime.now(),
            read=False,
        )

    filtro_superuser = User.objects.filter(is_staff=True)

    for n in filtro_superuser:
        Notificacion.objects.get_or_create(
            user_destino=n,
            data="Se ha registrado el usuario %s" % instance.username + " en el sistema",
            tipo=TipoNotificacion.objects.get(pk=REGISTRO),
            datetime=datetime.datetime.now(),
            read=False,
        )


@receiver(post_save, sender=User)
def init_datos_personales(sender, instance, signal, created, **kwargs):
    if created:
        p = Persona()

        p.user = instance
        p.datos_direccion = DatosDireccion.objects.create()
        p.datos_universitarios = DatosUniversitarios.objects.create()
        p.datos_antropometricos = DatosAntropometricos.objects.create()
        p.datos_trabajo = DatosTrabajo.objects.create()

        p.save()


@receiver(post_save, sender=Solicitud)
def init_practica_profesional(sender, instance, signal, created, **kwargs):
    if created:

        filtro_superuser = User.objects.filter(is_staff=True)
        for n in filtro_superuser:
            Notificacion.objects.get_or_create(
                user_destino=n,
                data="El usuario %s" % instance.persona.user.username + " ha generado una nueva solicitud a las practicas profesionales",
                tipo=TipoNotificacion.objects.get(pk=SOLICITUD),
                datetime=datetime.datetime.now(),
                read=False,
            )

    if instance.estado.id == ACEPTADO:
        PracticaProfesional.objects.get_or_create(
            estado=Estado.objects.get(pk=PENDIENTE),
            solicitud=instance,
            empresa=None,
            tutor=None,
            nota=None,
            fecha_inicio=None,
            fecha_final=None,
        )

        filtro_user = User.objects.get(pk=instance.persona.user.pk)

        Notificacion.objects.get_or_create(
            user_destino=filtro_user,
            data="Su solicitud a las practicas profesionales ha sido aceptada",
            tipo=TipoNotificacion.objects.get(pk=ESTUDIANTE),
            datetime=datetime.datetime.now(),
            read=False,
        )

    if instance.estado.id == RECHAZADO:
        filtro_user = User.objects.get(pk=instance.persona.user.pk)
        Notificacion.objects.get_or_create(
            user_destino=filtro_user,
            data="Su solicitud a las practicas profesionales ha sido rechazada, genere una nueva cumpliendo con todos los requisitos",
            tipo=TipoNotificacion.objects.get(pk=ESTUDIANTE),
            datetime=datetime.datetime.now(),
            read=False,
        )


@receiver(post_save, sender=Notificacion)
def init_notificacion(sender, instance, signal, created, **kwargs):
    if created:
        """import pusher
        pusher_client = pusher.Pusher(
            app_id=PUSHER_APP_ID,
            key=PUSHER_KEY,
            secret=PUSHER_SECRET,
            cluster='us2',
            ssl=True
        )"""
        filtro_notificacion = Notificacion.objects.filter(user_destino_id=instance.user_destino.pk)
        data = []
        if len(filtro_notificacion) >= 1:
            for n in filtro_notificacion:
                """channel = ("notificacion-%d" % n.user_destino.pk)

                event_data = {"pk": n.pk,
                              "title": n.data,
                              "datetime": str(n.datetime),
                              "tipo_id": n.tipo.pk,
                              "read": n.read,
                              "avatar": "static/notifications/star.png",
                              "show": True,
                              }
                pusher_client.trigger(channel,'notificar',{'message': event_data})"""

                data.append(
                    {"pk": n.pk,
                     "title": n.data,
                     "datetime": str(n.datetime),
                     "tipo_id": n.tipo.pk,
                     "read": n.read,
                     "avatar": "static/notifications/star.png",
                     "show": True,
                     })

                Group("notificacion-%d" % n.user_destino.pk).send({
                    "text": json.dumps(data)
                })
