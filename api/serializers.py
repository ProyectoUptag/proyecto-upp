from rest_framework import response, permissions, serializers, status
from django.contrib.auth.models import User
from .constans import ESTUDIANTE
import datetime
from .models import Persona, DatosDireccion, DatosUniversitarios, DatosAntropometricos, DatosTrabajo, Estado, Archivo, \
    Solicitud, Empresa, Tutor, PracticaProfesional, Notificacion, TipoNotificacion
import os, json
from app.settings import BASE_DIR


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'is_staff', 'is_superuser', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user_extra_data = {
            'is_staff': validated_data.get('is_staff', False),
            'is_superuser': validated_data.get('is_superuser', False)
        }

        return User.objects.create_user(
            validated_data.get('username', None),
            validated_data.get('email', None),
            validated_data.get('password'),
            **user_extra_data
        )


class DatosDireccionSerializer(serializers.ModelSerializer):
    class Meta:
        model = DatosDireccion
        fields = ('id', 'ciudad_origen', 'ciudad_actual',
                  'estado', 'direccion_primaria', 'direccion_secundaria', 'codigo_postal')

    def create(self, validated_data):
        return DatosPersonales.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.ciudad_origen = validated_data.get('ciudad_origen', instance.ciudad_origen)
        instance.ciudad_actual = validated_data.get('ciudad_actual', instance.ciudad_actual)
        instance.estado = validated_data.get('estado', instance.estado)
        instance.direccion_primaria = validated_data.get('direccion_primaria', instance.direccion_primaria)
        instance.direccion_secundaria = validated_data.get('direccion_secundaria', instance.direccion_secundaria)
        instance.codigo_postal = validated_data.get('codigo_postal', instance.codigo_postal)
        instance.save()
        return instance


class DatosUniversitariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = DatosUniversitarios
        fields = ('id', 'pnf', 'fecha_entrada', 'promedio', 'codigo_est', 'turno')

    def create(self, validated_data):
        return DatosUniversitarios.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.pnf = validated_data.get('pnf', instance.pnf)
        instance.fecha_entrada = validated_data.get('fecha_entrada', instance.fecha_entrada)
        instance.promedio = validated_data.get('promedio', instance.promedio)
        instance.codigo_est = validated_data.get('codigo_est', instance.codigo_est)
        instance.turno = validated_data.get('turno', instance.turno)
        instance.save()
        return instance


class DatosAntropometricosSerializer(serializers.ModelSerializer):
    class Meta:
        model = DatosAntropometricos
        fields = ('id', 'peso', 'estatura', 'talla', 'numero_calzado',
                  'grupo_sanguineo', 'enfermedad', 'tipo_enfermedad', 'alergico', 'tipo_alergia')

    def create(self, validated_data):
        return DatosAntropometricos.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.peso = validated_data.get('peso', instance.peso)
        instance.estatura = validated_data.get('estatura', instance.estatura)
        instance.talla = validated_data.get('talla', instance.talla)
        instance.numero_calzado = validated_data.get('numero_calzado', instance.numero_calzado)
        instance.grupo_sanguineo = validated_data.get('grupo_sanguineo', instance.grupo_sanguineo)
        instance.enfermedad = validated_data.get('enfermedad', instance.enfermedad)
        instance.tipo_enfermedad = validated_data.get('tipo_enfermedad', instance.tipo_enfermedad)
        instance.alergico = validated_data.get('alergico', instance.alergico)
        instance.tipo_alergia = validated_data.get('tipo_alergia', instance.tipo_alergia)
        instance.save()
        return instance


class DatosTrabajoSerializer(serializers.ModelSerializer):
    class Meta:
        model = DatosTrabajo
        fields = ('id', 'trabaja', 'empresa', 'actividad', 'direccion_empresa',
                  'telefono_empresa')

    def create(self, validated_data):
        return DatosTrabajo.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.trabaja = validated_data.get('trabaja', instance.trabaja)
        instance.empresa = validated_data.get('empresa', instance.empresa)
        instance.actividad = validated_data.get('actividad', instance.actividad)
        instance.direccion_empresa = validated_data.get('direccion_empresa', instance.direccion_empresa)
        instance.telefono_empresa = validated_data.get('telefono_empresa', instance.telefono_empresa)
        instance.save()
        return instance


class EstadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estado
        fields = ('id', 'nombre',)

    def create(self, validated_data):
        return Estado.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.id = validated_data.get('id', instance.id)
        instance.nombre = validated_data.get('nombre', instance.nombre)
        instance.save()
        return instance


class PersonaSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    datos_direccion = DatosDireccionSerializer()
    datos_universitarios = DatosUniversitariosSerializer()
    datos_antropometricos = DatosAntropometricosSerializer()
    datos_trabajo = DatosTrabajoSerializer()

    class Meta:
        model = Persona
        fields = ('user', 'id', 'primer_nombre', 'segundo_nombre', 'primer_apellido',
                  'segundo_apellido', 'fecha_nac', 'estado_civil', 'nacionalidad', 'cedula', 'telefono_movil',
                  'telefono_fijo', 'sexo', 'datos_direccion', 'datos_universitarios', 'datos_antropometricos',
                  'datos_trabajo')
        extra_kwargs = {'datos_direccion': {'write_only': True},
                        'datos_universitarios': {'write_only': True},
                        'datos_antropometricos': {'write_only': True},
                        'datos_trabajo': {'write_only': True}}

    def update(self, instance, validated_data):
        """
        Update and return an existing Persona instance, given the validated data.
        """

        instance.user = validated_data.get('user', instance.user)

        DatosDireccion.objects.filter(pk=instance.datos_direccion.id).update(
            **validated_data.get('datos_direccion', instance.datos_direccion))
        DatosUniversitarios.objects.filter(pk=instance.datos_universitarios.id).update(
            **validated_data.get('datos_universitarios', instance.datos_universitarios))
        DatosAntropometricos.objects.filter(pk=instance.datos_antropometricos.id).update(
            **validated_data.get('datos_antropometricos', instance.datos_antropometricos))
        DatosTrabajo.objects.filter(pk=instance.datos_trabajo.id).update(
            **validated_data.get('datos_trabajo', instance.datos_trabajo))

        instance.cedula = validated_data.get('cedula', instance.cedula)
        instance.primer_nombre = validated_data.get('primer_nombre', instance.primer_nombre)
        instance.segundo_nombre = validated_data.get('segundo_nombre', instance.segundo_nombre)
        instance.primer_apellido = validated_data.get('primer_apellido', instance.primer_apellido)
        instance.segundo_apellido = validated_data.get('segundo_apellido', instance.segundo_apellido)
        instance.fecha_nac = validated_data.get('fecha_nac', instance.fecha_nac)
        instance.estado_civil = validated_data.get('estado_civil', instance.estado_civil)
        instance.nacionalidad = validated_data.get('nacionalidad', instance.nacionalidad)
        instance.telefono_movil = validated_data.get('telefono_movil', instance.telefono_movil)
        instance.telefono_fijo = validated_data.get('telefono_fijo', instance.telefono_fijo)
        instance.sexo = validated_data.get('sexo', instance.sexo)
        instance.save()
        return instance


class ArchivoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Archivo
        fields = ('id', 'nombre', 'file', 'url')

    def create(self, validated_data):
        return Archivo.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.file = validated_data.get('file', instance.file)
        instance.nombre = validated_data.get('nombre', instance.nombre)
        instance.url = validated_data.get('url', instance.url)
        instance.save()
        return instance


class SolicitudSerializer(serializers.ModelSerializer):
    persona = PersonaSerializer(read_only=True)
    estado = serializers.SlugRelatedField(slug_field='pk', queryset=Estado.objects.all())
    archivos = ArchivoSerializer(many=True, read_only=True)

    class Meta:
        model = Solicitud
        fields = ('id', 'persona', 'estado', 'archivos', 'fecha', 'ubicacion_local', 'ubicacion_regional',
                  'ubicacion_nacional', 'nombre_empresa', 'numero_contacto1', 'numero_contacto2')
        extra_kwargs = {'persona': {'write_only': True},
                        'estado': {'write_only': True},
                        'archivos': {'write_only': True}}

    def create(self, validated_data):
        solicitud = Solicitud(**validated_data)
        solicitud.save()

        for i in archivo:
            solicitud.archivos.add(i)

        return solicitud

    def update(self, instance, validated_data):
        instance.persona = validated_data.get('persona', instance.persona)
        instance.estado = validated_data.get('estado', instance.estado)

        instance.fecha = validated_data.get('fecha', instance.fecha)
        instance.ubicacion_local = validated_data.get('ubicacion_local', instance.ubicacion_local)
        instance.ubicacion_regional = validated_data.get('ubicacion_regional', instance.ubicacion_regional)
        instance.ubicacion_nacional = validated_data.get('ubicacion_nacional', instance.ubicacion_nacional)
        instance.nombre_empresa = validated_data.get('nombre_empresa', instance.nombre_empresa)
        instance.numero_contacto1 = validated_data.get('numero_contacto1', instance.numero_contacto1)
        instance.numero_contacto2 = validated_data.get('numero_contacto2', instance.numero_contacto2)
        instance.save()
        return instance


class EmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empresa
        fields = ('id', 'nombre', 'rif', 'direccion', 'numero_contacto')

    def create(self, validated_data):
        return Empresa.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.nombre = validated_data.get('nombre', instance.nombre)
        instance.rif = validated_data.get('rif', instance.rif)
        instance.direccion = validated_data.get('direccion', instance.direccion)
        instance.numero_contacto = validated_data.get('numero_contacto', instance.numero_contacto)
        instance.save()
        return instance


class TutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tutor
        fields = ('id', 'nombre', 'apellido', 'cedula', 'numero_contacto')

    def create(self, validated_data):
        return Tutor.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.nombre = validated_data.get('nombre', instance.nombre)
        instance.apellido = validated_data.get('apellido', instance.apellido)
        instance.cedula = validated_data.get('cedula', instance.cedula)
        instance.numero_contacto = validated_data.get('numero_contacto', instance.numero_contacto)
        instance.save()
        return instance


class PracticaProfesionalSerializer(serializers.ModelSerializer):
    empresa = EmpresaSerializer(read_only=True)
    tutor = TutorSerializer(read_only=True)
    solicitud = SolicitudSerializer(read_only=True)
    estado = serializers.SlugRelatedField(slug_field='pk', queryset=Estado.objects.all())

    class Meta:
        model = PracticaProfesional
        fields = ('id', 'estado', 'solicitud', 'empresa', 'tutor', 'fecha_inicio', 'fecha_final', 'nota')


class PracticaProfesionalPutSerializer(serializers.ModelSerializer):
    empresa = serializers.SlugRelatedField(slug_field='pk', queryset=Empresa.objects.all(), required=False)
    tutor = serializers.SlugRelatedField(slug_field='pk', queryset=Tutor.objects.all(), required=False)
    solicitud = SolicitudSerializer(read_only=True)
    estado = serializers.SlugRelatedField(slug_field='pk', queryset=Estado.objects.all())

    class Meta:
        model = PracticaProfesional
        fields = ('id', 'estado', 'solicitud', 'empresa', 'tutor', 'fecha_inicio', 'fecha_final', 'nota')

    def update(self, instance, validated_data):
        filtro_user = User.objects.get(pk=instance.solicitud.persona.user.pk)

        if instance.empresa != validated_data.get('empresa', instance.empresa):
            Notificacion.objects.get_or_create(
                user_destino=filtro_user,
                data="Usted ha sido asignado a una empresa para realizar su practica profesional",
                tipo=TipoNotificacion.objects.get(pk=ESTUDIANTE),
                datetime=datetime.datetime.now(),
                read=False,
            )
        if instance.tutor != validated_data.get('tutor', instance.tutor):
            Notificacion.objects.get_or_create(
                user_destino=filtro_user,
                data="Se le ha sido asignado un tutor a su practica profesional",
                tipo=TipoNotificacion.objects.get(pk=ESTUDIANTE),
                datetime=datetime.datetime.now(),
                read=False,
            )

        if instance.nota != validated_data.get('nota', instance.nota):
            Notificacion.objects.get_or_create(
                user_destino=filtro_user,
                data="Ya tiene disponible la nota correspondiente a su practica profesional",
                tipo=TipoNotificacion.objects.get(pk=ESTUDIANTE),
                datetime=datetime.datetime.now(),
                read=False,
            )
        instance.empresa = validated_data.get('empresa', instance.empresa)
        instance.tutor = validated_data.get('tutor', instance.tutor)
        instance.estado = validated_data.get('estado', instance.estado)
        instance.fecha_inicio = validated_data.get('fecha_inicio', instance.fecha_inicio)
        instance.fecha_final = validated_data.get('fecha_final', instance.fecha_final)
        instance.nota = validated_data.get('nota', instance.nota)
        instance.save()


        return instance


