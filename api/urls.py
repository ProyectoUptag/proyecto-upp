from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token

from .views import UserViewSet, PersonaViewSet, DatosDireccionViewSet, DatosUniversitariosViewSet, DatosAntropometricosViewSet, DatosTrabajoViewSet, EstadoViewSet, SolicitudViewSet, ArchivoViewSet, EmpresaViewSet, TutorViewSet, PracticaProfesionalViewSet, PracticaProfesionalPutViewSet

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'personas', PersonaViewSet)
router.register(r'datospersonales', DatosDireccionViewSet)
router.register(r'datosuniversitarios', DatosUniversitariosViewSet)
router.register(r'datosantropometricos', DatosAntropometricosViewSet)
router.register(r'datostrabajo', DatosTrabajoViewSet)
router.register(r'estado', EstadoViewSet)
router.register(r'solicitud', SolicitudViewSet, base_name= 'solicitud')
router.register(r'archivo', ArchivoViewSet)
router.register(r'empresa', EmpresaViewSet)
router.register(r'tutor', TutorViewSet)
router.register(r'practicaprofesional', PracticaProfesionalViewSet, base_name= 'practica-profesional')
router.register(r'practicaprofesionalput', PracticaProfesionalPutViewSet, base_name= 'practica-profesional-put')






urlpatterns = router.urls

urlpatterns += [
    url(r'^obtain-auth-token/$', csrf_exempt(obtain_auth_token)),
   #

]