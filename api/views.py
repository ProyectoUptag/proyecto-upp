# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from .models import Persona, DatosDireccion, DatosUniversitarios, DatosAntropometricos, DatosTrabajo, Estado, Archivo, \
    Solicitud, Empresa, Tutor, ResetPassword, Notificacion
from api.models import PracticaProfesional
from rest_framework import viewsets, response, permissions, serializers, status, generics
from .serializers import UserSerializer, PersonaSerializer, DatosDireccionSerializer, DatosUniversitariosSerializer, \
    DatosAntropometricosSerializer, DatosTrabajoSerializer, EstadoSerializer, ArchivoSerializer, SolicitudSerializer, \
    EmpresaSerializer, TutorSerializer, PracticaProfesionalSerializer, PracticaProfesionalPutSerializer
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.decorators import detail_route, api_view
from rest_framework.response import Response
import json

from .utils import get_edad, agregar_dias_fecha
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view, permission_classes
from .permissions import IsAutenticatedOrDenyMethodGet
import os
import uuid
from django.core.mail import send_mail
from app import settings
import hashlib
import datetime
from templated_email import send_templated_mail
from app.settings import PUSHER_APP_ID, PUSHER_KEY, PUSHER_SECRET


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAutenticatedOrDenyMethodGet,)

    @detail_route(methods=['GET'])
    def persona(self, request, pk=None):

        user = self.get_object()
        id_user = user.pk

        if pk == 'i':
            id_user = request.user.pk

        try:
            data_persona = Persona.objects.get(pk=id_user)
        except:
            data_persona = None

        if request.method == 'GET':
            serializer = PersonaSerializer(data_persona)
            return Response(serializer.data)

    def retrieve(self, request, pk=None):

        if pk == 'i':
            return response.Response(UserSerializer(request.user).data)
        return super(UserViewSet, self).retrieve(request, pk)


"""def get_id_notificacion(request):
    return render(request, "get_id_notificacion.html")


def post_id_notificacion(request, pk=None):
    if request.method == 'POST':
        data = json.loads(pk)
        Notificacion.objects.filter(pk__in=data).update(read=True)
        return JsonResponse({"tipo":"notificacion","read": True})


def pusher(request, pk=None):
    import pusher

    if request.method == 'GET':
        pusher_client = pusher.Pusher(
            app_id=PUSHER_APP_ID,
            key=PUSHER_KEY,
            secret=PUSHER_SECRET,
            cluster='us2',
            ssl=True
        )
        filtro_notificacion = Notificacion.objects.filter(user_destino_id=pk)

        if len(filtro_notificacion) >= 1:
            for n in filtro_notificacion:
                channel = ("notificacion-%d" % n.user_destino.pk)

                event_data = {"pk": n.pk,
                              "title": n.data,
                              "datetime": str(n.datetime),
                              "tipo_id": n.tipo.pk,
                              "read": n.read,
                              "avatar": "static/notifications/star.png",
                              "show": True,
                              }
                pusher_client.trigger(channel, 'notificar', {'message': event_data})

    return render(request, "pusher.html")"""


@csrf_exempt
def reset_password(request):
    print request.META
    if request.method == 'POST':
        email = request.POST['email']
        filtro = User.objects.filter(email=email)
        if len(filtro) == 1:
            user = filtro[0]
            data = '{}.{}.{}'.format(user.id, user.username, str(datetime.datetime.now()))

            hash_object = hashlib.sha256(data)
            hash_string = hash_object.hexdigest()

            ResetPassword.objects.filter(user_id=user).delete()

            rp = ResetPassword()
            rp.user = filtro[0]
            rp.hash_recuperacion = hash_string
            rp.fecha_expire = agregar_dias_fecha(1)
            rp.save()

            mensaje = 'Haga click en el siguente enlace para continuar'
            url = 'http://' + request.META.get('HTTP_HOST') + '/#/password-forgot/{}'.format(hash_string)
            mensaje_email = "%s: %s" % (mensaje, url)
            send_templated_mail(
                template_name='send_email',
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=[email],
                context={
                    'encabezado': 'Hola, usted ha solicitado cambiar su contraseña.',
                    'mensaje': mensaje_email,
                    'pie': "Enviado por SIGEPP UPTAG.",
                },
            )
            return JsonResponse({"enviado": True, "mensaje": "Correo enviado con exito."})
        else:
            return JsonResponse({"enviado": False, "mensaje": "Este correo no esta asociado a ningun usuario"})


def get_confirm_password(request, hash_string=None):
    context = {'hash': hash_string}
    return render(request, "confirm.html", context)


@csrf_exempt
def confirm_password(request, action=None, hash_string=None):
    if request.method == 'GET':
        if action == 'verificar':
            filtro = ResetPassword.objects.filter(hash_recuperacion=hash_string)

            if len(filtro) == 1:
                return JsonResponse({"existe": True, "mensaje": "hash encontrado"})
            else:
                return JsonResponse({"existe": False, "mensaje": "Este hash no pertenece a ningun usuario"})

    if request.method == 'POST':
        if action == 'guardar':
            filtro = ResetPassword.objects.filter(hash_recuperacion=hash_string)

            if len(filtro) == 1:
                u = User.objects.get(id=filtro[0].user_id)
                u.set_password(request.POST.get('password'))
                u.save()
                return JsonResponse({"guardado": True, "mensaje": "contrasena cambiada"})
            else:
                return JsonResponse({"existe": False, "mensaje": "Este hash no pertenece a ningun usuario"})


class DatosDireccionViewSet(viewsets.ModelViewSet):
    queryset = DatosDireccion.objects.all()
    serializer_class = DatosDireccionSerializer
    permission_classes = (IsAuthenticated,)


class DatosUniversitariosViewSet(viewsets.ModelViewSet):
    queryset = DatosUniversitarios.objects.all()
    serializer_class = DatosUniversitariosSerializer
    permission_classes = (IsAuthenticated,)


class DatosAntropometricosViewSet(viewsets.ModelViewSet):
    queryset = DatosAntropometricos.objects.all()
    serializer_class = DatosAntropometricosSerializer
    permission_classes = (IsAuthenticated,)


class DatosTrabajoViewSet(viewsets.ModelViewSet):
    queryset = DatosTrabajo.objects.all()
    serializer_class = DatosTrabajoSerializer
    permission_classes = (IsAuthenticated,)


class EstadoViewSet(viewsets.ModelViewSet):
    queryset = Estado.objects.all()
    serializer_class = EstadoSerializer
    permission_classes = (IsAuthenticated,)


class ArchivoViewSet(viewsets.ModelViewSet):
    serializer_class = ArchivoSerializer
    parser_classes = (MultiPartParser, FormParser)
    # permission_classes = (IsAuthenticated,)
    queryset = Archivo.objects.all()


class SolicitudViewSet(viewsets.ModelViewSet):
    serializer_class = SolicitudSerializer
    parser_classes = (MultiPartParser, FormParser)
    # permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        querysets = Solicitud.objects.all()
        estado_id = self.request.query_params.get('estado_id', None)
        persona_id = self.request.query_params.get('persona_id', None)
        user_id = self.request.query_params.get('user_id', None)

        if estado_id is not None:
            querysets = querysets.filter(estado_id=estado_id)

        if persona_id is not None:
            querysets = querysets.filter(persona_id=persona_id)

        if user_id is not None:
            querysets = querysets.filter(persona__user=user_id)
        return querysets


class PersonaViewSet(viewsets.ModelViewSet):
    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer
    permission_classes = (IsAuthenticated,)


@csrf_exempt
def solicitud(request):
    if request.method == 'POST':
        s = Solicitud()
        s.ubicacion_local = request.POST.get('ubicacion_local', False)
        s.ubicacion_regional = request.POST.get('ubicacion_regional', False)
        s.ubicacion_nacional = request.POST.get('ubicacion_nacional', False)
        s.nombre_empresa = request.POST['nombre_empresa']
        s.numero_contacto1 = request.POST['numero_contacto1']
        s.numero_contacto2 = request.POST['numero_contacto2']
        s.estado_id = request.POST['estado_id']
        s.persona_id = request.POST['user_id']
        a = request.FILES.getlist('archivos')
        pks = []

        for n in a:

            usuario = s.persona.user.username
            nombre_carpeta = 'build/upload/{}'.format(usuario)

            extencion_archivo = n.name.split(".")[-1]
            nombre_archivo_codificado = "{}.{}".format(uuid.uuid4().hex, extencion_archivo)
            nombre_archivo = os.path.join(nombre_carpeta, nombre_archivo_codificado)
            urls='http://' + request.META.get('HTTP_HOST') + '/static/upload/{}'.format(usuario) + "/" + nombre_archivo_codificado
            if not os.path.exists(nombre_carpeta):
                os.makedirs(nombre_carpeta)

            with open(nombre_archivo, "w") as archivo:
                archivo.write(n.read())
                archivo.close()

            pks.append(Archivo.objects.create(file=nombre_archivo, nombre=n.name, url=urls))
        s.save()

        for i in pks:
            s.archivos.add(i)


        return JsonResponse({"tipo":"solicitud","enviado": True})


class EmpresaViewSet(viewsets.ModelViewSet):
    queryset = Empresa.objects.all()
    serializer_class = EmpresaSerializer
    #permission_classes = (IsAuthenticated,)


class TutorViewSet(viewsets.ModelViewSet):
    queryset = Tutor.objects.all()
    serializer_class = TutorSerializer
    #permission_classes = (IsAuthenticated,)


class PracticaProfesionalViewSet(viewsets.ModelViewSet):
    serializer_class = PracticaProfesionalSerializer
    # permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = PracticaProfesional.objects.all()
        estado = self.request.query_params.get('estado', None)
        user_id = self.request.query_params.get('user_id', None)
        sexo = self.request.query_params.get('sexo', None)
        ciudad = self.request.query_params.get('ciudad_origen', None)
        if estado is not None:
            queryset = queryset.filter(solicitud__estado=estado)

        if user_id is not None:
            queryset = queryset.filter(solicitud__persona__user=user_id)

        if sexo is not None:
            queryset = queryset.filter(solicitud__persona__sexo=sexo)

        if ciudad is not None:
            queryset = queryset.filter(solicitud__persona__datos_direccion__ciudad_origen=ciudad)

        return queryset
class PracticaProfesionalPutViewSet(viewsets.ModelViewSet):
    queryset = PracticaProfesional.objects.all()
    serializer_class = PracticaProfesionalPutSerializer
    #permission_classes = (IsAuthenticated,)
