# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User
from .constans import SOLTERO, CASADO, VIUDO, DIVORCIADO, VENEZOLANO, EXTRANJERO, MASCULINO, FEMENINO, MANANA, TARDE, NOCHE


class Persona(models.Model):
    user = models.ForeignKey(User)
    primer_nombre = models.CharField(max_length=15, null=True, blank=True)
    segundo_nombre = models.CharField(max_length=15, null=True, blank=True)
    primer_apellido = models.CharField(max_length=15, null=True, blank=True)
    segundo_apellido = models.CharField(max_length=15, null=True, blank=True)
    fecha_nac = models.DateField(null=True, blank=True)
    ESTADO_CIVIL_CHOICES = (
        (SOLTERO, 'Soltero'),
        (CASADO, 'Casado'),
        (VIUDO, 'Viudo'),
        (DIVORCIADO, 'Divorciado'),
    )
    estado_civil = models.CharField(max_length=12, choices=ESTADO_CIVIL_CHOICES, null=True, blank=True)

    NACIOANALIDAD_CHOICES = (
        (VENEZOLANO, 'Venezolano'),
        (EXTRANJERO, 'Extranjero'),
    )
    nacionalidad = models.CharField(max_length=20, choices=NACIOANALIDAD_CHOICES, null=True, blank=True)
    cedula = models.CharField(max_length=8, null=True, blank=True)
    telefono_movil = models.CharField(max_length=12, null=True, blank=True)
    telefono_fijo = models.CharField(max_length=12, null=True, blank=True)
    SEXO_CHOICES = (
        (MASCULINO, 'Masculino'),
        (FEMENINO, 'Femenino'),
    )
    sexo = models.CharField(max_length=20, choices=SEXO_CHOICES, null=True, blank=True)
    datos_direccion = models.ForeignKey('DatosDireccion')
    datos_universitarios = models.ForeignKey('DatosUniversitarios')
    datos_antropometricos = models.ForeignKey('DatosAntropometricos')
    datos_trabajo = models.ForeignKey('DatosTrabajo')

    def __unicode__(self):
        return str(self.id)


class DatosDireccion(models.Model):
    ciudad_origen = models.CharField(max_length=20, null=True, blank=True)
    ciudad_actual = models.CharField(max_length=20, null=True, blank=True)
    estado = models.CharField(max_length=20, null=True, blank=True)
    direccion_primaria = models.CharField(max_length=50, null=True, blank=True)
    direccion_secundaria = models.CharField(max_length=50, null=True, blank=True)
    codigo_postal = models.IntegerField(null=True, blank=True)

class DatosUniversitarios(models.Model):
    pnf = models.CharField(max_length=20, null=True, blank=True)
    TURNO_CLASE_CHOICES = (
        (MANANA, 'mañana'),
        (TARDE, 'tarde'),
        (NOCHE, 'noche'),
    )
    turno = models.CharField(max_length=20, choices=TURNO_CLASE_CHOICES, blank=True, null=True)
    fecha_entrada = models.DateField(null=True, blank=True)
    promedio = models.FloatField(null=True, blank=True, default=0)
    codigo_est = models.IntegerField(null=True, blank=True, default=0)


class DatosAntropometricos(models.Model):
    peso = models.FloatField(null=True, blank=True)
    estatura = models.FloatField(null=True, blank=True)
    talla = models.CharField(null=True, blank=True,max_length=20)
    numero_calzado = models.FloatField(null=True, blank=True)
    grupo_sanguineo = models.CharField(max_length=10, null=True, blank=True)
    enfermedad = models.BooleanField(default=False)
    tipo_enfermedad = models.TextField(blank=True, null=True)
    alergico = models.BooleanField(default=False)
    tipo_alergia = models.TextField(blank=True, null=True)


class DatosTrabajo(models.Model):
    trabaja = models.BooleanField(default=False)
    empresa = models.CharField(max_length=50, blank=True, null=True)
    actividad = models.CharField(max_length=60, blank=True, null=True)
    direccion_empresa = models.CharField(max_length=50, blank=True, null=True)
    telefono_empresa = models.CharField(max_length=11, blank=True, null=True)


class Estado(models.Model):
    class Meta:
        db_table = '{}_estado'.format(__module__.split(".")[0])

    nombre = models.CharField(max_length=20,blank=True, null=True)

    def __unicode__(self):
        return self.nombre


class Archivo(models.Model):
    file = models.FileField(upload_to='build/tmp')
    nombre = models.CharField(max_length=50, blank=True, null=True)
    url= models.CharField(max_length=200,blank=True, null=True)

    def __unicode__(self):
        return self.nombre


class Solicitud(models.Model):
    estado = models.ForeignKey(Estado)
    persona = models.ForeignKey(Persona)
    archivos = models.ManyToManyField(Archivo)
    fecha = models.DateField(auto_now=True)
    ubicacion_local = models.BooleanField(default=False)
    ubicacion_regional = models.BooleanField(default=False)
    ubicacion_nacional = models.BooleanField(default=False)
    nombre_empresa = models.CharField(max_length=60, blank=True, null=True)
    numero_contacto1 = models.CharField(max_length=11, blank=True, null=True)
    numero_contacto2 = models.CharField(max_length=11, blank=True, null=True)

    def __unicode__(self):
        return str(self.id)


class Empresa(models.Model):
    nombre = models.CharField(max_length=60,blank=True, null=True)
    rif = models.CharField(max_length=20, blank=True, null=True)
    direccion = models.CharField(max_length=200, blank=True, null=True)
    numero_contacto = models.CharField(max_length=11, blank=True, null=True)

    def __unicode__(self):
        return self.nombre


class Tutor(models.Model):
    nombre = models.CharField(max_length=20,blank=True, null=True)
    apellido = models.CharField(max_length=20,blank=True, null=True)
    cedula = models.CharField(max_length=8,blank=True, null=True)
    numero_contacto = models.CharField(max_length=11, blank=True, null=True)

    def __unicode__(self):
        return self.nombre + ' ' + self.apellido


class PracticaProfesional(models.Model):
    estado = models.ForeignKey(Estado)
    solicitud = models.ForeignKey(Solicitud)
    empresa = models.ForeignKey(Empresa, blank=True, null=True)
    tutor = models.ForeignKey(Tutor, blank=True, null=True)
    fecha_inicio = models.DateField(null=True, blank=True)
    fecha_final = models.DateField(null=True, blank=True)
    nota = models.FloatField(blank=True, null=True)

    def __unicode__(self):
        return self.solicitud.user.username


class ResetPassword(models.Model):
    user = models.ForeignKey(User)
    hash_recuperacion = models.CharField(max_length=200, blank=True, null=True)
    fecha_expire = models.DateTimeField()


class TipoNotificacion(models.Model):
    nombre = models.CharField(max_length=60)


class Notificacion(models.Model):
    user_destino = models.ForeignKey(User)
    tipo = models.ForeignKey(TipoNotificacion)
    data = models.CharField(max_length=200, blank=True, null=True)
    datetime = models.DateTimeField(auto_now_add=True)
    read = models.BooleanField(default=False)
